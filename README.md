# README #

The CentrisShared repository is NOT a standalone application, but rather a collection of components which are shared between other repositories (i.e. the LMS/SMS repos)

## How do I get set up? ##

To contribute to it, you just clone it, create a branch, write some awesome code, commit/push as needed, and then create a pull request when done.

This project is already referenced in the two main projects, i.e. LMS/SMS projects, so nothing needs to be done there, except updating by running the following command:

git submodule update --init

to ensure you've got the submodule.

## Documentation ##

This project uses grunt-ngdocs to generate documentation. The generation is currently a task in the CentrisSMS project, but might get moved to this repo. TODO