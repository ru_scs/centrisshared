"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisSemesterDropdown
 * @description
 * Creates a dropdown list of all semesters to filter search results. It has the following options:
 *
 * * multipleselect='true/false'    to have the option to select multiple semesters
 * * selected-filter='xxxx'         the variable in your scope you want to store the selected item/items
 * * on-update='xxxfunction()'      ng-click that calls xxxfunction.
 * * allow-clear='true'             to activate the option to clear the selected item from the filter.
 * * json-lang='courses.filter'     to add placeholder text to the dropdown list, simply create a varible
 *                                  in the language.json file in the app and use
 * * default-semester='false'       set this to false for no default semester chosen
 * * start='20123'                  specifies what should be the start semester, i.e. older semesters
 *                                  won't be displayed.
 *
 * @example
 * ```html
 * <centris-semester-dropdown
 *		ng-model='xxxx'
 *		multiple-select='false'
 *		on-update='updatefunction()'
 *		allow-clear='true'
 *		json-lang='courses.SemesterFilter'
 *		default-semester='false'>
 *	</centris-semester-dropdown>
 * ```
 * ```js
 * angular.module("**App", ['ui', 'sharedServices', 'ui.select'])
 * ```
 */
angular.module("sharedServices").directive("centrisSemesterDropdown",
function centrisSemesterDropdown($compile, SemestersResource, $translate) {
	var currentLanguage = $translate.use();
	return {
		restrict:   "EA",
		require:    "ngModel",
		replace:    true,
		transclude: true,
		scope: {
			onInit:   "&",
			onUpdate: "&",
			ngModel:  "=",
			start:    "="
		},
		link: function(scope, element, attributes) {
			var option       = "";
			var template     = "";
			var semesterName = "";
			var allowClear   = "";
			var required     = "";

			scope.currentSemester = [];

			scope.$watch("start", function(newValue, oldValue) {
				// Selects the current semester marked from the API
				SemestersResource.getAllSemesters(scope.start).success(function(data) {
					scope.allSemesters = data;
					for (var i = scope.allSemesters.length - 1; i >= 0; i--) {
						if (scope.allSemesters[i].Current === true ) {
							scope.currentSemester.push(scope.allSemesters[i].ID);
							if (attributes.defaultSemester !== "false" || attributes.defaultSemester === undefined) {
								scope.ngModel = scope.currentSemester;
							}
						}
					}
					scope.onInit({data: {semester: scope.ngModel[0], semesters: scope.allSemesters}});
				});
			}, true);

			// By default, the control get these class values,
			// but these can be overridden.
			var classVal = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classVal = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classVal = attributes.ngClass;
			}

			if (attributes.allowClear === "true") {
				allowClear = "{allowClear :true}";
			} else {
				allowClear = "{allowClear :false}";
			}

			if (currentLanguage === "is") {
				semesterName = "{{i.Name}}";
			} else {
				semesterName = "{{i.NameEN}}";
			}

			if (attributes.multipleSelect === "false" || attributes.multipleSelect === undefined) {
				option = "";
			} else {
				option = "multiple";
			}

			if (attributes.ngRequired || attributes.required) {
				required = "ng-required";
			}

			template +="<select class='" + classVal + "' " +
						" ng-click='onUpdate()' " +
						" ui-select2='" + allowClear + "' " +
						option +
						required +
						" ng-model='ngModel' " +
						" placeholder=\"{{ '" + attributes.jsonLang + "' | translate }}\">";
			template += "<option value=''></option>";
			template +=	"<option ng-repeat='i in allSemesters' value='{{i.ID}}'  >" + semesterName + "</option>";
			template +="</select>";

			element.append(template);
			$compile(element.contents())(scope);
		}
	};
});
