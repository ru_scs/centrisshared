"use strict";

/**
 * This is a API connection that gets all semesters and the first
 * in the list is the latest semester added.
 */
angular.module("sharedServices").factory("SemestersResource",
function SemestersResource(CentrisResource) {
	return {
		getAllSemesters: function(start) {
			// GET api/v1/semesters
			if (start) {
				return CentrisResource.get("semesters", "?start=:start", {start: start});
			} else {
				return CentrisResource.get("semesters");
			}
		}
	};
});
