"use strict";

describe("SemestersResource test", function() {
	beforeEach(module("sharedServices", "mockConfig"));

	var SemestersResource;
	var mockBackend;
	var baseApiUrl = "http://localhost:19357/api/v1/";

	beforeEach(function () {
		jasmine.addMatchers({
			toEqualData: function (expected) {
				return angular.equals(this.actual, expected);
			}
		});
	});

	beforeEach(inject(function(_SemestersResource_, _$httpBackend_) {
		SemestersResource = _SemestersResource_;
		mockBackend = _$httpBackend_;

		mockBackend.when("GET", baseApiUrl + "semesters").respond([{
			ID: "20141",
			Name: "Vorönn 2014",
			NameEN: "Spring 2014",
			DateBegin: "2014-01-03T00:00:00",
			DateFinish: "2014-05-30T00:00:00",
			Current: false
		}, {
			ID: "20133",
			Name: "Haustönn 2013",
			NameEN: "Fall 2013",
			DateBegin: "2013-08-16T00:00:00",
			DateFinish: "2013-12-31T00:00:00",
			Current: true
		}, {
			ID: "20132",
			Name: "Sumarönn 2013",
			NameEN: "Summer 2013",
			DateBegin: "2013-06-01T00:00:00",
			DateFinish: "2013-08-15T00:00:00",
			Current: false
		}]);
	}));

	it("should check the api", function () {
		mockBackend.expectGET(baseApiUrl + "semesters").respond({});
		spyOn(SemestersResource, "getAllSemesters").and.callThrough();
		SemestersResource.getAllSemesters();
		expect(SemestersResource.getAllSemesters).toHaveBeenCalled();
	});
});
