"use strict";

describe("centris semester dropdown directive", function() {
	var scope,
		compile,
		semestersResource,
		element,
		tran,
		language,
		semesterName,
		translater;

	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	beforeEach(function() {
		tran = {
			use: function(lang) {
				if (lang !== undefined) {
					language = lang;
				}
				return language;
			}
		};

		semestersResource = {
			getAllSemesters: function() {
				var toReturn = [{
					ID: "20141",
					Name: "Vorönn 2014",
					NameEN: "Spring 2014",
					DateBegin: "2014-01-03T00:00:00",
					DateFinish: "2014-05-30T00:00:00",
					Current: false
				}, {
					ID: "20133",
					Name: "Haustönn 2013",
					NameEN: "Fall 2013",
					DateBegin: "2013-08-16T00:00:00",
					DateFinish: "2013-12-31T00:00:00",
					Current: true
				}, {
					ID: "20132",
					Name: "Sumarönn 2013",
					NameEN: "Summer 2013",
					DateBegin: "2013-06-01T00:00:00",
					DateFinish: "2013-08-15T00:00:00",
					Current: false
				}];

				return {
					success: function(fn) {
						return fn(toReturn);
					}
				};
			}
		};

		translater = function() {
			return {
				translate: function(fn) {
					return fn({to: {match: "someWord"}});
				}
			};
		};

		semesterName = "";
		module(function($provide) {
			$provide.value("SemestersResource", semestersResource);
			$provide.value("$translate", tran);
			$provide.value("translateFilter", translater);
		});

		inject(function ($rootScope, $compile) {
			compile = $compile;
			scope = $rootScope.$new();
			scope.selectedSemester = "";
		});
	});

	xit("should check if current semester is in the scope", function() {
		inject(function() {
			tran.use("is");
			element = angular.element("<centris-semester-dropdown  multiple-select='false' " +
				"ng-model='selectedSemester' ></centris-semester-dropdown>");
			compile(element)(scope);
		});

		var iso = element.isolateScope();
		expect(iso.ngModel[0]).toBe("20133");
		expect(iso.ngModel[0]).not.toBe("20132");
	});

	xit("should check if the scope variable allSemesters contains our mock data", function() {
		inject(function() {
			tran.use("is");
			element = angular.element("<centris-semester-dropdown  multiple-select='false' " +
				"ng-model='selectedSemester' ></centris-semester-dropdown>");
			compile(element)(scope);
		});

		var iso = element.isolateScope();

		expect(iso.allSemesters[0].ID).toBe("20141");
		expect(iso.allSemesters[0].Current).not.toBe(true);
		expect(iso.allSemesters[2].NameEN).toBe("Summer 2013");
	});

	it("should create a multiple-select directive", function() {
		inject(function() {
			tran.use("is");
			element = angular.element("<centris-semester-dropdown  multiple-select='true' " +
				"ng-model='selectedSemester'></centris-semester-dropdown>");
			compile(element)(scope);
		});

		var dump = angular.mock.dump(element);
		expect(dump).toContain("multiple=\"\"");
	});

	it("should check if you can inject a language json object", function() {
		inject(function() {
			tran.use("is");
			element = angular.element("<centris-semester-dropdown  multiple-select='true' " +
				"ng-model='selectedSemester' json-lang='to.match' allow-clear='true'></centris-semester-dropdown>");
			compile(element)(scope);
		});
		var dump = angular.mock.dump(element);
		expect(dump).toContain("to.match");
	});
	it("should check if you can inject a language json object", function() {
		inject(function() {
			tran.use("en");
			element = angular.element("<centris-semester-dropdown  multiple-select='true' " +
				"ng-model='selectedSemester' json-lang='to.match' allow-clear='true'></centris-semester-dropdown>");
			compile(element)(scope);
		});
		var dump = angular.mock.dump(element);
		expect(dump).toContain("to.match");
	});
});
