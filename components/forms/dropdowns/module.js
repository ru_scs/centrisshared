"use strict";

/**
 * @ngdoc overview
 * @name forms.dropdowns
 * @description
 * This module contains application-specific dropdown controls, such that it should
 * be easy to fetch semesters, departments, majors, courses etc.
 */