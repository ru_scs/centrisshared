"use strict";

/**
 *
 * This resource is used to access a list of classrooms.
 *
 */
angular.module("sharedServices").factory("CentrisRoomResource",
function (CentrisResource) {
	return {
		getRooms: function getRooms() {
			// return all rooms, minimum seat count = 0
			return CentrisResource.get("rooms", null, {seatCount: 0});
		}
	};
});