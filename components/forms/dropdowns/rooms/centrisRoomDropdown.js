"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisRoomDropdown
 * @description
 * This dropdown allows the user to select a classroom.
 *
 * @example
 * ```html
 * <centris-room-dropdown
 *     ng-model="someRoomIDVariable"
 *     on-update="someFunction()"></centris-room-dropdown>
 * ```
 */
angular.module("sharedServices").directive("centrisRoomDropdown",
function ($compile, CentrisRoomResource) {
	return {
		restrict:   "EA",
		require:    "ngModel",
		replace:    true,
		transclude: true,
		scope: {
			onUpdate: "&",
			ngModel:  "="
		},
		link: function (scope, element, attributes) {
			var option     = "";
			var template   = "";
			var allowClear = "";
			var required   = "";
			CentrisRoomResource.getRooms().success(function (data) {
				scope.allRooms = data;
				if (scope.onLoaded) {
					scope.onLoaded ({
						allRooms: scope.allRooms
					});
				}
			});

			// By default, the control gets these class values,
			// but they can be overridden
			var classVal = "input-medium form-control";
			if (attributes["class"] !== undefined) {
				classVal = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classVal = attributes.ngClass;
			}

			if (attributes.allowClear === "true") {
				allowClear = "{allowClear :true}";
			} else {
				allowClear = "{allowClear :false}";
			}

			if (attributes.multipleSelect === "false" || attributes.multipleSelect === undefined) {
				option = "";
			} else {
				option = "multiple";
			}

			if (attributes.ngRequired || attributes.required) {
				required = "ng-required";
			}

			template = "<select class='" + classVal + "'' " +
						"ng-click='onUpdate()' " +
						"ui-select2='" + allowClear + "'' " +
						option + required +
						" ng-model='ngModel'>";
			template += "<option value=''></option>";
			template += "<option ng-repeat='room in allRooms' value='{{room.ID}}' >{{room.Name}}</option>";
			template += "</select>";
			element.append(template);
			$compile(element.contents())(scope);
		}
	};
});
