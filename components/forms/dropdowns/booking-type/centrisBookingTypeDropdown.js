"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisBookingTypedropdown
 * @description
 * This dropdown allows the user to select a type of booking when a classroom
 * is being booked.
 *
 * @example
 * ```html
 * <centris-booking-type-dropdown
 *	ng-model="model.someProperty" />
 * ```
 */
angular.module("sharedServices").directive("centrisBookingTypeDropdown",
function ($compile, $translate) {
	return {
		restrict:   "EA",
		require:    "ngModel",
		replace:    false,
		transclude: true,
		scope: {
			ngModel: "="
		},
		link: function (scope, element, attributes) {
			var includeRequired = "";
			var allowClear      = "";

			if (attributes.ngRequired !== undefined ||
				attributes.required !== undefined) {
				includeRequired = "ng-required";
			}

			// By default, the control gets these class values,
			// but they can be overridden
			var classValue = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classValue = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classValue = attributes.ngClass;
			}

			if (attributes.allowClear === "true") {
				allowClear = "{allowClear: true}";
			} else {
				allowClear = "{allowClear: false}";
			}

			// Hardcoded types of room bookings
			var template = "<select " +
				"class='" + classValue + "' " +
				"ui-select2='" + allowClear + "' " +
				includeRequired +
				" ng-model='ngModel'>" +
					"<option value='-3' translate='RoomBookingTypes.RoomBooking'></option>" +
					"<option value='-2' translate='RoomBookingTypes.RetakeExam'></option>" +
					"<option value='-1' translate='RoomBookingTypes.FinalExam'></option>" +
					"<option value='1' translate='RoomBookingTypes.Lecture'></option>" +
					"<option value='2' translate='RoomBookingTypes.Workshop'></option>" +
					"<option value='3' translate='RoomBookingTypes.Workshop2'></option>" +
					"<option value='4' translate='RoomBookingTypes.Message'></option>" +
					"<option value='5' translate='RoomBookingTypes.Appointment'></option>" +
				"</select>";

			element.append(template);
			$compile(element.contents())(scope);
		}
	};
});
