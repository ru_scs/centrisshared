"use strict";

/**
 * A resource object for course templates, used by the centrisCoursetemplateDropdown directive.
 */
angular.module("sharedServices").factory("CentrisCoursetemplateResource",
["CentrisResource", function (CentrisResource) {
	return {
		getCourseTemplates: function getCourseTemplates(departmentId) {
			return CentrisResource.get("coursetemplates", "?departmentid=:departmentId", {departmentId: departmentId});
		}
	};
}]);