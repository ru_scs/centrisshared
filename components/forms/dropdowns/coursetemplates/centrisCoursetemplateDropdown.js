"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisCoursetemplateDropdown
 * @description
 * This directive lists course templates, filtered by department (which __must__
 * be specified).
 *
 * @example
  * ```html
 * <centris-coursetemplate-dropdown
 *	department="someScopeVariable"
 *	ng-model="anotherScopeVariable" />
 * ```
 *
 * Optional attributes:
 *
 * * multiple/ngMultiple - the control will allow the user to select more than one coursetemplate.
 * * required/ngRequired - ensures the control has the 'required' flag.
 * * class - sets a CSS class for the control. If not specified, it will be set to 'input-medium'.
 *
 * The ng-model variable will contain the selection made by the user, and will
 * be a CourseTemplateDTO object, containing the selected course template.
 */
angular.module("sharedServices").directive("centrisCoursetemplateDropdown",
function ($compile, CentrisCoursetemplateResource) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			onChangeCourse: "&",
			ngModel:        "=",
			department:     "="
		},
		link: function link(scope, element, attributes) {
			var includeMultiple = "";
			if (attributes.multiple !== undefined ||
				attributes.ngMultiple !== undefined) {
				includeMultiple = " multiple";
			}

			var includeRequired = "";
			if (attributes.ngRequired !== undefined ||
				attributes.required !== undefined) {
				includeRequired = "ng-required";
			}

			var classValue = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classValue = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classValue = attributes.ngClass;
			}

			scope.$watch("department", function(newValue, oldValue) {
				scope.department = newValue;
				getCoursetemplates();
			}, true);

			function getCoursetemplates() {
				if (scope.department) {
					CentrisCoursetemplateResource.getCourseTemplates(scope.department).success(function(data) {
						scope.courseTemplates = data;
					});
				} else {
					// TODO: we should probably support this as well,
					// at least until we've got an ElasticSearch control which can handle this!
					// The problem is that this could be a query which is a bit
					// heavy.
				}
			}

			// TODO: IS/EN switch!!!

			var template = "<select " +
				"class='" + classValue + "' " +
				"ui-select2 " +
				includeMultiple +
				includeRequired +
				" ng-change='onChangeCourseEvent();'" +
				"ng-model='ngModel'>" +
					"<option ng-repeat='course in courseTemplates' value='{{course.CourseID}}'>" +
					"{{course.CourseID}} {{course.Name}}</option>" +
				"</select>";
			$compile(template)(scope, function(cloned, scope) {
				element.append(cloned);
			});

			scope.onChangeCourseEvent = function onChangeCourseEvent() {
				if (scope.onChangeCourse) {
					var courseTemplateID = scope.ngModel;
					if (courseTemplateID) {
						for (var i = 0; i < scope.courseTemplates.length; i++) {
							if (scope.courseTemplates[i].CourseID === courseTemplateID) {
								scope.onChangeCourse({course: scope.courseTemplates[i]});
								break;
							}
						}
					}
				}
			};
		}
	};
});
