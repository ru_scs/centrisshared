"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisMajorDropdown
 * @description
 * This dropdown allows the user to select a major.
 * It will always display a filtered list, i.e. filtered based
 * on a department. Therefore, you __must__ specify a departmentID,
 * which will be used to filter the list of majors. Note that you can
 * pass in a variable which is (for instance) linked to a centrisDepartmentDropdown
 * control, such that the user can select the department first, and then get a list
 * of majors belonging to that department.
 *
 * @example
 * ```html
 * <centris-major-dropdown
 *     ng-model="someMajorIDVariable"
 *     department="someDepartmentID"></centris-major-dropdown>
 * ```
 */
angular.module("sharedServices").directive("centrisMajorDropdown",
function centrisMajorDropdown($compile, CentrisMajorResource) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			department: "=",
			ngModel: "="
		},
		link: function(scope, element, attributes) {

			// By default, the control get these class values,
			// but these can be overridden.
			var classVal = "form-control input-medium";
			if (attributes["class"] !== undefined ) {
				classVal = attributes["class"];
			}

			var multiple = "";
			if (attributes.multiple !== undefined) {
				multiple = " multiple";
			}

			CentrisMajorResource.getMajorsByDepartment(scope.department).success(function(majors) {
				scope.majors = majors;
			});

			var template = "<select class='" + classVal + "' " +
				" ui-select2 " +
				" ng-model='ngModel' " +
				multiple +
				">" +
				"<option ng-repeat='major in majors' value='{{major.ID}}'>" +
				"{{major.Name}}" +
				"</option>" +
				"</select>";
			$compile(template)(scope, function(cloned, scope) {
				element.append(cloned);
			});
		}
	};
});
