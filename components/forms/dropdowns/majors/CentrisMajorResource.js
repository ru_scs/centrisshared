"use strict";

/**
 * This resource is used by centrisMajorDropdown to access a list of majors in a given department.
 */
angular.module("sharedServices").factory("CentrisMajorResource",
["CentrisResource",
function (CentrisResource) {
	return {
		getMajorsByDepartment: function getMajorsByDepartment(departmentID) {
			return CentrisResource.get("majors", "?departmentid=:departmentID", {departmentID: departmentID});
		}
	};
}]);