"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisTeacherDropdown
 * @description
 * This dropdown allows the user to select a teacher in a course.
 * The courseInstanceID must be passed into the control via the
 * courseinstanceid attribute, and the SSN of the teacher will be
 * passed into ng-model.
 *
 * @example
 * ```html
 * <centris-teacher-dropdown
 *		ng-model="theSelectedTeacherSSN"
 *		courseinstanceid="someScopeVariable">
 *	</centris-teacher-dropdown>
 * ```
 * ```js
 * angular.module("**App", ['ui', 'sharedServices', 'ui.select'])
 * ```
 */
angular.module("sharedServices").directive("centrisTeacherDropdown",
function centrisTeacherDropdown($compile, CentrisCourseResource, $translate) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			ngModel:          "=",
			onUpdate:         "&",
			courseinstanceid: "="
		},
		link: function link (scope, element, attributes) {
			var option     = "";
			var template   = "";
			var allowClear = "";
			var required   = "";

			function getTeachers() {
				CentrisCourseResource.getTeacherByCourseInstance(scope.courseinstanceid)
				.success(function (data) {
					scope.allTeachers = data;

					if (scope.allTeachers.length > 0) {
						scope.ngModel = scope.allTeachers[0].SSN;
					}
					if (scope.onLoaded) {
						scope.onLoaded ({
							allTeachers: scope.allTeachers
						});
					}
				});
			}

			scope.$watch("courseinstanceid", function(newValue, oldValue) {
				if (newValue) {
					getTeachers();
				}
			}, true);

			// By default, the control gets these class values,
			// but they can be overridden
			var classVal = "input-medium form-control";
			if (attributes["class"] !== undefined) {
				classVal = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classVal = attributes.ngClass;
			}

			if (attributes.allowClear === "true") {
				allowClear = "{allowClear :true}";
			} else {
				allowClear = "{allowClear :false}";
			}

			if (attributes.multipleSelect === "false" || attributes.multipleSelect === undefined) {
				option = "";
			} else {
				option = "multiple";
			}

			if (attributes.ngRequired || attributes.required) {
				required = "ng-required";
			}

			template = "<select class='" + classVal + "'' " +
						"ng-click='onUpdate()' " +
						"ui-select2='" + allowClear + "'' " +
						option + required +
						" ng-model='ngModel' ";
			template += "<option value=''></option>";
			template += "<option selected='selected' ng-repeat='teacher in allTeachers' ";
			template += "value='{{teacher.SSN}}' >{{teacher.FullName}}</option>";
			template += "</select>";
			element.append(template);
			$compile(element.contents())(scope);
		}
	};
});
