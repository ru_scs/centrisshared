"use strict";

describe("CentrisDepartmentResource test", function() {
	beforeEach(module("sharedServices", "mockConfig"));

	var CentrisDepartmentResource;
	var mockBackend;
	var baseApiUrl = "http://localhost:19357/api/v1/";

	beforeEach(function () {
		jasmine.addMatchers({
			toEqualData: function (expected) {
				return angular.equals(this.actual, expected);
			}
		});
	});

	beforeEach(inject(function(_CentrisDepartmentResource_, _$httpBackend_) {
		CentrisDepartmentResource = _CentrisDepartmentResource_;
		mockBackend = _$httpBackend_;

		mockBackend.when("GET", baseApiUrl + "?organization=").respond([{
			ID: 1,
			Name: "Tölvunarfræðideild",
			NameEN: "School of Computer Science",
			ChildDepartments: []
		}, {
			ID: 4,
			Name: "Stoðsvið",
			NameEN: "Support Services",
			ChildDepartments: [{
				ID: 14,
				Name: "Bókasafn og upplýsingaþjónusta",
				NameEN: "Library and Information Services",
				ChildDepartments: []
			}, {
				ID: 15,
				Name: "Stúdentaþjónusta",
				NameEN: "Student Services",
				ChildDepartments: []
			}]
		}, {
			ID: 5,
			Name: "Lagadeild",
			NameEN: "School of Law",
			ChildDepartments: []
		}]);
	}));

	it("should check the api", function () {
		mockBackend.expectGET(baseApiUrl + "?organization=").respond({});
		spyOn(CentrisDepartmentResource, "getAllDepartments").and.callThrough();
		CentrisDepartmentResource.getAllDepartments();
		expect(CentrisDepartmentResource.getAllDepartments).toHaveBeenCalled();
	});
});
