"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisDepartmentDropdown
 * @description
 * Creates a dropdown list of departments. It has the option of showing also the subdepartments
 * in a separate dropdown.
 * To use this directive you need to add sharedServices and ui.select as dependencies in the app.
 *
 * @example
 *
 * ```html
 *	<centris-department-dropdown
 *		sub-departments='true'
 *		ng-model='XXXX' - The ID of the selected department
 *		teaching-only="true"
 *		selected-filter-sub='YYYY'
 *		allow-clear='true'
 *		on-update-department='somefunction(department)'
 *		on-update-subdepartment='func(department)'
 *		json-dep-placeholder='key.value'
 *		json-sub-placeholder='key.value'
 *		all-departments='true'
 *		all-departments-placeholder='key.value'
 *		>
 *	</centris-department-dropdown>
 * ```
 * ```js
 * // In your module:
 * angular.module("**App", ['ui', 'sharedServices', 'ui.select']).
 * ```
 *
 * Selected department can be accessed in XXXX and returns the name of the department.
 * Selected sub department can be accessed in YYYY and returns the name of the department.
 * Avaible attribute options:
 *
 * * sub-departments='true/false' - true to show sub department dropdown false to hide
 * * ng-model='XXXX' - two-way binding of the ID(s) of the selected department(s)
 * * selected-filter-Sub='YYYY' - returns the sub departments of the selected department
 * * allow-clear='true' - allows the user to clear selected item
 * * all-departments='true' - set to true if you want the option "All departments" added (default is false)
 * * all-departments-placeholder='key.value' - the text to display for the "All departments" option (No default)
 * * on-update-department='somefunction(department)' - Adds a ng-change event when user changes selected item
 * * on-update-subdepartment='func(department)'
 * * json-dep-placeholder='key.value' - 'key.value' add to the json ->  "key": {"value": "Filter by Department"}
 * * json-sub-placeholder='key.value'
 * * teaching-only - Only include departments which are teaching departments
 * * multiple - Allows multiple selections
 */
angular.module("sharedServices").directive("centrisDepartmentDropdown",
function centrisDepartmentDropdown($compile, $translate, CentrisDepartmentResource) {
	var currentLanguage = $translate.use();
	return {
		restrict:   "EA",
		require:    "ngModel",
		replace:    false,
		transclude: true,
		scope: {
			onUpdateDepartment:    "&",
			onUpdateSubdepartment: "&",
			ngModel:               "=",
			selectedFilterSub:     "="
		},
		link: function($scope, element, attributes) {
			var template              = "";
			var departmentName        = "";
			var subDepartmentName     = "";
			var allowClear            = "";
			var includeAllDepartments = false;
			var includeMultiple       = "";
			var teachingOnly          = attributes.teachingOnly;
			$scope.subDepartments     = [];

			CentrisDepartmentResource.getAllDepartments(teachingOnly).success(function(data) {
				$scope.allDepartments = data;
			});

			var classValue = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classValue = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classValue = attributes.ngClass;
			}

			// Check language. We switch between the main language and english
			// based on what the $translate service tells us about the language in use.
			if (currentLanguage === "is") {
				departmentName    = "{{c.Name}}";
				subDepartmentName = "{{a.Name}}";
			} else {
				departmentName    = "{{c.NameEN}}";
				subDepartmentName = "{{a.NameEN}}";
			}

			// Should we allow the little 'x' next to a selected item?
			if (attributes.allowClear === "true") {
				allowClear = "{allowClear :true}";
			}

			// Note: this is the "All departments" options that could possibly be added to the dropdown.
			if (attributes.allDepartments === "true") {
				includeAllDepartments = true;
			}

			if (attributes.multiple !== undefined) {
				includeMultiple = " multiple";
			}

			var includeRequired = "";
			if (attributes.ngRequired ||
				attributes.required) {
				includeRequired = "ng-required";
			}

			// Build the template string:
			template +="<select class='" + classValue + "'" +
						" ng-change='onChangeDepartments();'" +
						" ng-model='ngModel'" +
						includeMultiple +
						includeRequired +
						" ui-select2='" + allowClear + "'";
			template += " placeholder=\"{{'" + attributes.jsonDepPlaceholder + "' | translate }}\">";
			template +="<option value=''></option>";
			if (includeAllDepartments === true) {
				template +="<option value='0' translate='" + attributes.allDepartmentsPlaceholder + "'></option>";
			}
			template += "<option ng-repeat='c in allDepartments' value='{{c.ID}}'>" + departmentName + "</option>";
			template += "</select>";

			// Check if subdepartments should load into the view:
			if (attributes.subDepartments === "true") {
				template +="<select class='" + classValue + "'" +
							" ng-change='onChangeSubdepartment();'" +
							" ng-model='selectedFilterSub'" +
							" ui-select2='" + allowClear + "'";
				template += " placeholder=\"{{'" + attributes.jsonSubPlaceholder + "' | translate }}\">";
				template +="<option value=''></option>";
				template +="<option ng-repeat='a in subDepartments' value='{{a.ID}}'>" + subDepartmentName + "</option>";
				template +="</select>";
			}

			$scope.onChangeDepartments = function onChangeDepartments() {
				// The value in the selected option is in
				// string format. We convert it to an object
				// before we pass it further.
				var departmentID = $scope.ngModel;
				if (departmentID !== undefined &&
					departmentID !== null &&
					departmentID.length > 0 ) {
					var iID = parseInt(departmentID, 10);
					var department;

					for (var i = 0; i < $scope.allDepartments.length; i++) {
						var d = $scope.allDepartments[i];
						if (d.ID === iID) {
							department = d;
							break;
						}
					}
					$scope.onUpdateDepartment({department: department});
					if (department === undefined || department === null) {
						return;
					}

					$scope.updateSubDepartments(department);
				}
			};

			$scope.onChangeSubdepartment = function onChangeSubdepartment() {
				var subDepartment = JSON.parse($scope.selectedFilterSub);
				$scope.onUpdateSubdepartment({department: subDepartment});
			};

			// Adds subdepartments into an array based on the selected department
			$scope.updateSubDepartments = function updateSubDepartments(parentDepartment) {
				$scope.subDepartments = [];
				angular.forEach($scope.allDepartments, function(dep) {
					if (parentDepartment.ID === dep.ID) {
						angular.forEach(dep.ChildDepartments, function(x) {
							this.push(x);
						},$scope.subDepartments);
					}
				});
			};

			element.append(template);
			$compile(element.contents())($scope);
		}
	};
});
