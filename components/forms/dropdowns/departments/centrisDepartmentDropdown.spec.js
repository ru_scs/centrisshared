"use strict";

describe("centris department dropdown directive", function() {
	var scope,
		compile,
		departmentFactory,
		element,
		tran,
		language,
		translateFilterProv,
		translate,
		translater;

	var toCopmile =  "<centris-department-dropdown sub-departments='true'";
	toCopmile +=" ng-model='department' selected-filter-Sub='subDepartment' allow-clear='true'";
	toCopmile +=" on-update-department='somefunction()' json-dep-placeholder='key.value' " +
		"json-sub-placeholder='key.value' ></centris-department-dropdown>";

	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	beforeEach(function() {
		tran = {
			use: function(lang) {
				if (lang !== undefined) {
					language = lang;
				}
				return language;
			}
		};

		departmentFactory = {
			getAllDepartments: function() {
				var toReturn = [{
					ID: 1,
					Name: "Tölvunarfræðideild",
					NameEN: "School of Computer Science",
					ChildDepartments: []
				}, {
					ID: 4,
					Name: "Stoðsvið",
					NameEN: "Support Services",
					ChildDepartments: [{
						ID: 14,
						Name: "Bókasafn og upplýsingaþjónusta",
						NameEN: "Library and Information Services",
						ChildDepartments: []
					}, {
						ID: 15,
						Name: "Stúdentaþjónusta",
						NameEN: "Student Services",
						ChildDepartments: []
					}]
				}, {
					ID: 5,
					Name: "Lagadeild",
					NameEN: "School of Law",
					ChildDepartments: []
				}];

				return {
					success: function(fn) {
						return fn(toReturn);
					}
				};
			}
		};
		translater = function() {
			return {
				translate: function(fn) {
					return fn({to: {dep: "someWord"}});
				}
			};
		};

		module(function($provide) {
			$provide.value("CentrisDepartmentResource", departmentFactory);
			$provide.value("$translate", tran);
			$provide.value("translateFilterProvider", translateFilterProv);
			$provide.value("translateFilter", translater);
		});

		inject(function ($rootScope, $compile, $translate) {
			compile = $compile;
			scope = $rootScope.$new();
			translate = $translate;
		});
	});

	it("should check if scope is isolated", function() {
		inject(function() {
			tran.use("is");
			element = angular.element(toCopmile);
			compile(element)(scope);
		});
		var iscope = element.isolateScope();
		expect(scope.allDepartments).toBe(undefined);
		expect(iscope.allDepartments[0].Name).toBe("Tölvunarfræðideild");
	});

	it("should return correct number of elements in the option list", function() {
		inject(function() {
			tran.use("is");
			element = angular.element(toCopmile);
			compile(element)(scope);
			scope.$digest();
		});
		var iscope = element.isolateScope();
		var directive = element.find("option");
		expect(directive.length).toEqual(5);
	});

	it("should verify that department name uses English option name ", function() {
		inject(function() {
			tran.use("en");
			element = angular.element(toCopmile);
			compile(element)(scope);
			scope.$digest();
		});
		var iscope = element.isolateScope();
		var directive = element.find("option");
		expect(angular.mock.dump(directive[1])).toContain("School of Computer Science");
	});

	it("should verify that department name uses Icelandic option name ", function() {
		inject(function() {
			tran.use("is");
			element = angular.element(toCopmile);
			compile(element)(scope);
			scope.$digest();
		});
		var iscope = element.isolateScope();
		var directive = element.find("option");
		expect(angular.mock.dump(directive[1])).toContain("Tölvunarfræðideild");
	});

	it("should add to element to subDepartments and verify that the correct item should be in the list", function() {
		inject(function() {
			tran.use("is");
			element = angular.element(toCopmile);
			compile(element)(scope);
			scope.$digest();
		});
		var iscope = element.isolateScope();
		var parent = iscope.allDepartments[1];
		iscope.updateSubDepartments(parent);
		expect(iscope.subDepartments[0].NameEN).toBe("Library and Information Services");
	});

	it("should create a multipleselect directive", function() {
		inject(function() {
			tran.use("is");
			var toCopmile =  "<centris-department-dropdown sub-departments='true'";
			toCopmile += " ng-model='department' selected-filter-Sub='subDepartment' allow-clear='true'";
			toCopmile += " on-update='somefunction()' json-dep-placeholder='key.value' " +
				"json-sub-placeholder='key.value' ></centris-department-dropdown>";
			element = angular.element(toCopmile);
			compile(element)(scope);
		});

		var dump = angular.mock.dump(element);
		expect(dump).toContain("ngRepeat: a in subDepartments -");
		expect(dump).toContain("c in allDepartments");
	});

	it("should create a single selected directive", function() {
		inject(function() {
			tran.use("is");
			var toCopmile =  "<centris-department-dropdown sub-departments='false'";
			toCopmile +=" ng-model='department' selected-filter-Sub='subDepartment' allow-clear='true'";
			toCopmile +=" on-update='somefunction()' json-dep-placeholder='key.value' " +
				"json-sub-placeholder='key.value' ></centris-department-dropdown>";
			element = angular.element(toCopmile);
			compile(element)(scope);
		});
		var dump = angular.mock.dump(element);
		expect(dump).not.toContain("ngRepeat: a in subDepartments -");
		expect(dump).toContain("c in allDepartments");
	});
});
