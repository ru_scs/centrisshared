"use strict";

/**
 * A resource object which allows us to access a list of departments. Used by the
 * centrisDepartmentDropdown directive.
 */
angular.module("sharedServices").factory("CentrisDepartmentResource",
function CentrisDepartmentResource(CentrisResource) {
	return {
		getAllDepartments: function getAllDepartments(teachingOnly) {
			if (teachingOnly === undefined) {
				return CentrisResource.get("departments", "?organization=");
			}
			return CentrisResource.get("departments", "?teachingOnly=:teachingOnly", {teachingOnly: teachingOnly});
		}
	};
});
