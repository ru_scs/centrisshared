"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisMajorTypeDropdown
 * @description
 * This dropdown allows the user to select a major type.
 *
 * FIX THIS!!
 * @example
 * ```html
 * <centris-major-type-dropdown
 *     ng-model="someMajorIDVariable"
 *     department="someDepartmentID"></centris-major-dropdown>
 * ```
 */
angular.module("sharedServices").directive("centrisMajorTypeDropdown",
function centrisMajorTypeDropdown($compile, CentrisMajorTypeResource) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			ngModel: "="
		},
		link: function(scope, element, attributes) {

			// By default, the control get these class values,
			// but these can be overridden.
			var classVal = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classVal = attributes["class"];
			}

			var multiple = "";
			if (attributes.multiple !== undefined) {
				multiple = " multiple";
			}

			CentrisMajorTypeResource.getMajorTypes().success(function (majorTypes) {
				scope.majorTypes = majorTypes;
			});

			var template = "<select class='" + classVal + "' " +
				" ui-select2 " +
				" ng-model='ngModel' " +
				multiple +
				">" +
				"<option ng-repeat='majorType in majorTypes' value='{{majorType.ID}}'>" +
				"{{majorType.Name}}" +
				"</option>" +
				"</select>";
			$compile(template)(scope, function(cloned, scope) {
				element.append(cloned);
			});
		}
	};
});
