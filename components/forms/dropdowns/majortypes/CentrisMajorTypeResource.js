"use strict";

/**
 * This resource is used by centrisMajorTypeDropdown to access a list of all major types.
 */
angular.module("sharedServices").factory("CentrisMajorTypeResource",
["CentrisResource",
function (CentrisResource) {
	return {
		getMajorTypes: function getMajorTypes() {
			return CentrisResource.get("majors", "types");
		}
	};
}]);
