"use strict";

/**
 * A resource object for course instances, used by the centrisCourseDropdown directive.
 */
angular.module("sharedServices").factory("CentrisCourseResource",
function CentrisCourseResource(CentrisResource) {
	return {
		getCourses: function getCourses(department, semester) {
			return CentrisResource.get("courses", "?department=:department&semester=:semester", {
				department: department,
				semester: semester
			});
		},

		getCoursesByTemplate: function getCoursesByTemplate(courseID, semester) {

			// The API has the option to specify start semester as "current":
			if (!semester) {
				semester = "current";
			}

			var param = {
				courseID: courseID,
				startSemester: semester
			};

			return CentrisResource.get("courses", ":courseID/courses?startSemester=:startSemester", param);
		},

		getTeacherByCourseInstance: function getTeachersByCourseInstance (courseInstanceID) {
			var param = {
				courseInstanceID: courseInstanceID
			};

			return CentrisResource.get("courses", ":courseInstanceID/teachers", param);
		}
	};
});
