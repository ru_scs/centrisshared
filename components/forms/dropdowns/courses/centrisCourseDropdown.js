"use strict";

/**
 * @ngdoc object
 * @name ui.dropdowns.centrisCourseDropdown
 * @description
 * This dropdown allows the user to select a course (or more specifically, a
 * course instance) from a dropdown. Since the list of courses can get quite
 * long, it is assumed that there is a filter on a department and on a
 * semester, i.e. the directive will always filter the courses based on these
 * two criteria (semester/department). This will ensure the list of courses
 * will be reasonably short.
 *
 * @example
 * ```html
 * <centris-course-dropdown
 *	semester="someScopeVariable"
 *	department="anotherScopeVariable"
 *	ng-model="yetAnotherScopeVariable" />
 * ```
 *
 * This dropdown can ALSO be used to filter all courseinstances based on CourseID
 * (i.e. a given course template) and (optionally) on a given start semester,
 * if that is specified then only courses from that semester and later will
 * be displayed.
 *
 * The ng-model variable will contain the selection made by the user, and will
 * be a CourseInstanceDTO object, containing the selected course instance.
 */
angular.module("sharedServices").directive("centrisCourseDropdown",
function ($compile, CentrisCourseResource, $translate) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			ngModel:        "=", // This variable will receive the CourseInstanceID of the selected course
			department:     "=", // The ID of the department to filter by (if any)
			semester:       "=", // The semester to filter courses by (if any) OR
			coursetemplate: "=", //
			onLoaded:       "&"
		},
		link: function link(scope, element, attributes) {
			var includeMultiple = "";
			if (attributes.multiple !== undefined) {
				includeMultiple = " multiple";
			}

			var includeRequired = "";
			if (attributes.ngRequired !== undefined ||
				attributes.required !== undefined) {
				includeRequired = "ng-required";
			}

			var classValue = "form-control input-medium";
			if (attributes["class"] !== undefined) {
				classValue = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classValue = attributes.ngClass;
			}

			scope.$watch("semester", function(newValue, oldValue) {
				scope.semester = newValue;
				getCourses();
			}, true);

			scope.$watch("department", function(newValue, oldValue) {
				scope.department = newValue;
				getCourses();
			}, true);

			scope.$watch("coursetemplate", function(newValue, oldValue) {
				scope.coursetemplate = newValue;
				getCourses();
			});

			function getCourses() {
				if (scope.semester &&
					scope.department) {
					CentrisCourseResource.getCourses(scope.department, scope.semester).success(function(data) {
						scope.courses = data;
						if (scope.onLoaded) {
							scope.onLoaded({courses:scope.courses});
						}
					});
				} else if (scope.coursetemplate) {
					// Note: the semester variable is optional.
					CentrisCourseResource.getCoursesByTemplate(scope.coursetemplate, scope.semester).success(function(data) {
						scope.courses = data;
						if (scope.onLoaded) {
							scope.onLoaded({courses: scope.courses});
						}
					});
				}
			}

			var template = "<select " +
				"class='" + classValue + "' " +
				"ui-select2 " +
				includeMultiple +
				includeRequired +
				"ng-model='ngModel'>" +
					"<option value='null'>" + $translate.instant("NoCourse") + "</option>" +
					"<option ng-repeat='course in courses' value='{{course.ID}}'>{{course.CourseID}} {{course.Name}}</option>" +
				"</select>";
			$compile(template)(scope, function(cloned, scope) {
				element.append(cloned);
			});
		}
	};
});
