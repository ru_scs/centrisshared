"use strict";

/**
 * @ngdoc object
 * @name forms.dropdowns.centrisGroupsDropdown
 * @description
 *
 *
 * @example
 * ```html
 * <centris-groups-dropdown
 *		ng-model="groups"
 *		courseinstanceid="courseID">
 *	</centris-groups-dropdown>
 *
 *
 *
 *
 */
angular.module("sharedServices").directive("centrisGroupsDropdown",
function centrisGroupsDropdown($compile, CentrisGroupsResource , $translate) {
	return {
		restrict: "E",
		require: "ngModel",
		scope: {
			ngModel:          "=",
			onUpdate:         "&",
			courseinstanceid: "="
		},
		link: function link (scope, element, attributes) {
			var option     = "";
			var template   = "";
			var allowClear = "";
			var required   = "";

			function getGroups() {
				CentrisGroupsResource.getGroups(scope.courseinstanceid)
				.success(function (data) {
					scope.allGroups = data;

					if (scope.allGroups.length > 0) {
						scope.ngModel = scope.allGroups[0].ID;
					}
					if (scope.onLoaded) {
						scope.onLoaded ({
							allGroups: scope.allGroups
						});
					}
				});
			}

			scope.$watch("courseinstanceid", function(newValue, oldValue) {
				if (newValue) {
					getGroups();
				}
			}, true);

			// By default, the control gets these class values,
			// but they can be overridden
			var classVal = "input-small form-control";
			if (attributes["class"] !== undefined) {
				classVal = attributes["class"];
			} else if (attributes.ngClass !== undefined) {
				classVal = attributes.ngClass;
			}

			if (attributes.allowClear === "true") {
				allowClear = "{allowClear :true}";
			} else {
				allowClear = "{allowClear :false}";
			}

			if (attributes.multipleSelect === "false" || attributes.multipleSelect === undefined) {
				option = "";
			} else {
				option = "multiple";
			}

			if (attributes.ngRequired || attributes.required) {
				required = "ng-required";
			}

			template = "<select class='" + classVal + "'' " +
						"ng-click='onUpdate()' " +
						"ui-select2='" + allowClear + "'' " +
						option + required +
						" ng-model='ngModel' ";
			template += "<option value=''></option>";
			template += "<option selected='selected' ng-repeat='group in allGroups' ";
			template += "value='{{group.ID}}' >{{group.GroupName}}</option>";
			template += "</select>";
			element.append(template);
			$compile(element.contents())(scope);
		}
	};
});
