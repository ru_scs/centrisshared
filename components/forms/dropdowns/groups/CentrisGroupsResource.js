"use strict";

/**
 * A resource object for course instances, used by the centrisCourseDropdown directive.
 */
angular.module("sharedServices").factory("CentrisGroupsResource",
function CentrisGroupsResource(CentrisResource) {
	return {
		getGroups: function getGroups(courseInstanceID) {
			return CentrisResource.get("courses", ":id/groups", {
				id: courseInstanceID
			});
		}

	};
});
