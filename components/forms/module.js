"use strict";

/**
 * @ngdoc overview
 * @name forms
 * @description
 * This module contains various form elements which are commonly used in the Centris project.
 * They can be used for input controls, validation etc.
 */