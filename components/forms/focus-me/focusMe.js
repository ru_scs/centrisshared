"use strict";

/**
 * @ngdoc object
 * @name forms.focusMe
 * @description
 * Directive "borrowed" from [here](http://stackoverflow.com/questions/14833326/how-to-set-focus-in-angularjs).
 * NOTE: for ui-bootstrap dialogs (using the $uibModal service), it should be enough to mark
 * the control which should receive focus when the dialog is displayed using the HTML5 autofocus
 * attribute. In other cases, this directive could come in handy.
 *
 * @example
 * ```html
 *     <!-- In your markup: -->
 *     <input focusMe="someVariable" ... etc.>
 * ```
 *
 * ```js
 *     // In your JavaScript controller:
 *     $scope.someVariable = true;
 *     // Should be set at the time when the control should receive focus.
 * ```
 */
angular.module("sharedServices").directive("focusMe",
function focusMe($timeout, $parse) {
	return {
		link: function(scope, element, attrs) {
			var model = $parse(attrs.focusMe);
			scope.$watch(model, function(value) {
				if (value === true) {
					$timeout(function() {
						var elem = element[0];
						if (elem.tagName.toLowerCase() === "input") {
							elem.select();
						} else if (elem.tagName.toLowerCase() === "select") {
							// Testing...
							elem.select2("focus", true);
						} else {
							elem.focus();
						}
					});
				}
			});

			element.bind("blur", function() {
				if (model.assign) {
					scope.$apply(model.assign(scope, false));
				}
			});
		}
	};
});
