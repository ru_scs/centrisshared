"use strict";

/**
 * @ngdoc object
 * @name forms.centrisValidate
 * @description
 * The centrisValidate directive allows you to specify a tooltip which will
 * be displayed if a given condition evaluates to true, such as "name is empty".
 * The example below shows usage with a text input, but this directive can be
 * used with a variety of other elements - in fact, anywhere where the uib-popover
 * directive is supported.
 *
 * NOTE: it requires version 0.14 or better of the ui-bootstrap library!
 *
 * @example
 * ```html
 * <input type="text"
 *     ng-model="whatever"
 *     centris-validate="language.Constant.To.Be.Displayed"
 *     validate-condition="some.Boolean.Property />
 * ```
 */
angular.module("sharedServices").directive("centrisValidate",
function centrisValidate($compile) {
	return {
		// Note: the following properties are all necessary, due to the fact that
		// we are creating a directive that will replace itself with other
		// directives, and needs therefore to be compiled early in the process.
		// See here: http://stackoverflow.com/questions/19224028/add-directives-from-directive-in-angularjs
		restrict: "A",
		replace:  false,
		terminal: true,
		priority: 1000,

		compile: function compile(element, attrs) {
			var value = attrs.centrisValidate;
			var condition = attrs.validateCondition;
			element.attr("uib-popover", "{{'" + value + "' | translate}}");
			element.attr("popover-placement", "right");
			element.attr("popover-trigger", "none");
			element.attr("popover-is-open", condition);
			element.attr("popover-class", "centrisValidate");
			element.removeAttr("centris-validate");
			element.removeAttr("data-centris-validate");
			element.removeAttr("validate-condition");
			element.removeAttr("data-validate-condition");
			return {
				pre: function preLink(scope, iElement, iAttrs, controller) {  },
				post: function postLink(scope, iElement, iAttrs, controller) {
					$compile(iElement)(scope);
				}
			};
		}
	};
});
