"use strict";

/**
* @ngdoc object
* @name forms.FileuploadValidation
*
* @description
* This directive deals with the validation of files.
* You must have file-valid inside your input tag to make use of this directive.
*
* Directive source http://plnkr.co/edit/3TZCMUb9XXPGP5ifHf6x?p=preview
*
* @example
* ```html
*	<input type="file"
	name="file"
	file-model="File"
	ng-model="model.file"
	file-valid
	required/>
* ```
*/
angular.module("sharedServices").directive("fileValid",
function fileValid() {
	return {
		require: "ngModel",
		link: function(scope,el,attrs,ngModel) {
			el.bind("change", function() {
				scope.$apply(function() {
					ngModel.$setViewValue(el.val());
					ngModel.$render();
				});
			});
		}
	};
});
