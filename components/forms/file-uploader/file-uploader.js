"use strict";

/**
*	@ngdoc object
*	@name forms.fileUploader
*
*	@description
*	To upload any kind of file with drag-and drop or select.
*	The directive utilize ng-file-upload directive from
*	https://github.com/danialfarid/ng-file-upload.
*	To use add a valuation function and a recieve function
*	to a scope variable in the parent controller
*	like so:
*
*		$scope.valfunc = function(value) {
*			return true;
*		};
*
*		$scope.recfunc = function(value) {
*			$scope.putOnCanvas(value);
*		};
*
*	The valuation function should check if the file is on
*	format the file should be (jpg/pdf/zip... etc).
*	And the recfunc should be a callback function that does
*	something to the file.
*
*	Notes from author:
*		This can be tweaked for better useability. Like giving it
*	the ability to use a different class. It could be done by doing
*	<div ngf-drop ngf-select ng-model='file' class='{{someClassOrClasses}}'>
*	Notice the {{someClassOrClasses}} text.
*	Which will be a string in the parentCtrl. If done, please add the string 'col-md-12 dropbox'
*	to imgCropperCtrl to the someClassOrClasses scope variable used.
*	@example
*	```html
*	<file-uploader valfunc="someValuationFunc" recfunc="someRecieveFunc"></file-uploader>
*	```
*/
angular.module("sharedServices").directive("fileUploader",
function ($compile, graphTypes, $window) {
	return {
		restrict: "E",
		scope: {
			valfunc: "=valfunc",
			recfunc: "=recfunc"
		},
		// TODO: move to translations!!!
		template: "<div ngf-drop ngf-select ng-model='file' " +
		"ngf-drag-over-class='dragover' class='col-md-12 drop-box'>" +
			"<span translate='ProfilePage.UploadNewPicture' /></div>",
		link: function(scope, element, attrs) {
			scope.$watch("file", function(value, $event) {
				if (value && value.length > 0) {
					if (scope.valfunc(value)) {
						scope.recfunc(value);
					} else {
						scope.file = null;
					}
				}
			});
		}
	};
});
