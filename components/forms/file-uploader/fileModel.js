"use strict";

// Directive source: http://jsfiddle.net/JeJenny/ZG9re/

angular.module("sharedServices").directive("fileModel",
function ($parse) {
	return {
		restrict: "A",
		link: function (scope, elem, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSet = model.assign;

			elem.bind("change", function () {
				scope.$apply(function () {
					modelSet(scope, elem[0].files);
				});
			});
		}
	};
});
