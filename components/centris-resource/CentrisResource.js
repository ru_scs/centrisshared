"use strict";

/**
 * @ngdoc service
 * @name general.GenericResource
 * @description
 * A common resource object, used by other resource objects which are specialized.
 */
angular.module("sharedServices").provider("GenericResource",
function GenericResource() {
	this.$get = ["$http", function($http) {
		return {
			query: function(url, method, app, path, params, data, options) {
				if (!path) {
					path = "";
				}
				if (!params) {
					params = {};
				}
				if (!data) {
					data = {};
				}
				method = method.toLowerCase();

				// Assemble full path
				var fullpath = url + app + "/" + path;
				var extraParams = [];

				// Go through all properties in parameter object
				for (var o in params) {
					// If property for :someparam is not found
					if (fullpath.indexOf(":" + o) === -1) {
						// If property name is 'params', then we use the whole value
						if (o === "params") {
							if (params[o] !== "") {
								extraParams.push(params[o]);
							}
						} else {
							// Add property name and value to parameter array
							extraParams.push(o + "=" + encodeURIComponent(params[o]));
						}
					} else {
						// If property for :someparam is found, we replace it in the full path
						if (!params[o]) {
							params[o] = "";
						}

						fullpath = fullpath.replace(":" + o, params[o]);
					}
				}

				// Append parameters to the full path
				if (extraParams.length > 0) {
					fullpath += "?" + extraParams.join("&");
				}

				// Return the $http request object
				return $http[method](fullpath, data, options);
			},

			// Adds a Authorization token, which will be used in
			// all requests hereafter:
			setPermanentAuthHeader: function (token) {
				var authz = "Bearer" + " " + token;
				$http.defaults.headers.common.Authorization = authz;
			}
		};
	}];
});

/**
 * @ngdoc service
 * @name general.CentrisResource
 * @description
 * Generic Centris tailored implementation of $http provider
 * Sends a HTTP request to API and responds with data and resolves promise in callback.
 *
 * Responds with three functions: then(), success(), error()
 * If you need to work with a promise, use then() - otherwise use success()
 *
 * === Parameters ===
    - METHOD: GET/POST/PUT/DELETE
    - APP: The path in the API following api/v1/ - for example: students, courses, rooms
    - PATH: Path following the app - for example: :ssn/timeline
    - PARAMS: Resolves variables in path OR adds parameters to URL - for example: {ssn: '1407883039'}
        - 3 types:
            - Property resolves a variable in path
                   Example: {ssn:'1407883039'} will replace :ssn in path to: 1407883039
            - Property not found in path, then it will be added as a parameter in URL
                   Example: {param1: 'value1'} will be: ?param1=value1
            - Property 'params' is a finished query string
                   Example: {params: 'param1=value1&param2=value2'} will be: ?param1=value1&param2=value2
 * - DATA: Payload data (like creating new object with POST, then put the object as a payload)
 *
 * @example
 * ```js
 *  CentrisResource.query(
        'get',
        'students',
        ':ssn',
        {ssn: '1407883039', param1: 'value1', param2: 'value2'},
        [optional payload data]
    )
    .success(function(data) {
        // do something with data
    })
    .error(function(data, status, headers, config) {
        // do something with error
    });
 * ```
 *
 *  // HTTP GET request will be sent to:
 *  http://server/api/v1/students/1407883039?param1=value1&param2=value2
 *
 */
angular.module("sharedServices").provider("CentrisResource", function () {
	this.$get = ["ENV", "GenericResource", function (ENV, GenericResource) {

		return {
			query: function(method, app, path, params, data) {
				return GenericResource.query(ENV.apiEndpoint, method, app, path, params, data);
			},
			get: function(app, path, params, data) {
				return GenericResource.query(ENV.apiEndpoint, "get", app, path, params, data);
			},
			post: function(app, path, params, data, isFormdata) {
				var options;

				if (isFormdata) {
					options = { transformRequest: angular.identity, headers: { "Content-Type": undefined } };
				}

				return GenericResource.query(ENV.apiEndpoint, "post", app, path, params, data, options);
			},
			put: function(app, path, params, data, isFormdata) {
				var options;

				if (isFormdata) {
					options = { transformRequest: angular.identity, headers: { "Content-Type": undefined } };
				}

				return GenericResource.query(ENV.apiEndpoint, "put", app, path, params, data, options);
			},
			remove: function(app, path, params, data) {
				return GenericResource.query(ENV.apiEndpoint, "delete", app, path, params, data);
			},
			setAuthorizationHeader: function (token) {
				GenericResource.setPermanentAuthHeader(token);
			}
		};
	}];
});

angular.module("sharedServices").provider("GenesisApiResource", function () {
	this.$get = ["ENV", "GenericResource", function (ENV, GenericResource) {
		return {
			query: function(method, app, path, params, data) {
				return GenericResource.query(ENV.genesisEndpoint, method, app, path, params, data);
			}
		};
	}];
});

angular.module("sharedServices").provider("GenesisMvcResource", function () {
	this.$get = ["ENV", "GenericResource", function (ENV, GenericResource) {
		return {
			query: function(method, app, path, params, data) {
				return GenericResource.query(ENV.genesisMVC, method, app, path, params, data);
			}
		};
	}];
});