"use strict";

/*
* Service that talks to the authorization server.
*
* We might want to refactor this to use the generice resource dude here above
* but this will do for now.
* The reason why this has not already been done is probably because that one
* will try to automatically inject the token into the request, but there is
* no token valid currently.
*/
angular.module("sharedServices").factory("TokenResource",
function ($http, ENV, $q) {
	return {
		getToken: function (username, password) {
			var deferred = $q.defer();

			var payload = {
				user: username,
				pass: password
			};

			$http({
				method: "POST",
				url: ENV.apiEndpoint + "login",
				data: payload
			}).success(function (data) {
				if (data && data !== "") {
					deferred.resolve(data);
				} else {
					deferred.reject();
				}
			}).error(function (data) {
				deferred.reject(data);
			});
			return deferred.promise;
		}
	};
});
