"use strict";

angular.module("sharedServices").factory("TokenService",
function (localStorageService) {
	return {
		setToken: function setToken(token) {
			localStorageService.set("Token", token);
		},

		getToken: function getToken() {
			return localStorageService.get("Token");
		},

		removeToken: function removeToken() {
			localStorageService.remove("Token");
		}
	};
});