"use strict";

/**
 * The application module for the login component. A separate module is needed,
 * since it defines a separate state/route.
 */
angular.module("loginApp", ["sharedServices", "ui.router"])
.config(function ($stateProvider) {
	$stateProvider
	.state("login", {
		url: "/login",
		templateUrl: "components/login/login.tpl.html",
		controller: "LoginController",
		permission: "public"
	});
});