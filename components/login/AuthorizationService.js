"use strict";

angular.module("sharedServices").factory("AuthenticationService",
function ($q, TokenResource, TokenService, UserService) {
	// Private variables:
	var _nextState;
	var _nextStateParam;

	// Public service
	return {
		// Logs in a user by asking the TokenResource to fetch an access token.
		// TokenService then takes care of storing the token if successful.
		login: function login (username, password) {
			var deferred = $q.defer();
			TokenResource.getToken(username, password).then(function (userData) {
				// Save token:
				TokenService.setToken(userData.AccessToken);
				UserService.setUserInfo(userData);
				deferred.resolve(userData);
			}, function (reason) {
				deferred.reject(reason);
			});
			return deferred.promise;
		},

		logout: function logout () {
			TokenService.removeToken();
		},
		// Returns true if token is found, false otherwise.
		isLoggedIn: function isLoggedIn () {
			var token = TokenService.getToken();
			return token && token.length > 0;
		},

		getToken: function getToken() {
			return TokenService.getToken();
		},

		// These functions are needed, due to the following scenario:
		// * user has a non-root link to the application (bookmark, sent from
		//   another user etc.)
		// * there is no valid access token stored currently
		// In this case, we store the state of the next URL the user is about
		// to view, in case a request will fail due to lack of authorization.
		setNextState: function setNextState (state, param) {
			// Remember the next url to be able to redirect to it after a
			// successful login. However, we don't want to redirect back to the
			// login page after login, so we ignore this in the case the last
			// request is the login window itself.
			if (state.url !== "/login") {
				_nextState      = state;
				_nextStateParam = param;
			}
		},
		getNextState: function getNextState () {
			return _nextState;
		},
		getNextStateParam: function getNextStateParam () {
			return _nextStateParam;
		}
	};
});