"use strict";

angular.module("loginApp").controller("LoginController",
function ($scope, $rootScope, $state, $location, AuthenticationService) {
	var buttonTexts = {
		login:    "login.Login",
		active:   "login.Active",
		loggedIn: "login.LoggedIn"
	};

	// Variables
	$scope.loginStatus     = AuthenticationService.isLoggedIn();
	$scope.error           = false;
	$scope.loginButtonText = buttonTexts.login;
	$scope.usernameFocus   = true;
	$scope.user = {
		username: {
			error: false,
			value: ""
		},
		password: {
			error: false,
			value: ""
		}
	};

	// Public methods
	$scope.login = function () {
		if (validateInput()) {
			$scope.loginButtonText = buttonTexts.active;
			$scope.loginInProgress = true;
			AuthenticationService.login($scope.user.username.value, $scope.user.password.value)
			.then(function (data) {
				$scope.loginInProgress = false;
				$scope.loginStatus = AuthenticationService.isLoggedIn();
				$rootScope.$broadcast("loginSuccess");
				var state = AuthenticationService.getNextState();
				if (!state || state.url === "/login") {
					$location.path("/");
				} else {
					$state.go(state, AuthenticationService.getNextStateParam());
				}
			}, function (reason) {
				$scope.loginInProgress = false;
				$scope.error = true;
				$scope.loginButtonText = buttonTexts.login;
			});
		}
	};

	// Helper methods

	// Validates the users input and sets scope.error to be true if
	// there are some errors.
	function validateInput () {
		$scope.user.username.error = $scope.user.username.value === "" ? true : false;
		$scope.user.password.error = $scope.user.password.value === "" ? true : false;
		if ($scope.user.username.error || $scope.user.password.error) {
			$scope.error = true;
			return false;
		} else {
			$scope.error = false;
			return true;
		}
	}
});
