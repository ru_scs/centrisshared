"use strict";

/*
 * A do-it-all HTTP interceptor that currently does the following:
 *  - adds the "Authorization" token to each request.
 *  - listens to 401 (Unauthorized) response code and redirects to
 *    login page in that case.
 */
angular.module("sharedServices").factory("centrisAuthInterceptor",
function ($q, $location, TokenService, ENV) {
	return {
		request: function(config) {
			if (config.url.indexOf(ENV.gradesEndpoint) === 0 ||
				config.url.indexOf(ENV.apiEndpoint) === 0) {
				config.headers.Authorization = "Bearer " + TokenService.getToken();
			}
			return config;
		},

		responseError: function (response) {
			if (response.status === 401) { // UNAUTHORIZED
				// Intercept and redirect to login page:
				$location.path("/login");
			}
			return $q.reject(response);
		}
	};
});