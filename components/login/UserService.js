"use strict";

/*
 * This service takes the data passed to us during login,
 * i.e. data about the user which logged on, and ensures
 * it is available to other parts of the system.
 */
angular.module("sharedServices").factory("UserService",
function (localStorageService, $window) {
	return {
		setUserInfo: function setUserInfo(userInfo) {
			localStorageService.set("FullName",   userInfo.FullName);
			localStorageService.set("SSN",        userInfo.SSN);
			localStorageService.set("Department", userInfo.Department);
			localStorageService.set("DefaultLanguage", userInfo.DefaultLanguage);

			// Also, let trackJs know who the current user is:
			if ($window.trackJs) {
				$window.trackJs.configure({
					userId: userInfo.UserName
				});
			}
		},
		getFullName: function getFullName() {
			return localStorageService.get("FullName");
		},
		getUserSSN: function getUserSSN() {
			return localStorageService.get("SSN");
		},
		getUserDepartment: function getUserDepartment() {
			return localStorageService.get("Department");
		},
		getUserLanguage: function getUserLanguage() {
			var lang = localStorageService.get("DefaultLanguage");
			if (lang === undefined || lang === null || lang === "") {
				lang = "is"; // TODO: we may want to configure this elsewhere!
			}
			return lang;
		},
		setUserLanguage: function setUserLanguage(lang) {
			localStorageService.set("DefaultLanguage", lang);
		},
		getValue: function getValue(valueName, defaultValue) {
			var result = localStorageService.get(valueName);
			if (result === null || result === undefined) {
				return defaultValue;
			}
			return result;
		},
		setValue: function setValue(valueName, value) {
			localStorageService.set(valueName, value);
		},
		reset: function reset() {
			localStorageService.remove("FullName");
			localStorageService.remove("SSN");
			localStorageService.remove("Department");
			if ($window.trackJs) {
				$window.trackJs.configure({
					userId: ""
				});
			}
		}
	};
});