"use strict";

angular.module("sharedServices").directive("centrisWysiwyg",
function centrisWysiwyg() {
	return {
		restrict: "E",
		templateUrl: "components/centris-wysiwyg/centris-wysiwyg.tpl.html",
		scope: {
			ngModel: "="
		},
		link: function link (scope, element, attrib) {
		}
	};
});
