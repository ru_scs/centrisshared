"use strict";

/*
*moduleHeader
*--
*This directive handles the main headings in submodules, which contains not only the title of the
*module but also possibly contents such as buttons and filters.
*--
*Note: The directive currently uses the translate attribute described in another directive, and
*		is marked with the following classes:
*										*module-header-container
*--
** USAGE EXAMPLE (using students module)
*--------------------------------------------------------
*	<centris-module-header module="students">
		<button class = "testbutton"></button>
*	</centris-module-header>
*--------------------------------------------------------
*	This will result in the following markup:
*--------------------------------------------------------
*		<div class='module-header-container'>
*			<div>
*				h2 translate='students.Title'></h2>
*			</div>
*			<div ng-transclude>
*				<button class="testbutton"></button>
*			</div>
*		</div>
*--------------------------------------------------------
* Essentially, the directive will spew out two things:
*		*	A header, containing the title of the module
			after it has been translated using the
			translate directive
		*	A div, containing the contents which were placed
			between the opening and closing tags of this
			directive (ie, <centris-module-header>).
*--------------------------------------------------------
*/
angular.module("sharedServices").directive("centrisModuleHeader",
function centrisModuleHeader() {
	return {
		restrict: "EA",
		transclude: true,
		template: "<div class='module-header-container'><div><h2 translate='{{module}}.Title'>" +
			"</h2></div> <div ng-transclude></div ></div>",
		replace: true,
		link: function link (scope, element, attrib) {
			scope.module = attrib.module;
		}
	};
});

/*
 * Note: this directive will hopefully replace the one above. In the meantime,
 * they will coexist (peacefully).
 */
angular.module("sharedServices").directive("centrisPageHeader",
function centrisPageHeader() {
	return {
		restrict: "E",
		transclude: true,
		template:	"<div class='row centris-page-header'>" +
						"<div class='col-sm-12'>" +
							"<h2 translate='{{pageTitle}}'></h2>" +
							"<div class='centris-header-controls' ng-transclude>" +
						"</div>" +
					"</div>",
		replace: true,
		link: function link (scope, element, attrib) {
			scope.pageTitle = attrib.pageTitle;
		}
	};
});
