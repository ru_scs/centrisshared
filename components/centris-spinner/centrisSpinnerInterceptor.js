"use strict";

angular.module("sharedServices").factory("centrisSpinnerInterceptor",
["$q", "$rootScope",
function ($q, $rootScope) {
	return {
		request: function (request) {
			if ($rootScope.centrisSpinnerVisible === undefined) {
				$rootScope.centrisSpinnerVisible = 0;
			}
			$rootScope.centrisSpinnerVisible++;
			return request;
		},

		response: function (response) {
			$rootScope.centrisSpinnerVisible--;
			return response;
		},

		responseError: function (response) {
			$rootScope.centrisSpinnerVisible--;
			return $q.reject(response);
		}
	};
}]);
