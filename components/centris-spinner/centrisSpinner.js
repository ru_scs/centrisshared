"use strict";

/**
 * @ngdoc object
 * @name general.centrisSpinner
 * @description
 * A simple directive which wraps a spinner. By default, the spinner should
 * be hidden, but to show it, you can set the centrisSpinnerVisible variable
 * on the rootScope to true (and then back to false when you are done). Do
 * note however, that this variable should only be controlled by a special
 * $http interceptor, and should not be tinkered with.
*/
angular.module("sharedServices").directive("centrisSpinner",
function centrisSpinner() {
	return {
		restrict: "E",
		template: "<div class='centrisSpinner' ng-show='centrisSpinnerVisible'>" +
			"<i class='fa fa-spinner fa-spin'></i>" +
			"</div>"
	};
});
