"use strict";

angular.module("sharedServices").factory("MockHelpers",
function MockHelpers() {
	return {
		mockHttpPromise: function mockHttpPromise(value, isSuccessful) {
			return {
				success: function(fn) {
					if (isSuccessful) {
						fn(value);
					}

					return {
						error: function(fn) {
							if (!isSuccessful) {
								fn();
							}
						}
					};
				},
				// We throw in a "regular" then() promise method
				// for good measure, sometimes that is used instead
				// (although there should be consistency in the codebase
				// about which method is preferred).
				then: function (fnSuccess, fnError) {
					if (isSuccessful) {
						fnSuccess({ data: value });
					} else {
						fnError();
					}
				}
			};
		}
	};
});
