"use strict";

/**
 * @ngdoc overview
 * @name dialogs
 * @description
 * This module contains application-specific directives which are aimed at
 * helping developers write dialogs which have a consistent look'n'feel.
 * It also contains some commonly (or less-commonly-but-still-more-than-once)
 * used dialogs.
 */