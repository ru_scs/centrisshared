"use strict";

angular.module("sharedServices").controller("CentrisErrorDlgController",
function CentrisErrorDlgController($scope, $window) {
	// When the user clicks on the screenshot button, this function will
	// take a screenshot and display it for the user,
	$scope.showDialog    = true;
	$scope.imageCaptured = false;
	$scope.model = {
		Message: "",
		Image: ""
	};

	$scope.errors = {
		MessageMissing: false
	};

	$scope.capture = function() {
		// Hide the modal to take the screenshot.
		$scope.showDialog = false;
		setTimeout(function() {
			$window.html2canvas(document.body, {
				onrendered: function(canvas) {
					var img = canvas.toDataURL();
					$scope.model.Image   = img;
					$scope.imageCaptured = true;
					$scope.showDialog    = true;
					// Display the screenshot in the modal.
					$scope.screenshot = "<img src=" + img + " height='312' width='555'/>";
					if (!$scope.$$phase) {
						$scope.$apply();
					}
				}
			});
		},500);
	};

	$scope.validate = function validate() {
		$scope.errors = {};
		if ($scope.model.Message.length < 1) {
			$scope.errors.MessageMissing = true;
			return;
		}

		$scope.$close($scope.model);
	};

	$scope.onChangeMessage = function onChangeMessage() {
		$scope.errors = {};
	};
});
