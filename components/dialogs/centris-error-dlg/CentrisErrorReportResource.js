"use strict";

/**
 * This resource handles all requests made by the centrisErrorDlg component.
 */
angular.module("sharedServices").factory("CentrisErrorReportResource",
function (CentrisResource) {
	return {
		sendReport: function sendReport(report) {
			return CentrisResource.post("errorreports", "", {}, report);
		}
	};
});