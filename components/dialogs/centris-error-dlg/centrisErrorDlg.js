"use strict";

/**
 * @ngdoc service
 * @name dialogs.centrisErrorDlg
 * @description
 * This service takes care of giving the user a chance to report errors.
 * It will allow the user to take a screenshot and send that along with
 * a message from the user.
 *
 * @example
 * ```js
 *    centrisErrorDlg.show();
 * ```
 */
angular.module("sharedServices").factory("centrisErrorDlg",
function centrisErrorDlg($uibModal, CentrisErrorReportResource, centrisNotify) {
	return {
		show: function show() {
			var modalInstance = $uibModal.open({
				templateUrl: "components/dialogs/centris-error-dlg/centris-error-dlg.tpl.html",
				size:        "lg",
				backdrop:    false, // So when we hide the dialog and take the screenshot we can see the page uninterrupted
				controller:  "CentrisErrorDlgController"
			});

			modalInstance.result.then(function(report) {
				CentrisErrorReportResource.sendReport(report).success(function(result) {
					centrisNotify.success("ErrorReport.Completed");
				}).error(function(data, status) {
					centrisNotify.error("ErrorReport.Failed");
				});
			});
		}
	};
});