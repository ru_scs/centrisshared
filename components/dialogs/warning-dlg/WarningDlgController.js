"use strict";

angular.module("sharedServices").controller("WarningDlgController",
function WarningDlgController($scope, warningConfig) {
	$scope.model = {
		Message: warningConfig.message,
		Title:   warningConfig.title
	};
});