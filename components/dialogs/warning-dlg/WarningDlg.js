"use strict";

angular.module("sharedServices").factory("WarningDlg",
function WarningDlg($uibModal) {
	return {
		show: function show(msg, title) {
			var modalInstance = $uibModal.open({
				controller: "WarningDlgController",
				templateUrl: "components/dialogs/warning-dlg/warning-dlg.tpl.html",
				resolve: {
					warningConfig: function() {
						return {
							message: msg,
							title: title
						};
					}
				}
			});

			return modalInstance.result;
		}
	};
});
