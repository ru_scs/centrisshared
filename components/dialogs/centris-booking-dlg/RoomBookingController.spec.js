'use strict';

describe('RoomBookingController', function () {
	beforeEach(module('sharedServices'));
	var scope, controller, createController;

	var bookings = [
		{
			ID: 1,
			Room: 'M101',
			Description: 'Fyrirlestur'
		}
	];

	var mockUserSettings = {
		getDefaultDepartment: function () {
			return 1;
		}
	};

	var mockRoomBookingResourceSuccess = {
		postBooking: function (booking, listOfBookings, recurrence) {
			return {
				success: function (fn) {
					fn (bookings);
					return {
						error: function (errorFn) { }
					};
				}
			};
		}
	};

	var errorMsg = 'Could not book this room';

	var mockRoomBookingResourceError = {
		postBooking: function (booking, listOfBookings, reccurrence) {
			return {
				success: function (fn) {
					return {
						error: function (errorFn) {
							errorFn (errorMsg);
						}
					};
				}
			};
		}
	};

	var mockModalParamDefined = {
		start: moment().hour(10).minute(20),
		end:   moment().hour(11).minute(55),
		room:  {
			ID:   207,
			Name: 'M101'
		},
		courseInstance: {
			ID: 200,
			CourseID: 'T-207-GAG'
		},
		course: {
			course: {
				Name: 'Gagnaskipan'
			}
		}
	};

	var mockModalParamUndefined = {
		start:          undefined,
		end:            undefined,
		room:           undefined,
		courseInstance: undefined,
		course:         undefined
	};

	var mockModalParamRoomDefined = {
		start: moment().hour(10).minute(20),
		end:   moment().hour(11).minute(55),
		room:  {
			ID:   207,
			Name: 'M101'
		},
		courseInstance: undefined,
		course:         undefined
	};

	var mockModalParamCourseDefined = {
		start: moment().hour(10).minute(20),
		end:   moment().hour(11).minute(55),
		room:  undefined,
		courseInstance: {
			ID: 200,
			CourseID: 'T-207-GAG'
		},
		course: {
			course: {
				Name: 'Gagnaskipan'
			}
		}
	};

	var mockTranslate = {
		instant: function (string) {}
	};

	var mockCentrisNotify = {
		success: function (msg) {},
		error:   function (msg) {}
	};

	var mockFormatEventsForCalendar = {
		formatEvents: function (bookingList) {}
	};

	var mockDateTimeService = {
		dateFormat: function () {},
		mergeDateAndTime: function (start, end) {
			return {
				isAfter: function (date) {
					return false;
				}
			};
		},
		momentToISO: function (date) {}
	};

	beforeEach(inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();

		// mock modal functions
		scope.$close = function (events) {};
		scope.$dismiss = function () {};

		createController = function (isError, modalParamType) {
			var mockRoomBooking = isError ? mockRoomBookingResourceError : mockRoomBookingResourceSuccess;
			var mockModalParam;
			if (modalParamType === 0) {
				mockModalParam = mockModalParamRoomDefined;
			} else if (modalParamType === 1) {
				mockModalParam = mockModalParamCourseDefined;
			} else {
				mockModalParam = mockModalParamUndefined;
			}

			return $controller('RoomBookingController',
							{
								$scope:                  scope,
								UserSettings:            mockUserSettings,
								RoomBookingResource:     mockRoomBooking,
								modalParam:              mockModalParam,
								$translate:              mockTranslate,
								centrisNotify:           mockCentrisNotify,
								formatEventsForCalendar: mockFormatEventsForCalendar,
								dateTimeService:         mockDateTimeService
							});
		};

		spyOn(mockUserSettings, 'getDefaultDepartment').and.callThrough();
		spyOn(mockRoomBookingResourceSuccess, 'postBooking').and.callThrough();
		spyOn(mockRoomBookingResourceError, 'postBooking').and.callThrough();
		spyOn(mockTranslate, 'instant');
		spyOn(mockCentrisNotify, 'success');
		spyOn(mockCentrisNotify, 'error');
		spyOn(mockFormatEventsForCalendar, 'formatEvents').and.callThrough();
		spyOn(mockDateTimeService, 'dateFormat').and.callThrough();
		spyOn(mockDateTimeService, 'mergeDateAndTime').and.callThrough();
		spyOn(mockDateTimeService, 'momentToISO').and.callThrough();
		spyOn(scope, '$emit');
		spyOn(scope, '$close');
		spyOn(scope, '$dismiss');
	}));

	describe('when course is undefined and postBooking returns success', function () {
		beforeEach(function () {
			controller = createController(false, 0);
		});

		it('should initialize scope variables', function () {
			expect(mockUserSettings.getDefaultDepartment).toHaveBeenCalled();
			expect(scope.room).toEqual(mockModalParamRoomDefined.room);
			expect(mockDateTimeService.dateFormat).toHaveBeenCalled();
		});

		it('should apply tab booleans correctly', function () {
			scope.generalTab();
			expect(scope.forGeneral).toBeTruthy();
			expect(scope.forCourse).not.toBeTruthy();
			expect(scope.forRepeat).not.toBeTruthy();
			scope.courseTab();
			expect(scope.forGeneral).not.toBeTruthy();
			expect(scope.forCourse).toBeTruthy();
			expect(scope.forRepeat).not.toBeTruthy();
			scope.repeatTab();
			expect(scope.forGeneral).not.toBeTruthy();
			expect(scope.forCourse).not.toBeTruthy();
			expect(scope.forRepeat).toBeTruthy();
		});

		it('should post booking and notify user', function () {
			scope.bookEvent();
			expect(mockDateTimeService.mergeDateAndTime).toHaveBeenCalled();
			expect(mockRoomBookingResourceSuccess.postBooking).toHaveBeenCalled();
			expect(mockFormatEventsForCalendar.formatEvents).toHaveBeenCalled();
			expect(scope.$emit).toHaveBeenCalled();
			expect(mockCentrisNotify.success).toHaveBeenCalled();
			expect(scope.$close).toHaveBeenCalled();
		});
	});

	describe('when room is undefined and postBooking returns success', function () {
		beforeEach(function () {
			controller = createController(false, 1);
		});

		it('should initialize scope variables', function () {
			expect(scope.courseID).toEqual(mockModalParamCourseDefined.courseInstance.ID);
			expect(mockModalParamCourseDefined.room).toEqual({ID: undefined, Name: ''});
		});
	});

	describe('when modalParam is undefined', function () {
		beforeEach(function () {
			controller = createController(true, -1);
		});

		it('should replace modalParam attributes with defaults', function () {
			expect(mockModalParamUndefined.start).not.toEqual(undefined);
			expect(mockModalParamUndefined.end).not.toEqual(undefined);
		});
	});

	describe('when room is undefined and postBooking returns error', function () {
		beforeEach(function () {
			controller = createController(true, 1);
		});

		it('should make use of the room dropdown and notify that error occurred', function () {
			scope.roomID = 2;
			scope.bookEvent();

			expect(scope.booking.courseID).toBeTruthy();

			expect(mockRoomBookingResourceError.postBooking).toHaveBeenCalled();
			expect(scope.$dismiss).toHaveBeenCalled();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith(errorMsg);
		});
	});
});