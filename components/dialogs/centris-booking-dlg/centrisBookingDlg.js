"use strict";

/**
 * @ngdoc service
 * @name dialogs.centrisBookingDlg
 * @description
 * Allows the user to book a room.
 *
 * @example:
 * ```html
 * centrisBookingDlg.show(room).then(function(data){
 *     // TODO: do something with the result!!!
 });
 * ```
 */
angular.module("sharedServices").factory("centrisBookingDlg",
function centrisBookingDlg($translate, $uibModal) {
	return {
		show: function show(room, courseInstance, ssn, start, end) {
			var modalInstance = $uibModal.open({
				controller: "RoomBookingController",
				templateUrl:"components/dialogs/centris-booking-dlg/room-booking-dialog.tpl.html",
				resolve: {
					modalParam: function () {
						return {
							room:           room,
							courseInstance: courseInstance,
							ssn:            ssn,
							start:          start,
							end:            end
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});
