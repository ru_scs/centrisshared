'use strict';

// TODO: this is probably no longer used, since the API takes care
// of calculating recurring events now.
angular.module('sharedServices').service('centrisBookingService',
function (dateTimeService) {
	// Private variables:
	var i,
		runner,
		runnerStart,
		runnerEnd,
		mergeStart,
		mergeEnd,
		obj,
		diff,
		onlyTheDayFormat,
		x,
		dayOfWeek;

	return {
		calculateSingleBooking: function(startDate, endDate, startTime, endTime) {
			var listOfBookings = [];
			mergeStart = dateTimeService.mergeDateAndTime(moment(startDate), dateTimeService.parseTime(startTime));
			mergeEnd = dateTimeService.mergeDateAndTime(moment(endDate), dateTimeService.parseTime(endTime));
			obj = {
				start: moment(mergeStart).format(),
				end: moment(mergeEnd).format()
			};
			listOfBookings.push(obj);
			return listOfBookings;
		},
		calculateDailyBookings: function (startDate, endDate, startTime, endTime, untilDate, endType, interval, occurr) {
			var listOfBookings = [];
			if (interval === 1) { // Every single day
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(1, 'days').format(), runnerEnd = moment(runnerEnd).add(1, 'days')) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
						i < occurr;
						i++, runnerStart = moment(runnerStart).add(1, 'days').format(), runnerEnd = moment(runnerEnd).add(1, 'days') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			} else if (interval > 1) { // Repeat days with interval
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(interval, 'days').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'days')) {
						if (runnerStart > moment(untilDate).format()) {
							break; // The interval could jump over the untilDate, so this should stop it
						}
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							i < occurr;
							i++, runnerStart = moment(runnerStart).add(interval, 'days').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'days') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			}
			return listOfBookings;
		},
		calculateWeeklyBookings: function (startDate, endDate, startTime, endTime, untilDate,
			endType, interval, occurr, selectedDays) {
			var listOfBookings = [];
			if (interval === 1) { // Every single week
				if (endType === 'date') { // Ends on a date
					// Start by adding date from and to as the first booking
					mergeStart = dateTimeService.mergeDateAndTime(moment(startDate), dateTimeService.parseTime(startTime));
					mergeEnd = dateTimeService.mergeDateAndTime(moment(endDate), dateTimeService.parseTime(endTime));
					obj = {
						start: moment(mergeStart).format(),
						end: moment(mergeEnd).format()
					};
					listOfBookings.push(obj);

					// Now calculate the repeat:
					diff = moment(mergeStart).diff(moment(mergeEnd)); // How long the booking is
					onlyTheDayFormat = 'YYYY-MM-DD'; // Don't want to compare the hour of the day.
					angular.forEach(selectedDays, function (day) {
						for (var date = moment().day(day.day);
								moment(date).format(onlyTheDayFormat) <= moment(untilDate).format();
								date = date.add(1, 'weeks')) {
							if (moment(date).format(onlyTheDayFormat) <= moment(startDate).format(onlyTheDayFormat)) {
								continue;
							} else {
								mergeStart = dateTimeService.mergeDateAndTime(moment(date), dateTimeService.parseTime(startTime));
								var end = moment(mergeStart).add(Math.abs(diff), 'milliseconds');
								obj = {
									start: moment(mergeStart).format(),
									end: moment(end).format()
								};
								listOfBookings.push(obj);
							}
						}
					});
				} else { // Ends after x occurrences
					x = 0; // X occurrences
					// Start by adding date from and to as the first booking
					mergeStart = dateTimeService.mergeDateAndTime(moment(startDate), dateTimeService.parseTime(startTime));
					mergeEnd = dateTimeService.mergeDateAndTime(moment(endDate), dateTimeService.parseTime(endTime));
					obj = {
						start: moment(mergeStart).format(),
						end: moment(mergeEnd).format()
					};
					listOfBookings.push(obj);
					x = x + 1; // Increment the occurrences
					if (x >= occurr) {
						return;
					}

					// Now calculate the repeat
					for (i = x, runnerStart = moment(startDate).add(1, 'days'), runnerEnd = moment(endDate).add(1, 'days');
						i < occurr;
						runnerStart = runnerStart.add('days', 1), runnerEnd = runnerEnd.add('days', 1)) {
						dayOfWeek = moment(runnerStart).day();
						for (var b = 0; b < selectedDays.length; b++) {
							if (i < occurr) {
								if (selectedDays[b].day === dayOfWeek) {
									mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
									mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
									obj = {
										start: moment(mergeStart).format(),
										end: moment(mergeEnd).format()
									};
									listOfBookings.push(obj);
									i++;
								}
							}
						}
					}
				}
			} else if (interval > 1) { // Repeat weeks with interval
				if (endType === 'date') { // Ends on a date
					// Start by adding date from and to as the first booking
					mergeStart = dateTimeService.mergeDateAndTime(moment(startDate), dateTimeService.parseTime(startTime));
					mergeEnd = dateTimeService.mergeDateAndTime(moment(endDate), dateTimeService.parseTime(endTime));
					obj = {
						start: moment(mergeStart).format(),
						end: moment(mergeEnd).format()
					};
					listOfBookings.push(obj);

					// Now calculate the repeat
					diff = moment(mergeStart).diff(moment(mergeEnd)); // How long the booking is
					onlyTheDayFormat = 'YYYY-MM-DD'; // Don't want to compare the hour of the day.
					angular.forEach(selectedDays, function (day) {
						for (var date = moment().day(day.day);
								moment(date).format(onlyTheDayFormat) <= moment(untilDate).format();
								date = date.add(interval, 'weeks')) {
							if (moment(date).format(onlyTheDayFormat) <= moment(startDate).format(onlyTheDayFormat)) {
								continue;
							} else {
								mergeStart = dateTimeService.mergeDateAndTime(moment(date), dateTimeService.parseTime(startTime));
								var end = moment(mergeStart).add(Math.abs(diff), 'milliseconds');
								obj = {
									start: moment(mergeStart).format(),
									end: moment(end).format()
								};
								listOfBookings.push(obj);
							}
						}
					});
				} else { // Ends after x occurrences
					x = 0; // X occurrences
					// Start by adding date from and to as the first booking
					mergeStart = dateTimeService.mergeDateAndTime(moment(startDate), dateTimeService.parseTime(startTime));
					mergeEnd = dateTimeService.mergeDateAndTime(moment(endDate), dateTimeService.parseTime(endTime));
					obj = {
						start: moment(mergeStart).format(),
						end: moment(mergeEnd).format()
					};
					listOfBookings.push(obj);
					x = x + 1; // Increment the occurrences
					if (x >= occurr) {
						return;
					}
					// Now calculate the repeat
					onlyTheDayFormat = 'YYYY-MM-DD'; // Don't want to compare the hour of the day.
					var whatWeek = moment().startOf('week', moment(startDate));
					for (i = x, runner = moment(whatWeek).format();
							i < occurr;
							runner = moment(runner).add(interval, 'weeks')) {
						var weekfromNow = moment(runner).add(1, 'weeks');
						for (runnerStart = moment(runner).format(), runnerEnd = moment(runner).format();
								moment(runnerStart).format(onlyTheDayFormat) < moment(weekfromNow).format(onlyTheDayFormat);
								runnerStart = moment(runnerStart).add(1, 'weeks'), runnerEnd = moment(runnerEnd).add(1, 'weeks')) {
							if (moment(runnerStart).format(onlyTheDayFormat) <= moment(startDate).format(onlyTheDayFormat)) {
								continue;
							}
							if (i >= occurr) {
								break;
							}
							dayOfWeek = moment(runnerStart).day();
							for (var v = 0; v < selectedDays.length; v++) {
								if (i < occurr) {
									if (selectedDays[v].day === dayOfWeek) {
										mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
										mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
										obj = {
											start: moment(mergeStart).format(),
											end: moment(mergeEnd).format()
										};
										listOfBookings.push(obj);
										i++;
									}
								}
							}
						}
					}
				}
			}
			return listOfBookings;
		},
		calculateMonthlyBookings: function(startDate, endDate, startTime, endTime, untilDate,
			endType, interval, occurr, selectedDays) {
			var listOfBookings = [];
			if (interval === 1) { // Every single month
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(1, 'months').format(), runnerEnd = moment(runnerEnd).add(1, 'months')) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							i < occurr;
							i++, runnerStart = moment(runnerStart).add(1, 'months').format(),
							runnerEnd = moment(runnerEnd).add(1, 'months') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			} else if (interval > 1) { // Repeat months with interval
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(interval, 'months').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'months')) {
						if (runnerStart > moment(untilDate).format()) {
							break; // The interval could jump over the untilDate, so this should stop it
						}
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							i < occurr;
							i++, runnerStart = moment(runnerStart).add(interval, 'months').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'months') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			}
			return listOfBookings;
		},
		calculateYearlyBookings: function(startDate, endDate, startTime, endTime, untilDate,
			endType, interval, occurr, selectedDays) {
			var listOfBookings = [];
			if (interval === 1) { // Every single year
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(1, 'years').format(), runnerEnd = moment(runnerEnd).add(1, 'years')) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							i < occurr;
							i++, runnerStart = moment(runnerStart).add(1, 'years').format(),
							runnerEnd = moment(runnerEnd).add(1, 'years') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			} else if (interval > 1) { // Repeat months with interval
				if (endType === 'date') { // Ends on a date
					for (runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							runnerStart <= moment(untilDate).format();
							runnerStart = moment(runnerStart).add(interval, 'years').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'years')) {
						if (runnerStart > moment(untilDate).format()) {
							break; // The interval could jump over the untilDate, so this should stop it
						}
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				} else { // Ends after x occurrences
					for (i = 0, runnerStart = moment(startDate).format(), runnerEnd = moment(endDate).format();
							i < occurr;
							i++, runnerStart = moment(runnerStart).add(interval, 'years').format(),
							runnerEnd = moment(runnerEnd).add(interval, 'years') ) {
						mergeStart = dateTimeService.mergeDateAndTime(moment(runnerStart), dateTimeService.parseTime(startTime));
						mergeEnd = dateTimeService.mergeDateAndTime(moment(runnerEnd), dateTimeService.parseTime(endTime));
						obj = {
							start: moment(mergeStart).format(),
							end: moment(mergeEnd).format()
						};
						listOfBookings.push(obj);
					}
				}
			}
			return listOfBookings;
		}
	};
});
