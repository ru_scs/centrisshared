"use strict";

/**
 * This is the backend for the RoomBookingDialog.
 */
angular.module("sharedServices").factory("RoomBookingResource",
function RoomBookingResource(CentrisResource) {
	return {
		postBooking: function (booking, listOfDates, recurrence) {
			var bookingViewModel = {
				Bookings:     [],
				Recurrence:   recurrence,
				Description:  booking.eventDescription,
				CourseID:     booking.courseID,
				GroupID:      booking.groupID === null ? 0 : booking.groupID,
				BookingType:  booking.bookingType,
				DepartmentID: booking.departmentID,
				TeacherSSN:   booking.teacherSSN
			};
			for (var i = 0; i < listOfDates.length; i++) {
				bookingViewModel.Bookings.push( {
					StartTime: listOfDates[i].start,
					EndTime: listOfDates[i].end
				});
			}

			return CentrisResource.post("rooms", ":roomid/bookings", { roomid: booking.roomID }, bookingViewModel);
		},
		editBooking: function (bookingId, from, to, roomId) {
			var obj = {
				ID:        bookingId,
				StartTime: from,
				EndTime:   to,
				RoomID:    roomId
			};

			return CentrisResource.put("rooms", ":roomid/bookings", {roomid: roomId}, obj);
		},
		removeBooking: function (roomId, bookingId) {
			var param = {
				roomid: roomId,
				ID:     bookingId
			};
			return CentrisResource.remove("rooms", ":roomid/bookings/:ID", param);
		}
	};
});
