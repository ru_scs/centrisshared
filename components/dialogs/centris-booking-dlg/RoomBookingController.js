"use strict";

/*
 * RoomBookingController
 * Handles the booking form.
 * Can accept one or more of the following:
 * - room object (in which case we don't display the option to select a room),
 * - courseID (in which case we don't display the option to select a course instance)
 * - ssn (in case we want to create a booking belonging to a given person)
 * This controller takes care of interacting with the backend service.
 */
angular.module("sharedServices").controller("RoomBookingController",
function RoomBookingController($scope, UserSettings, RoomBookingResource, modalParam, $translate,
	centrisNotify, formatEventsForCalendar, dateTimeService) {
	$scope.courseInstance = modalParam.courseInstance;
	$scope.departmentID   = UserSettings.getDefaultDepartment();
	$scope.semester       = "";
	$scope.room           = undefined;
	$scope.roomID         = 207; // room ID for M101
	$scope.courseID       = null;
	// set boolean variables for tabs
	$scope.forGeneral     = true;
	$scope.forCourse      = false;
	$scope.forRepeat      = false;
	$scope.bookingType    = -3; // default type, room booking

	var dlgTitle = "(no title)";

	// Set sensible defaults for those parameters which aren't passed in:
	if (modalParam.start === undefined) {
		modalParam.start = moment().hour(8).minute(30);
	}
	if (modalParam.end === undefined) {
		modalParam.end = moment().hour(9).minute(15);
	}
	if (modalParam.room === undefined) {
		if (modalParam.courseInstance !== undefined) {
			var forTranslated = $translate.instant("RoomBookingDialog.For");
			dlgTitle = forTranslated + " " + modalParam.courseInstance.CourseID;
		}

		modalParam.room = {
			ID: undefined,
			Name: ""
		};
	} else {
		dlgTitle = modalParam.room.Name;
		$scope.room = modalParam.room;
	}

	if (modalParam.courseInstance !== undefined) {
		$scope.courseID = modalParam.courseInstance.ID;
	}

	if (modalParam.course === undefined) {
		modalParam.course = {
			Name: ""
		};
	}

	$scope.bookingFocus = true;

	$scope.dateFormat = dateTimeService.dateFormat();

	$scope.booking = {
		startDate:    modalParam.start.format($scope.dateFormat),
		endDate:      modalParam.end.format($scope.dateFormat),
		fromTime:     modalParam.start.format("HH:mm"),
		toTime:       modalParam.end.format("HH:mm"),
		roomID:       modalParam.room.ID,
		courseID:     null,
		title:        dlgTitle,
		weekday:      modalParam.start.day(),
		departmentID: 1, // TODO: Allow user to set this!!!
		bookingType:  $scope.bookingType,
		teacherSSN:   null,
		groupID:      null
	};

	if ($scope.courseInstance !== undefined) {
		// If the booking was accessed through a course schedule
		// we want to use that specific course
		$scope.booking.courseID = $scope.courseInstance.ID;
	}

	$scope.generalTab = function () {
		$scope.forGeneral = true;
		$scope.forCourse = false;
		$scope.forRepeat = false;
	};

	$scope.courseTab = function () {
		$scope.forCourse = true;
		$scope.forGeneral = false;
		$scope.forRepeat = false;
	};

	$scope.repeatTab = function () {
		$scope.forRepeat = true;
		$scope.forGeneral = false;
		$scope.forCourse = false;
	};

	$scope.defaultFocus = true;

	// Used in modal for types of repetitions, needs to bee refactored
	$scope.repeatFilter = {
		repeatTypes: [
			{ value: "None",   text: $translate.instant("RoomBookingDialog.none")  },
			{ value: "Daily",  text: $translate.instant("RoomBookingDialog.days")  },
			{ value: "Weekly", text: $translate.instant("RoomBookingDialog.weeks") },
			{ value: "Monthly",text: $translate.instant("RoomBookingDialog.months")}
			// Noone seems to be using the yearly option (can't blame them...)
		],
		repeatDays: [
			{ day: 1, text: "monday",    shortText: $translate.instant("RoomBookingDialog.mon"),
				selected: 1 === $scope.booking.weekday  },
			{ day: 2, text: "tuesday",   shortText: $translate.instant("RoomBookingDialog.tue"),
				selected: 2 === $scope.booking.weekday  },
			{ day: 3, text: "wednesday", shortText: $translate.instant("RoomBookingDialog.wed"),
				selected: 3 === $scope.booking.weekday  },
			{ day: 4, text: "thursday",  shortText: $translate.instant("RoomBookingDialog.thu"),
				selected: 4 === $scope.booking.weekday  },
			{ day: 5, text: "friday",    shortText: $translate.instant("RoomBookingDialog.fri"),
				selected: 5 === $scope.booking.weekday  },
			{ day: 6, text: "saturday",  shortText: $translate.instant("RoomBookingDialog.sat"),
				selected: 6 === $scope.booking.weekday  },
			{ day: 7, text: "sunday",    shortText: $translate.instant("RoomBookingDialog.sun"),
				selected: 7 === $scope.booking.weekday  }
		],

		// Repeat every 1st day/week/month, 2nd, 3rd etc.
		repeatInterval: 1,

		// How should the repeat end?
		repeatEndDate: moment().startOf("day").add(7, "days").format($scope.dateFormat),
		repeatEndOccur: 12,
		endsAfterType: "occurr" // date vs. occurr
	};

	// By default, there is no recurrence:
	$scope.repeatSelected = $scope.repeatFilter.repeatTypes[0];

	$scope.bookEvent = function () {
		var startDate        = $scope.booking.startDate;
		var startTime        = $scope.booking.fromTime;
		var endTime          = $scope.booking.toTime;
		var eventDescription = $scope.booking.eventDescription;
		var listOfBookings   = [];

		var mergeStart = dateTimeService.mergeDateAndTime(startDate, startTime);
		var mergeEnd   = dateTimeService.mergeDateAndTime(startDate, endTime);

		if (mergeStart.isAfter(mergeEnd)) {
			centrisNotify.error("RoomBookingDialog.Messages.StartMustPrecedeEnd");
			return;
		}

		if ($scope.room === undefined) {
			// if we are booking a room for a specific course
			// we want to make use of the centris room dropdown list
			$scope.booking.roomID = $scope.roomID;
		} else if ($scope.courseInstance === undefined) {
			// if we are booking for a specific room
			// we want to make use of the centris course dropdown list
			// if the no course option is selected we set courseID to null
			$scope.booking.courseID = ($scope.courseID === "null") ? null : $scope.courseID;
		}

		// Use the value selected in the booking type dropdown
		$scope.booking.bookingType = $scope.bookingType;

		var recurrence;

		if ($scope.repeatSelected.value !== "None") { // Repeated event
			var endType   = $scope.repeatFilter.endsAfterType;   // Type of end
			var untilDate = $scope.repeatFilter.repeatEndDate;   // When the repeat of event should stop
			var occurr    = $scope.repeatFilter.repeatEndOccur;  // Number of occurrences
			var interval  = $scope.repeatFilter.repeatInterval;
			var repType   = $scope.repeatSelected.value;

			var untilMerged = dateTimeService.mergeDateAndTime(untilDate, endTime);

			if (endType === "date") {
				if (mergeStart.isAfter(untilMerged)) {
					centrisNotify.error("RoomBookingDialog.Messages.EndDateMustComeAfterStart");
				}
			}

			recurrence = {
				Type:           repType,            // Daily/weekly/monthly/yearly
				Begins:         dateTimeService.momentToISO(mergeStart),
				EndType:        endType,            // Type of end (after a certain date has been set,
				// or after a certain number of instances)
				Ends:           dateTimeService.momentToISO(untilMerged),  // Only valid if endType is set to "date"
				Occurs:         occurr,             // Only valid if endType is set to "occurr"
				PeriodType:     1,                  // TODO
				PeriodInterval: interval,
				WeekMonday:     $scope.repeatFilter.repeatDays[0].selected,
				WeekTuesday:    $scope.repeatFilter.repeatDays[1].selected,
				WeekWednesday:  $scope.repeatFilter.repeatDays[2].selected,
				WeekThursday:   $scope.repeatFilter.repeatDays[3].selected,
				WeekFriday:     $scope.repeatFilter.repeatDays[4].selected,
				WeekSaturday:   $scope.repeatFilter.repeatDays[5].selected,
				WeekSunday:     $scope.repeatFilter.repeatDays[6].selected
			};
		} else {
			listOfBookings.push({
				start: dateTimeService.momentToISO(mergeStart),
				end:   dateTimeService.momentToISO(mergeEnd)
			});
		}

		// Make sure we don't send the groupID with when not appropriate
		if ($scope.booking.bookingType !== 2 && $scope.booking.bookingType !== 3) {
			$scope.booking.groupID = null;
		}

		RoomBookingResource.postBooking($scope.booking, listOfBookings, recurrence).success(function (bookings) {
			var events = formatEventsForCalendar.formatEvents(bookings);
			$scope.$emit("eventsAdded", bookings);
			centrisNotify.success("RoomBookingDialog.Messages.BookingsAdded");
			$scope.$close(events);
		}).error(function(data) {
			$scope.$dismiss();
			centrisNotify.error(data);
		});
	};
});
