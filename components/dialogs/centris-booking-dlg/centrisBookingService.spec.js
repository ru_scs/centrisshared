'use strict';

describe('centrisBookingService', function () {
	// load modules
	beforeEach(module('sharedServices'));
	beforeEach(module('mockConfig'));

	var centrisBookingService,
	startDate = '2014-04-28',
	endDate = '2014-04-28',
	startTime = '12:00',
	endTime = '13:00';

	beforeEach(inject(function (_centrisBookingService_) {
		centrisBookingService = _centrisBookingService_;
	}));

	describe('Initialization', function () {
		it('should have a \'calculateDailyBookings\' function', function () {
			expect(centrisBookingService.calculateDailyBookings).toBeDefined();
		});
		it('should have a \'calculateWeeklyBookings\' function', function () {
			expect(centrisBookingService.calculateWeeklyBookings).toBeDefined();
		});
		it('should have a \'calculateMonthlyBookings\' function', function () {
			expect(centrisBookingService.calculateMonthlyBookings).toBeDefined();
		});
	});

	describe('Booking Daily', function () {
		it('should calculate repeated bookings on each day ending on a another date ', function () {
			var result = centrisBookingService.calculateDailyBookings(startDate, endDate, startTime, endTime,
				'2014-05-01', 'date', 1, null);
			expect(result.length).toBe(4);
		});

		it('should calculate repeated bookings on skipping days ending on another date', function () {
			var result = centrisBookingService.calculateDailyBookings(startDate, endDate, startTime, endTime,
				'2014-05-01', 'date', 2, null);
			expect(result.length).toBe(2);
		});

		it('should calculate repeated bookings on each day ending after some occurrences', function () {
			var result = centrisBookingService.calculateDailyBookings(startDate, endDate, startTime, endTime,
				null, 'occurr', 1, 4);
			expect(result.length).toBe(4);
		});

		it('should calculate repeated bookings on skipping days ending after some occurrences', function () {
			var result = centrisBookingService.calculateDailyBookings(startDate, endDate, startTime, endTime,
				null, 'occurr', 2, 4);
			expect(result.length).toBe(4);
		});
	});

	describe('Booking Weekly', function () {
		it('should calculate repeated bookings with one booking in each week for each week ending on another date',
		function () {
			var result = centrisBookingService.calculateWeeklyBookings(startDate, endDate, startTime, endTime,
				'2014-05-05', 'date', 2, null, [{day: 1}]);
			expect(result.length).toBe(1);
		});
	});
});