"use strict";

angular.module("sharedServices").factory("centrisMarkupBuilder",
function centrisMarkupBuilder() {

	// Returns an element with passed in tags, around passed in contents
	centrisMarkupBuilder.getElementWithContents = function getElementWithContents(parentTag, content, attributeList) {
		var element = "";
		element += centrisMarkupBuilder.getOpeningTag(parentTag, attributeList);
		element += content;
		element += centrisMarkupBuilder.getClosingTag(parentTag);
		return element;
	};

	// Returns an element with no contents
	centrisMarkupBuilder.getEmptyElement = function getEmptyElement(parentTag, attributeList) {
		var element = "";
		element += centrisMarkupBuilder.getOpeningTag(parentTag, attributeList);
		element += centrisMarkupBuilder.getClosingTag(parentTag);
		return element;
	};

	// Concatenates two elements together so that they share a nesting level
	centrisMarkupBuilder.makeSiblings = function makeSiblings(firstElement, secondElement) {
		return firstElement + secondElement;
	};

	centrisMarkupBuilder.concatenateElements = function concatenateElements(elementList) {
	};

	// Returns the opening tag of an element, along with any supplied attributes
	centrisMarkupBuilder.getOpeningTag = function getOpeningTag(tagName, attributeList) {
		var tag = "<" + tagName;
		if (attributeList !== undefined) {
			// space before attributes
			tag += " ";
			tag += centrisMarkupBuilder.concatenateAttributes(attributeList);
		}
		tag += ">";
		return tag;
	};

	// Returns the closing tag of an element
	centrisMarkupBuilder.getClosingTag = function getClosingTag(tagName) {
		return "</" + tagName + ">";
	};

	// Returns singleton tag
	centrisMarkupBuilder.getSingletonTag = function getSingletonTag(tagName, attributeList) {
		var tag = "<" + tagName;
		if (attributeList !== undefined) {
			// Space before attributes
			tag += " ";
			tag += centrisMarkupBuilder.concatenateAttributes(attributeList);
		}
		tag += "/>";
	};

	// Concatenates a list of attributes together
	centrisMarkupBuilder.concatenateAttributes = function concatenateAttributes(attributeList) {
		var concatenatedAttributes = "";
		attributeList.forEach(function forEachAttribute(attribute) {
			concatenatedAttributes += centrisMarkupBuilder.buildAttributeValues(attribute["name"], attribute["values"]);
			// One space between attributes for readability
			concatenatedAttributes += "";
		});

		return concatenatedAttributes;
	};

	// Concatenates a list of values together under one attribute name
	centrisMarkupBuilder.buildAttributeValues = function buildAttributeValues(attributeName, valueList) {
		// Opening
		var concatenatedValues = "'";
		valueList.forEach(function forEachValue(value, index) {
			concatenatedValues += value;
			// Space after every value except last
			if (index < valueList.length - 1) {
				concatenatedValues += " ";
			}
		});

		concatenatedValues += "'";

		return attributeName + "=" + concatenatedValues;
	};

	return centrisMarkupBuilder;
});
