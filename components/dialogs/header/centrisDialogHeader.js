"use strict";

/**
* @ngdoc object
* @name dialogs.centrisDialogHeader
*
* @description
* This directive deals with headers in modal dialog windows.
*
*	Attributes:
*-------------------------------------------------------------------------------------------------
* - __titlecode (_Optional_)__
*  * String in the form of a translate language key
*  * Default value: "Title"
*  * Example:
* ```html
*	titlecode="courses.Keyword.Course"
* ```
* - __titletext (_Optional_)__
*  * String which is *not* translated, but added directly.
*  * Default value: undefined
* ```html
*	titletext="{{someobject.Property}}"
* ```
* - __dismiss (_Optional_)__
*  * Boolean (true/false). Determines whether there should be
*  an "x" button in the top right corner to dismiss
*  the dialog window using the following action.
* ```js
* $scope.$dismiss();
* ```
*  * Default value: true
*  * Example:
* ```html
*	dismiss="false"
* ```
* - __headerSize (_Optional_)__
*  * Size of the header in the range of 1-6, from largest to smallest
*  * Default value: 3
*  * Example:
* ```html
*	header-size="3"
* ```
* - __dialogFor (_Optional_)__
*  * A subtitle next to the supplied title
*  * Default value: undefined
*  * Example:
* ```html
*	dialog-for="T-109-INTO"
* ```
*-------------------------------------------------------------------------------------------------
*	Usage Example #1:
*-------------------------------------------------------------------------------------------------
* ```html
*	<centris-dialog-header title="courses.Header.AddingStudent" header-size="4" dismiss="true" dialog-for="T-109-INTO">
*	</centris-dialog-header>
* ```
*-------------------------------------------------------------------------------------------------
*	Output Example #1:
*-------------------------------------------------------------------------------------------------
* ```html
*	<div class="modal-header">
*		<div class='row'>
*			<div class='col-md-11'>
*				<h4>
*					<span translate='courses.Header.AddingStudent'></span>
*					<span> (T-109-INTO)</span>
*				</h4>
*			</div>
*			<div class='col-md-1'>
*				<button type='button' class="close">
*					<span>&times;</span>
*				</button>
*			</div>
*		</div>
*	</div>
* ```
*-------------------------------------------------------------------------------------------------
*	Usage Example #2
*-------------------------------------------------------------------------------------------------
* ```html
*	<!-- No attributes! -->
*	<centris-dialog-header></centris-dialog-header>
* ```
*-------------------------------------------------------------------------------------------------
*	Output Example #2:
*-------------------------------------------------------------------------------------------------
* ```html
*	<div class="modal-header">
*		<div class='row'>
*			<div class='col-md-12'>
*				<h3>
*					<span translate='Title'></span>
*					<span></span>
*				</h3>
*			</div>
*		</div>
*	</div>
* ```
*/
angular.module("sharedServices").directive("centrisDialogHeader",
function centrisDialogHeader($compile, centrisMarkupBuilder) {

	// setting up the directive object
	var directive = {};

	// Configurations
	directive.restrict   = "E";
	directive.replace    = true;
	directive.transclude = true;

	// Linking function, used to check value of attributes and build template
	directive.link = function link(scope, element, attributes) {
		var template   = "";
		var titlecode  = "Title";
		var titletext  = "";
		var dialogFor  = "";
		var headerSize = 3;
		var dismiss    = true;

		// Checking for header size
		if (attributes.headerSize !== undefined) {
			var size = parseInt(attributes.headerSize, 10);
			if (!isNaN(size)) {
				headerSize = size;
			}
		}

		// Checking whether the dismiss button in the top right corner should be left out
		if (attributes.dismiss === "false") {
			dismiss = false;
		}

		// Checks whether to use the default title or not
		if (attributes.titlecode !== undefined) {
			titlecode = attributes.titlecode;
		}

		// It is also possible to use regular text,
		// which isn't an entry in a translation table.
		// The title attribute will not be used in this case,
		// unless it is explicitly specified.
		if (attributes.titletext !== undefined) {
			titletext = attributes.titletext;
			// Unless the translated title is explicitly specified,
			// it should not be used.
			if (attributes.titlecode === undefined) {
				titlecode = undefined;
			}
		}

		// Checking whether the dismiss button in the top right corner should be left out
		if (attributes.dialogFor !== undefined &&
			attributes.dialogFor !== null &&
			attributes.dialogFor.length > 0) {
			dialogFor = " (" + attributes.dialogFor + ")";
		}

		// The span inside the header, contains translated contents
		var dialogTitle = "";
		if (titletext !== "") {
			if (titlecode !== undefined) {
				dialogTitle = centrisMarkupBuilder.getEmptyElement("span", [{name: "translate", values: [titlecode] }]);
			}
			dialogTitle += " ";
			dialogTitle += centrisMarkupBuilder.getElementWithContents("span", titletext);
		} else {
			dialogTitle = centrisMarkupBuilder.getEmptyElement("span", [{name: "translate", values: [titlecode] }]);
		}
		var forHeader = centrisMarkupBuilder.getElementWithContents("span", dialogFor);

		// Joining the header contents together
		var headerContents = centrisMarkupBuilder.makeSiblings(dialogTitle, forHeader);

		// The actual header
		var header = centrisMarkupBuilder.getElementWithContents("h" + headerSize, headerContents);

		// The container around the header
		var headerContainer, button, buttonAttributes;
		var buttonContainer = "";

		// If the dismiss attribute was supplied
		if (dismiss === true) {
			buttonAttributes = [
				{name: "type",     values: ["button"]},
				{name: "class",    values: ["close"]},
				{name: "ng-click", values: ["$dismiss()"]}
			];
			button = centrisMarkupBuilder.getElementWithContents("button", "&times;", buttonAttributes);
			headerContainer = centrisMarkupBuilder.getElementWithContents("div", header, [{
				name: "class",
				values: ["col-md-11"]}]);
			buttonContainer = centrisMarkupBuilder.getElementWithContents("div", button, [{
				name: "class",
				values: ["col-md-1"]}]);
		} else {
			// If the dismiss attribute is NOT supplied, we let the container take up 12/12 of the space
			headerContainer = centrisMarkupBuilder.getElementWithContents("div", header, [{
				name: "class",
				values: ["col-md-12"]}]);
		}

		template = centrisMarkupBuilder.makeSiblings(headerContainer, buttonContainer);
		template = centrisMarkupBuilder.getElementWithContents("div", template, [{
			name: "class",
			values: ["row"]}]);
		template = centrisMarkupBuilder.getElementWithContents("div", template, [{
			name: "class",
			values: ["modal-header"]}]);

		element.append(template);
		$compile(element.contents())(scope);
	};

	return directive;
});
