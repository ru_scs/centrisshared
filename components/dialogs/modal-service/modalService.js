"use strict";

/**
 * @ngdoc service
 * @name dialogs.modalService
 * @description
 * This service takes care of opening a modal.
 *  - Some parameters for $uibModal.open({}) are optional.
 *  - Please note that there are other ways to to this and if
 *  the function here do not suit your need. Please add it and describe.
 *  You can test your function through showCaseCustomized like so:
 *
 *	var object = {
 *		controller: undefined,
 *		templateUrl: 'courseinstance/home/components/sidebar/sidebar.html',
 *		size: 'lg',
 *		resolve: {
 *			modalParam: function () {
 *				return {course: $scope.courseInstance};
 *			}
 *		}
 *	};
 *
 *  modalService.showCaseCustomized(object)
 *
 *  But your code might
 *  be prettier if you added some of it here.
 *
 * @example
 * ```js
 *    modalService.show('{course: someCourse}, "someCtrl", "someUrl", "lg || sm" ');
 *    modalService.show('{course: someCourse}, "someCtrl", "<div>Please add text Steve</div>", "lg || sm" ');
 * ```
 */
angular.module("sharedServices").factory("modalService",
function modalService($translate, $uibModal) {
	return {
		show: function show(obj, ctrl, url, size, closeFunc) {
			if (size === undefined) {
				size = "lg";
			}

			var modalInstance = $uibModal.open({
				controller: ctrl,
				templateUrl: url,
				size: size,
				resolve: {
					modalParam: function () {
						return obj;
					}
				}
			});
			return modalInstance;
		},
		showCase2: function show(obj, ctrl, htmlString, size) {
			if (size === undefined) {
				size = "sm";
			}

			var modalInstance = $uibModal.open({
				controller: ctrl,
				template: htmlString,
				size: size,
				resolve: {
					modalParam: function () {
						return obj;
					}
				}
			});
			return modalInstance;
		},
		showCaseCustomized: function show(obj) {
			var modalInstance = $uibModal.open(obj);
			return modalInstance;
		}
	};
});
