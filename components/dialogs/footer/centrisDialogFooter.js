"use strict";

/**
* @ngdoc object
* @name dialogs.centrisDialogFooter
*
* @description
* This directive deals with footers in modal dialog windows.
* Every attribute is optional, though it does make some assumptions as to
* how the scope is put up.
*
* _ __NOTE:__ THIS DIRECTIVE WILL NOT FUNCTION CORRECTLY UNLESS ITS PARENT SCOPE CONTAINS THE FOLLOWING: _
*
* - An instance of a modal dialog
* - The parent controller (i.e., the controller than governs the dialog window) must contain a
*   model data object to hold the input data.
*	For example:
*	```js
*	$scope.model = {name: "Steve", ssn: 121290-1313};
*	```
*
*	Attributes:
*-------------------------------------------------------------------------------------------------
* - __submit (_Optional_)__
*  * String contents of submit button in the form of a translate language key
*  * Default value: "Confirm""
* - __cancel (_Optional_)__
*  * String contents of cancel button in the form of a translate language key
*  * Default value: "Cancel""
* - __submitDisabled (_Optional_)__
*  * Determines whether the submit button should be disabled or not
*  * Default value: undefined
* - __onSubmit (_Optional_)__
*  * A callback function to be called when submit is pressed
*	_Be warned however._ The default submit function contains no type of data and/or form validation
*  * Default action:
* ```js
* $scope.$close(model);
* ```
* - __onCancel (_Optional_)__
*  * A callback functoon to be called when cancel is pressed
*  * Default action:
* ```js
* $scope.$dismiss();
* ```
*-------------------------------------------------------------------------------------------------
*	Usage Example #1:
*-------------------------------------------------------------------------------------------------
* ```html
*	<centris-dialog-footer on-submit="validateRegistration()" submit="courses.Button.Validate"
*       cancel ="courses.Button.Close">
*	</centris-dialog-footer>
* ```
*-------------------------------------------------------------------------------------------------
*	Output Example #1:
*-------------------------------------------------------------------------------------------------
* ```html
*	<div class='modal-footer'>
*		<div class='row'>
*			<div class='col-md-7'>"
*			</div>"
*			<div class='col-md-5'>
*				<a class='btn btn-sm btn-default pull-right' ng-click='cancelEvent()'>
*					<span translate='courses.Button.Close'></span>
*				</a>
*				<a style='margin-right: 1em' class='btn btn-sm btn-default pull-right'
*                  ng-click='submitEvent()'>
*					<span translate='courses.Button.Validate'></span>
*				</a>
*			</div>
*		</div>
*	</div>
* ```
*-------------------------------------------------------------------------------------------------
*	Usage Example #2
*-------------------------------------------------------------------------------------------------
* ```html
*	<!-- No attributes! -->
*	<centris-dialog-footer></centris-dialog-footer>
* ```
*-------------------------------------------------------------------------------------------------
*	Output Example #2:
*-------------------------------------------------------------------------------------------------
* ```html
*	<div class='modal-footer'>
*		<div class='row'>
*			<div class='col-md-7'>"
*			</div>"
*			<div class='col-md-5'>
*				<a class='btn btn-sm btn-default pull-right' ng-click='cancelEvent()'>
*					<span translate='Cancel'></span>
*				</a>
*				<a style='margin-right: 1em' class='btn btn-sm btn-default pull-right' ng-click='submitEvent()'>
*					<span translate='Confirm'></span>
*				</a>
*			</div>
*		</div>
*	</div>
* ```
*/

angular.module("sharedServices").directive("centrisDialogFooter",
function centrisDialogFooter($compile) {

	// Setting up the directive object
	var directive = {};

	// Configurations
	directive.restrict   = "E";
	directive.replace    = false;
	directive.transclude = true;

	// The directive scope
	directive.scope = {
		onSubmit:       "&",
		onCancel:       "&",
		submit:         "=",
		cancel:         "=",
		submitDisabled: "="
	};

	// Linking function, used to check value of attributes and build template
	directive.link = function link(scope, element, attributes) {
		var template = "";
		var disableSubmit = false;

		var classes = {
			button: {
				confirm: "btn btn-sm btn-primary",
				cancel: "btn btn-sm btn-default"
			}
		};

		var styles = {
			button: {
				confirm: "margin-right: 1em"
			}
		};

		var submitText = "Confirm";
		var cancelText = "Cancel";

		// Default submit event
		link.defaultSubmit = function defaultSubmit() {
			scope.$parent.$close(scope.$parent.model);
		};

		// Default cancel event
		link.defaultCancel = function defaultCancel() {
			scope.$parent.$dismiss();
		};

		// Checking whether to use default submit event
		if (attributes.onSubmit === undefined) {
			scope.submitEvent = link.defaultSubmit;
		} else {
			scope.submitEvent = scope.onSubmit;
		}

		// Checking whether to use default cancel event
		if (attributes.onCancel === undefined) {
			scope.cancelEvent = link.defaultCancel;
		} else {
			scope.cancelEvent = scope.onCancel;
		}

		// Checking if submit button should be disabled
		if (attributes.submitDisabled !== undefined) {
			disableSubmit = true;
		}

		// Overwriting default confirm text
		if (attributes.submit !== undefined) {
			submitText = attributes.submit;
		}

		// Overwriting default cancel text
		if (attributes.cancel !== undefined) {
			cancelText = attributes.cancel;
		}

		template += "<div class='modal-footer'>";
		template +=		"<div class='row'>";
		template +=			"<div class='col-md-7'>";
		template +=			"</div>";
		template +=			"<div class='col-md-5'>";
		template +=			"<div class='pull-right'>";
		if (disableSubmit === true) {
			template +=			"<button disabled='' style='" +
				styles.button.confirm + "'  class='" +
				classes.button.confirm + "' ng-click='submitEvent()'>";
		} else {
			template +=			"<button style='" +
				styles.button.confirm + "'  class='" +
				classes.button.confirm + "' ng-click='submitEvent()'>";
		}
		template +=					"<span translate='" + submitText + "'></<span>";
		template +=				"</button>";
		template +=				"<button class='" + classes.button.cancel + "' ng-click='cancelEvent()'>";
		template +=					"<span translate='" + cancelText + "'></<span>";
		template +=				"</button>";
		template +=				"</div>";
		template +=			"</div>";
		template +=		"</div>";
		template +=	"</div>";

		element.append(template);
		$compile(element.contents())(scope);
	};

	return directive;
});
