"use strict";

/**
 * CommentDlg allows the user to add a comment, this dialog
 * is designed to be reusable under various circumstances.
 */
angular.module("sharedServices").factory("CommentDlg",
function CommentDlg($uibModal) {
	return {
		// This function is all the dialog has to offer.
		// It allows the caller to specify two language
		// keys:
		// * a key to some messsage which will be displayed
		//   above the input box, instructing the user what
		//   to enter into that box. This is optional, and
		//   no message will be displayed if this is empty.
		// * a key to some language string which will be used
		//   as a header in the dialog. Optional, a default
		//   title will be used otherwise.
		show: function show(msg, title) {
			var modalInstance = $uibModal.open({
				controller: "CommentDlgController",
				templateUrl: "components/dialogs/comment-add-dlg/comment-dlg.tpl.html",
				// size:        "lg",
				resolve: {
					commentConfig: function() {
						return {
							message: msg,
							title: title
						};
					}
				}
			});

			return modalInstance.result;
		}
	};
});
