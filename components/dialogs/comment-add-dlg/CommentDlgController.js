"use strict";

angular.module("sharedServices").controller("CommentDlgController",
function CommentDlgController($scope, commentConfig) {
	$scope.model = {
		Comment: "",
		Message: commentConfig.message,
		Title:   commentConfig.title
	};
});