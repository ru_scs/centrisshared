"use strict";

/**
 * This controller handles allowing the user to create a new issue.
 */
angular.module("sharedServices").controller("IssueDirectiveController",
function IssueDirectiveController($scope, $location, $uibModalInstance, $translate,
	IssueDirectiveResource, UserService, type) {
	$scope.lang           = $translate.use(); // en or is
	$scope.types          = [];
	$scope.departmentID   = 0;
	$scope.departmentName = "";
	$scope.initialType    = type || "";
	$scope.issueType      = type || "";
	$scope.description    = "";
	$scope.user           = {
		name:       UserService.getFullName(),
		ssn:        UserService.getUserSSN(),
		department: UserService.getUserDepartment()
	};
	$scope.departmentID = $scope.user.department.ID;

	IssueDirectiveResource.getTypes().success(function (types) {
		$scope.types = types;
	}).error(function (err) {
		console.log("err:", err);
	});

	$scope.postIssue = function () {
		var data = {
			typeName:    $scope.issueType,
			issuerSSN:   $scope.user.ssn,
			issuerName:  $scope.user.name,
			description: $scope.description,
			department:  $scope.departmentID
		};

		IssueDirectiveResource.postIssue(data).
		success(function (res) {
			$uibModalInstance.close();
			$location.path("issues/" + res.id);
		}).
		error(function (err) {
			console.log(err);
		});
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss("cancel");
	};

	$scope.depChange = function (dep) {
		$scope.departmentName = dep.Name;
		$scope.departmentID   = dep.ID;
	};
});
