"use strict";

/**
 * This directive is for the modal window to create
 * an issue for a certain office. It is basically a
 * button tag, so the text inside the button is what
 * you put in the directive in the markup.
 *
 * Optional attribute:
 *    type: takes in the name of the type to be selected
 *
 *    Example: type="changeGroup" -- this makes the change
 *             group type pre-selected.
 */
angular.module("sharedServices").directive("submitIssue",
function submitIssue($uibModal) {
	return {
		transclude: true,
		restrict: "E",
		template: "<button ng-click='open()' type='button' class='btn btn-default btn-success'" +
			" ng-transclude></button>",
		scope: {
			type: "@"
		},
		link: function (scope, element, attrs) {
			scope.open = function () {
				$uibModal.open({
					templateUrl: "components/issues/submit.tpl.html",
					controller:  "IssueDirectiveController",
					resolve: {
						type: function () {
							return scope.type;
						}
					}
				});
			};
		}
	};
});
