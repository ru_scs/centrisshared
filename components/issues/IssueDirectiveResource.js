"use strict";

angular.module("sharedServices").factory("IssueDirectiveResource",
function ($http, ENV) {
	var url = ENV.issuesapiEndpoint;

	return {
		postIssue: function (issue) {
			return $http.post(url + "issues/", issue);
		},
		getTypes: function () {
			return $http.get(url + "types");
		}
	};
});