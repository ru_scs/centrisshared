"use strict";

angular.module("sharedServices").factory("mockCentrisNotify", function() {
	return {
		success: function success(messageKey, titleKey) {

		},
		error: function error(messageKey, titleKey) {

		},
		successWithParam: function successWithParam(messageKey, param) {

		},
		errorWithParam: function errorWithParam(messageKey, param) {

		},
		successWithUndo: function successWithUndo(messageKey, undoID) {

		}
	};
});
