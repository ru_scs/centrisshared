"use strict";

angular.module("sharedServices").controller("UserDropDownController",
function UserDropDownController($scope, $rootScope, $state, UserService, $translate, dateTimeService,
	centrisErrorDlg, centrisNotify, AuthenticationService, UserProfileResource) {
	$scope.showMenuItem = false;
	$scope.currentLanguage = UserService.getUserLanguage();
	$scope.languages = [{
		value:  "en",
		name:   "English",
		icon:   "assets/img/flags/en.png"
	}, {
		value:  "is",
		name:   "Íslenska",
		icon:   "assets/img/flags/is.png"
	}];

	if (AuthenticationService.isLoggedIn()) {
		$scope.showMenuItem = true;
	}
	$scope.loginStatus     = AuthenticationService.isLoggedIn();
	$scope.currentUser     = UserService.getFullName();
	$scope.currentUserSSN  = UserService.getUserSSN();
	if ($scope.currentUserSSN === null) {
		$scope.currentUserSSN = "";
	}

	$scope.$on("loginSuccess", function() {
		$scope.currentUser    = UserService.getFullName();
		$scope.currentUserSSN = UserService.getUserSSN();
		$scope.showMenuItem = true;
		$translate.use($scope.currentLanguage);
		moment.locale($scope.currentLanguage);
		dateTimeService.setLocale($scope.currentLanguage);
		$rootScope.$broadcast("changedLanguage");
	});

	// To change language
	$scope.changeLanguage = function changeLanguage(value) {
		$translate.use(value);
		moment.locale(value);
		UserService.setUserLanguage(value);
		$scope.currentLanguage = UserService.getUserLanguage();
		dateTimeService.setLocale(value);
		$rootScope.$broadcast("changedLanguage");
		UserProfileResource.updateLanguage(value).success(function() {
			centrisNotify.success("ProfilePage.UpdatedLanguage");
		}).error(function() {
			centrisNotify.error("ProfilePage.ErrorMsg");
		});
	};

	// To report errors
	$scope.reportError = function reportError() {
		centrisErrorDlg.show();
	};

	// Logs out current user
	$scope.onLogout = function onLogout() {
		// This should clear the token:
		AuthenticationService.logout();
		$scope.loginStatus = AuthenticationService.isLoggedIn();
		$scope.showMenuItem = false;
		// ... and this should clear out any cached user info:
		UserService.reset();
		$scope.currentUser     = "";
		$scope.currentUserSSN  = "";
		// Finally, ensure we're not displaying 'stale' data (can we disable the back button?)
		$state.go("login");
		$rootScope.$broadcast("logoutSuccess", function() {
			$scope.showMenuItem = false;
		});
	};
});
