"use strict";

/**
 * @ngdoc       filter
 * @name        general.centrisTranslate
 * @description
 * This filter is used to get the english or icelandic titles and names of various objects
 * that are stored in the database. It could also be used to translate other things on the fly but that
 * is not it's intented purpose.
 *
 * This filter is part of the sharedServices so there is no need to inject anything in the controllers,
 * just start to use it in the HTML.
 *
 * Angular-translate knows which language is set (if not defined, this filter will use icelandic).
 *
 * The filter takes in a object, containing both icelandic and english translations.
 *
 * @example
 * For example a course is stored with Name field (in icelandic) and NameEN.
 *
 * ```html
 * <div>{{courseInstanceObject | centrisTranslate: "Name" : "NameEN"}}</div>
 * ```
 *
 * This filter can also be used in angular databinding.
 * For example, teaching methods needs ng-bind-html since it's values are written in html.
 * In that case we skip the curly braces and use single quoutes around the fields like so:
 *
 * ```html
 * <p ng-bind-html="thisCourse | centrisTranslate: 'TeachingMethods' : 'TeachingMethodsEN'"></p>
 * ```
 * For other translations, check out Centris Docs under UI standards -> Translations
 */
angular.module("sharedServices").filter("centrisTranslate",
function ($translate) {
	return function (obj, name, nameEN) {
		if (obj) {
			if ($translate.use() === "en") {
				return obj[nameEN];
			}
			return obj[name];
		} else {
			return "";
		}
	};
});
