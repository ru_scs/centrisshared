"use strict";

/**
 * @ngdoc overview
 * @name general
 * @description
 * This module contains various components which are commonly used in the Centris project.
 */