"use strict";

/**
 * @ngdoc object
 * @name images.centrisCourseImage
 * @description
 * This directive handles displaying an image of a course, given a course ID
 * such as "T-213-VEFF". It also handles displaying a default avatar if
 * no image is found.
 *
 * @example
 * ```html
 * <!-- Add the following tag to your markup: -->
 *
 * <centris-course-image id="'T-111-PROG'" />
 *
 * <!-- or - which is more likely - if you have the course ID in a scope variable: -->
 * <centris-course-image course-id="variableContainingCourseID" />
 *
 * ```
 */
angular.module("sharedServices").directive("centrisCourseImage",
function (ENV) {
	return {
		restrict: "E",
		template: "<img ng-src='{{imageUrl()}}'\n     class='img-polaroid pull-left'" +
			" onerror='this.src=\"assets/img/course.png\"'>",
		replace: true,
		scope: {
			courseId: "=courseId"
		},
		controller: function ($scope) {
			$scope.imageUrl = function imageUrl() {
				if ($scope.courseId !== undefined && $scope.courseId !== null) {
					return ENV.staticFileEndpoint + "courses/" + $scope.courseId + ".png";
				}
				return "";
			};
		}
	};
});
