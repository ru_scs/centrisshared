"use strict";

/**
 * @ngdoc object
 * @name images.scaleSlider
 * @description
 * This component is a scale slider used in image cropper to make the img
 * larger and smaller, gets a value from 1.0 up to 3.0 and each step in
 * the slider is 0.1
 *
 * @example
 * ```html
 * <!-- TODO: give an example! -->
 * ```
 */
angular.module("sharedServices").directive("scaleSlider",
function (rectangle, image) {

	return {
		restrict: "E",
		scope: {
			scale: "=scale"
		},
		template: "<input class='scaleSlider' id='scaleSlider' type='range' min='1.0' max='3.0'" +
			" step='0.01' value='1.0' ng-model='scale' />"
	};
});