"use strict";

/**
 * @ngdoc object
 * @name images.imgCropper
 * @description
 * This is an canvas image cropper used for selecting a profile picture.
 * @example
 * ```html
 * <!-- TODO: give an example! -->
 * ```
 */
angular.module("sharedServices").directive("imgCropper",
function (rectangle, image) {

	return {
		restrict: "E",
		scope: {
			width:      "=width",
			height:     "=height",
			scale:      "=scale",
			value:      "=value",
			cropit:     "=cropit",
			croppedimg: "=croppedimg"
		},
		template: "<canvas id='myCanvas' width='550' height='100' ng-mousemove='moveImage($event)'" +
		" ng-mousedown='mouseDown($event)' ng-mouseup='mouseUp()'> Your browser does not support Canvas.</canvas>",
		link: function(scope, element, attrs) {
			var canvas = document.getElementById("myCanvas");
			var ctx = canvas.getContext("2d");
			var cropSize = 200;
			var prevCoord = {x: 0, y: 0};
			var curCoord = {x: 0, y: 0};
			var mouseIsDown = false;
			var imageToDraw = null;

			var prefs = {};

			// Clears the canvas (used before drawing a new image on mousemove for example)
			function clearCanvas() {
				ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height);
			}

			function setUpCanvas() {
				ctx.canvas.width = prefs.width;
				ctx.canvas.height = prefs.height;
			}

			// Draws the image, finds the coords where we click and calculates from the
			// top left corner of image to draw it in the correct place on mousemove
			function drawImage(x, y) {
				curCoord.x = curCoord.x + (x - prevCoord.x);
				curCoord.y = curCoord.y + (y - prevCoord.y);
				prevCoord.x = x;
				prevCoord.y = y;

				ctx.drawImage(prefs.img,
					curCoord.x,
					curCoord.y,
					ctx.canvas.width * scope.scale,
					ctx.canvas.height * scope.scale);
			}

			// Draws the crop frame over the image
			function drawFrame() {

				// calculations to set the frame in the center of the canvas
				prefs.crop = {
					offSet: {
						x: ctx.canvas.width / 2 - cropSize / 2,
						y: ctx.canvas.height / 2 - cropSize / 2
					},
					cropSize: cropSize
				};

				prefs.color  = "rgba(255,255,255, 0.8)";
				prefs.height = ctx.canvas.height;
				prefs.width  = ctx.canvas.width;

				// This function is under ui/services/canvasObject
				var rectangleToDraw = rectangle.Rectangle(0, 0, prefs);

				rectangleToDraw.draw(ctx);
			}

			// Gets current mouse coord
			function mouseCoord(e) {
				var rect = canvas.getBoundingClientRect();
				return {
					x: e.clientX - rect.left,
					y: e.clientY - rect.top
				};
			}

			scope.mouseDown = function($event) {
				mouseIsDown = true;
				prevCoord = mouseCoord($event);
			};

			scope.mouseUp = function() {
				mouseIsDown = false;
			};

			scope.moveImage = function($event) {
				if (mouseIsDown) {
					var imgLeft    = curCoord.x,
						imgTop     = curCoord.y,
						imgRight   = curCoord.x + ctx.canvas.width * scope.scale,
						imgBottom  = curCoord.y + ctx.canvas.height * scope.scale,
						cropLeft   = prefs.crop.offSet.x,
						cropTop    = prefs.crop.offSet.y,
						cropRight  = prefs.crop.offSet.x + cropSize,
						cropBottom = prefs.crop.offSet.y + cropSize,
						coord      = mouseCoord($event);

					// These if statements are used to keep the image inside the crop frame
					// (drawFrame) so we can not crop outside the image
					if (imgLeft >= cropLeft) {
						curCoord.x = cropLeft;
					}
					if (imgTop >= cropTop) {
						curCoord.y = cropTop;
					}
					if (imgRight <= cropRight) {
						curCoord.x = prefs.crop.offSet.x - ctx.canvas.width * scope.scale + cropSize;
					}
					if (imgBottom <= cropBottom) {
						curCoord.y = prefs.crop.offSet.y - ctx.canvas.height * scope.scale + cropSize;
					}

					clearCanvas();
					drawImage(coord.x, coord.y);
					drawFrame();
				}
			};

			scope.$watch("value", function() {
				if (scope.value) {
					imageToDraw = new Image();
					imageToDraw.src = scope.value;
					imageToDraw.onload = function() {
						scope.$apply(function() {
							var height = null;
							var width = null;

							if (imageToDraw.width > imageToDraw.height) {
								height = cropSize;
								width = (imageToDraw.width / imageToDraw.height) * height;
							} else {
								width = cropSize;
								height = (imageToDraw.height / imageToDraw.width) * width;
							}

							imageToDraw.height = height;
							imageToDraw.width  = width;

							prefs = {
								img: imageToDraw,
								width: imageToDraw.width || 300,
								height: imageToDraw.height || 200
							};
							clearCanvas();
							setUpCanvas();
							drawImage(0, 0);
							drawFrame();
						});
					};
				}
			});

			scope.$watch("scale", function(value) {
				if (imageToDraw) {
					clearCanvas();
					setUpCanvas();
					drawImage(prevCoord.x, prevCoord.y);
					drawFrame();
				}
			});

			// When crop button is pressed
			scope.$watch("cropit", function() {
				if (scope.cropit) {
					var tmpCtx, tmpCanvas, imgSrc;
					tmpCanvas = document.createElement("canvas");
					tmpCtx = tmpCanvas.getContext("2d");

					tmpCanvas.width = cropSize;
					tmpCanvas.height = cropSize;
					tmpCtx.drawImage(ctx.canvas, prefs.crop.offSet.x, prefs.crop.offSet.y,
						cropSize, cropSize, 0, 0, cropSize, cropSize);

					imgSrc = tmpCanvas.toDataURL();

					scope.croppedimg = imgSrc;
					document.getElementById("croppedImg").src = imgSrc;

					scope.cropit = false;
				}
			});
		}
	};
});
