"use strict";

angular.module("sharedServices").controller("ImgCropperController",
function ImgCropperController($scope, centrisNotify, modalParam) {
	$scope.width      = 300;
	$scope.height     = 200;
	$scope.scale      = 1.0;
	$scope.slideMin   = 1.0;
	$scope.slideMax   = 3.0;
	$scope.img        = new Image();
	$scope.value      = false;
	$scope.beforeCrop = false;
	$scope.afterCrop  = false;
	$scope.croppedimg = false;
	var reader        = new FileReader();

	$scope.valfunc = function(value) {
		return true;
	};

	$scope.recfunc = function(value) {
		$scope.putOnCanvas(value);
	};

	$scope.cropImg = function() {
		$scope.cropit     = true;
		$scope.beforeCrop = false;
		$scope.afterCrop  = true;
	};

	$scope.putOnCanvas = function(value) {
		reader.onload = function(event) {
			$scope.$apply(function() {
				$scope.value = event.target.result;
				$scope.beforeCrop = true;
			});
		};
		reader.readAsDataURL(value[0]);
	};

	$scope.saveImg = function() {
		var split = $scope.croppedimg.split(",");
		var imgSrc = split[1];

		var returnObject = {
			Base64: imgSrc,
			Format: "png"
		};

		$scope.$close(returnObject);
	};
});
