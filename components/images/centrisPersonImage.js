"use strict";

/**
 * @ngdoc object
 * @name images.personImage
 * @description
 * This directive handles displaying an image of a person, given
 * the ssn of that person. It also handles displaying a default avatar if
 * no image is found.
 *
 * @example
 * ```html
 * <!-- Add the following tag to your markup: -->
 *
 * <centris-person-image ssn="variableContainingSSN" />
 * ```
 */
angular.module("sharedServices").directive("centrisPersonImage",
function centrisPersonImage(ENV) {
	return {
		restrict: "E",
		template: "<img ng-src='{{imageUrl()}}'\n     class='img-polaroid pull-left'" +
			" onerror='this.src=\"assets/img/avatar.png\"'>",
		replace: true,
		scope: {
			ssn: "=ssn"
		},
		controller: function ($scope) {
			$scope.imageUrl = function imageUrl() {
				if ($scope.ssn !== undefined && $scope.ssn !== null && $scope.ssn !== "") {
					// Something has changed... the value passed in is now
					// a number...? Anyway, we need to ensure it is a string.
					// Typescript would definitely help here!
					var ssnAsString = $scope.ssn.toString();
					if (ssnAsString.length === 9) {
						ssnAsString = "0" + ssnAsString;
					}
					var month = ssnAsString.substring(0, 2);
					return ENV.staticFileEndpoint + "persons/" + month + "/" + $scope.ssn + ".jpg";
				} else {
					return "assets/img/avatar.png";
				}
				return "";
			};
		}
	};
});
