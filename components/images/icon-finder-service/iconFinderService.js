"use strict";

/**
 * @ngdoc object
 * @name images.iconFinderService
 * @description
 * This is a service which accepts an URL as an input, and returns the type of the
 * url as an output, i.e. such that we can then display a proper icon alongside
 * the url
 *
 * @example
 *
 * ```js
 * var type = iconFinderService.getType(someUrl);
 * var anotherType = iconFinderService.getType("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
 * // anotherType should now be "yt"
 * ```
 */
angular.module("sharedServices").service("iconFinderService", function() {
	return {
		getType: function getType(url) {
			var matches = [
				{
					regex: /^https?:\/\/(?:www\.)?youtu(?:be\.com|\.be)\/?.+$/,
					type:  "yt"
				},
				{
					regex: /^https?:\/\/(?:www\.)?vimeo\.com\/?.*$/,
					type:  "vimeo"
				},
				{
					regex: /^https?:\/\/(?:docs|drive)\.google\.com\/?.*$/,
					type:  "drive"
				},
				{
					regex: /^https?:\/\/\w\w\.wikipedia\.org\/?.*$/,
					type:  "wiki"
				},
				{
					regex: /\W*.7z$/,
					type: "7z"
				},
				{
					regex: /\W*.avi$/,
					type: "avi"
				},
				{
					regex: /\W*.css$/,
					type: "css"
				},
				{
					regex: /\W*.doc$/,
					type: "doc"
				},
				{
					regex: /\W*.docx$/,
					type: "docx"
				},
				{
					regex: /\W*.exe$/,
					type: "exe"
				},
				{
					regex: /\W*.html$/,
					type: "html"
				},
				{
					regex: /\W*.jpg$/,
					type: "jpg"
				},
				{
					regex: /\W*.mp3$/,
					type: "mp3"
				},
				{
					regex: /\W*.mp4$/,
					type: "mp4"
				},
				{
					regex: /\W*.pdf$/,
					type: "pdf"
				},
				{
					regex: /\W*.png$/,
					type: "png"
				},
				{
					regex: /\W*.ppt$/,
					type: "ppt"
				},
				{
					regex: /\W*.pptx$/,
					type: "pptx"
				},
				{
					regex: /\W*.rar$/,
					type: "rar"
				},
				{
					regex: /\W*.txt$/,
					type: "txt"
				},
				{
					regex: /\W*.zip$/,
					type: "zip"
				},
				{
					// Check if it's an unckown link:
					// Source: http://blog.mattheworiordan.com/post/13174566389/url-regular-expression-for-links-with-or-without
					// Note: breaking the regex into multiple lines can be risky, so we
					// disable the jscs rule temporarily:
					// jscs:disable maximumLineLength
					regex: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
					// jscs:enable maximumLineLength
					type: "misc-link"
				}
			];

			for (var i = 0; i < matches.length; ++i) {
				if (matches[i].regex.test(url)) {
					return matches[i].type;
				}
			}

			return "misc-file";
		}
	};
});
