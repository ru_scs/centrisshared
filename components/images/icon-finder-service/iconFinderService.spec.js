"use strict";

describe("Unit: iconFinderService", function() {
	var service;

	beforeEach(module("sharedServices"));

	beforeEach(inject(function(iconFinderService) {
		service = iconFinderService;
	}));

	describe("See if each url type returns correct type", function() {
		it("should identify url as youtube", function() {
			var url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
			var type = service.getType(url);

			expect(type).toBe("yt");
		});

		it("should identify url as youtube", function() {
			var url = "https://youtu.be/tS0mQ25ezq4";
			var type = service.getType(url);

			expect(type).toBe("yt");
		});

		it("should identify url as vimeo", function() {
			var url = "https://vimeo.com/126725607";
			var type = service.getType(url);

			expect(type).toBe("vimeo");
		});

		it("should identify url as drive", function() {
			// jscs:disable maximumLineLength
			var url = "https://docs.google.com/presentation/d/1BVMiXTyPgfRCVZhFBTyfDKyqLR8SfMmQRS9jND-YdZQ/pub?start=false&loop=false&delayms=3000";
			// jscs:enable maximumLineLength
			var type = service.getType(url);

			expect(type).toBe("drive");
		});

		it("should identify url as wikipedia", function() {
			var url = "https://is.wikipedia.org/wiki/Slayer";
			var type = service.getType(url);

			expect(type).toBe("wiki");
		});

		it("should identify url as misc", function() {
			var url = "http://google.com/";
			var type = service.getType(url);

			expect(type).toBe("misc-link");
		});

		it("should identify file as misc", function() {
			var url = "file.aaa";
			var type = service.getType(url);

			expect(type).toBe("misc-file");
		});

		it("should identify url as pdf", function() {
			var url = "file.number.one.pdf";
			var type = service.getType(url);

			expect(type).toBe("pdf");
		});

		it("should identify url as pptx", function() {
			var url = "file.number.two.pdf.pptx";
			var type = service.getType(url);

			expect(type).toBe("pptx");
		});
	});
});
