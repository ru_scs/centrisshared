"use strict";

/**
 * @ngdoc overview
 * @name images
 * @description
 * This module contains various components which are used to either manipulate images,
 * display images for certain entities (persons, rooms etc.), or similar.
 */