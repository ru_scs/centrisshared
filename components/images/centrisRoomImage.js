"use strict";

/**
 * @ngdoc object
 * @name images.centrisRoomImage
 * @description
 * This is a directive which acts as an attribute. It should be applied to an
 * element (usually a div) which will then get a background-image of the room.
 *
 * @example
 * ```html
 * <div centris-room-image ng-model="theRoomObject.Name">
 * ```
 */
angular.module("sharedServices").directive("centrisRoomImage",
function centrisRoomImage(ENV) {
	return {
		restrict: "A",
		link: function(scope, elem, attrs) {
			scope.$watch(attrs.ngModel, function (roomName) {
				if (roomName !== undefined) {
					var url = ENV.staticFileEndpoint + "rooms/images/" + roomName + ".jpg";
					var cssValue = "url(" + url + ")";
					elem.css("background-image", cssValue);
				}
			});
		}
	};
});
