"use strict";

/**
 * @ngdoc object
 * @name images.centrisRoomMap
 * @description
 * This is a directive which takes care of displaying a map of a room.
 *
 * @example
 *
 * ```html
 * <centris-room-map room-name="theRoomObject.Name">
 * ```
 */
angular.module("sharedServices").directive("centrisRoomMap",
function centrisRoomMap(ENV) {
	return {
		restrict: "E",
		template: "<img ng-src='{{roomMap()}}' alt='{{roomName}}' style='width:100%;' " +
					"onerror='this.src=\"assets/img/defaultMap.png\"'/>",
		scope: {
			roomName: "="
		},
		link: function (scope, element, attrs) {
			scope.roomMap = function roomMap() {
				if (scope.roomName !== undefined) {
					var url = ENV.staticFileEndpoint + "rooms/maps/" + scope.roomName + ".png";
					return url;
				}
				return "";
			};
		}
	};
});
