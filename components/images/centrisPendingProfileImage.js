"use strict";

/**
 * @ngdoc object
 * @name images.centrisPendingProfileImage
 * @description
 * This directive handles displaying an image of a person, i.e.
 * an image which the user has requested to be his/her profile picture.
 */
angular.module("sharedServices").directive("centrisPendingProfileImage",
function centrisPendingProfileImage(ENV) {
	return {
		restrict: "E",
		template: "<img ng-src='{{imageUrl()}}'\n     class='img-polaroid pull-left'>",
		replace: true,
		scope: {
			ssn: "=ssn"
		},
		controller: function ($scope) {
			$scope.imageUrl = function imageUrl() {
				if ($scope.ssn !== undefined && $scope.ssn !== null && $scope.ssn !== "") {
					// TODO: perhaps this should be a separate endpoint defined
					// in the ENV variable...
					return ENV.profileRequests + $scope.ssn + ".jpg";
				} else {
					// TODO: different pending image?
					return "assets/img/avatar.png";
				}
				return "";
			};
		}
	};
});
