"use strict";

angular.module("sharedServices").directive("centrisFlag",
function centrisFlag() {
	return {
		restrict: "E",
		scope: {
			lang: "="
		},
		template: "<img ng-src='assets/img/flags/{{lang}}.png' alt='' />",
		link: function(scope, element, attributes) {
			if ( !(scope.lang === "is" ||
				scope.lang === "en" ||
				scope.lang === "es" ||
				scope.lang === "da" ||
				scope.lang === "de" ||
				scope.lang === "es_en" ||
				scope.lang === "fr" ||
				scope.lang === "is_en" ||
				scope.lang === "zh")) {
				// Ensure the lang - and therefore the flag we will
				// display - is known!
				scope.lang = "is";
			}
		}
	};
});