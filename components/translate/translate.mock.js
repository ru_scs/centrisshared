"use strict";

angular.module("sharedServices").factory("mockTranslate", function() {
	var translations = [];
	var language = "is";
	var translateFunc = function mockTranslate(keys) {
		return {
			then: function(fn) {
				fn(translations);
				return {
					error: function(fn) {
						fn();
					}
				};
			}
		};
	};

	translateFunc.use = function() {
		return language;
	};
	return {
		mockTranslate: translateFunc,
		setLanguage: function setLanguage(lang) {
			language = lang;
		}
	};
});
