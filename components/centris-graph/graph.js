"use strict";

/**
 * @ngdoc object
 * @name general.graph
 * @description
 *
 * @example
 * <graph typeOfGraph="bar"
 */
angular.module("sharedServices").directive("graph",
function ($compile, graphTypes, $window) {
	return {
		restrict: "E",
		scope: {
			data:    "=data",
			labels:  "=labels",
			student: "=student",
			height:  "=height",
			width:   "=width"
		},
		template: "<canvas id='graph'></canvas>",
		link: function(scope, element, attrs) {
			var previousWidth = element.width();
			var previousHeight = element.height();
			var ctx = document.getElementById("graph").getContext("2d");

			/* Optional attributes to include with the element */
			var offSet        = attrs.offSet ? attrs.offSet : 10;
			ctx.canvas.width  =  scope.width ? scope.width : 600;
			ctx.canvas.height = scope.height ? scope.height : 400;
			var student       = scope.student ? scope.student : false;
			/* Array is a required attribute */
			var array         = scope.data;

			var prefs = {
				// TODO: could we fetch this from CSS?
				fontSetting: "10px Arial",
				student:     0,
				xAxisLabels: scope.labels
			};
			var graph = graphTypes.BarGraph(ctx, array, prefs);
		}
	};
});