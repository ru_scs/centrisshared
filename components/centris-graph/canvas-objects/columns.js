"use strict";
angular.module("sharedServices").factory("columns",
function(rectangle) {
	function Columns(unitWidth, unitHeight, prefs) {
		var _this = this;

		(function() {
			_this.unitWidth = unitWidth || null;
			_this.unitHeight = unitHeight || null;
			_this.array = prefs.array || null;
			_this.student = prefs.student >= 0 ? prefs.student : null;
		})();

		this.draw = function(ctx) {
			for (var i = 0; i < _this.array.length; i++) {
				var columnHeight = _this.array[i] * _this.unitHeight;
				var x = unitWidth * (i + 1);
				var y = ctx.canvas.height - columnHeight - _this.unitWidth;
				var prefs = {
					width: _this.unitWidth,
					height: columnHeight,
					color: _this.student === null ? "#337ab7" : (_this.student === i ? "red" : "#000")
				};
				var rectangleToDraw = rectangle.Rectangle(x, y, prefs);
				rectangleToDraw.draw(ctx);
			}
		};

		function drawColumns(ctx, unitWidth, unitHeight, array, prefs) {
			for (var i = 0; i < array.length; i++) {
				var numOfUnits = array[i];
				var columnHeight = numOfUnits * unitHeight;
				if (prefs.student === i + 1) {
					ctx.fillStyle = "#FF0000";
					ctx.fillRect(unitWidth * (i + 1) , ctx.canvas.height - columnHeight - unitWidth, unitWidth, columnHeight);
				} else {
					ctx.rect(unitWidth * (i + 1) , ctx.canvas.height - columnHeight - unitWidth, unitWidth, columnHeight);
					ctx.stroke();
				}
				if (prefs) {
					ctx.font = prefs.fontSetting ? prefs.fontSetting : "20px Arial";
					ctx.strokeText(numOfUnits.toString(),
						unitWidth * (i + 1) + unitWidth / 3,
						ctx.canvas.height - columnHeight - unitWidth - 10);
				}
			}
		}
	}
	return {
		Columns: function(unitWidth, unitHeight, array) {
			return new Columns(unitWidth, unitHeight, array);
		}
	};
});
