"use strict";

angular.module("sharedServices").factory("rectangle",
function() {

	function Rectangle(x, y, prefs) {
		var _this = this;

		// Constructor
		(function() {
			_this.x      = x >= 0 ? x : 0;
			_this.y      = y >= 0 ? y : 0;
			_this.width  = prefs.width || null;
			_this.height = prefs.height || null;
			_this.color  = prefs.color || "#000";
			_this.crop   = prefs.crop || null;
			_this.prefs  = prefs || null;
		})();

		this.draw = function(ctx) {
			ctx.beginPath();
			if (_this.prefs && _this.prefs.stroke) {
				ctx.strokeStyle = "#fff";
				ctx.stroke();
			}

			ctx.fillStyle = _this.color;
			ctx.rect(_this.x,_this.y, _this.width, _this.height);

			if (_this.crop) {
				_this.cropper(ctx);
			}
			ctx.closePath();
			ctx.fill();
		};

		this.cropper = function(ctx) {
			ctx.moveTo(_this.crop.offSet.x, _this.crop.offSet.y);
			ctx.lineTo(_this.crop.offSet.x, _this.crop.offSet.y  + _this.crop.cropSize);
			ctx.lineTo(_this.crop.offSet.x  + _this.crop.cropSize, _this.crop.offSet.y  + _this.crop.cropSize);
			ctx.lineTo(_this.crop.offSet.x  + _this.crop.cropSize, _this.crop.offSet.y);
		};
	}

	return {
		Rectangle: function(x, y, color) {
			return new Rectangle(x, y, color);
		}
	};
});
