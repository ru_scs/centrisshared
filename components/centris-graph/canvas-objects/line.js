"use strict";
angular.module("sharedServices").factory("line",
function() {

	function Line(x,y,color) {
		var _this = this;

		// constructor
		(function() {
			_this.x = x.x >= 0 ? x.x : 0;
			_this.y = y.y >= 0 ? y.y : 0;
			_this.x2 = x.x2 >= 0 ? x.x2 : 0;
			_this.y2 = y.y2 >= 0 ? y.y2 : 0;
			_this.color = color || "#000";
		})();

		this.draw = function(ctx) {
			if (_this.x < 0 || _this.y < 0|| _this.x2 < 0 || _this.y2 < 0 || !_this.color) {
				console.error("Line requires an (x = {x: x, x2: x2}), (y = {y: y, y2: y2}), color");
				return;
			}
			ctx.beginPath();
			ctx.strokeStyle = _this.color;
			ctx.lineWidth = _this.width ? _this.width : 1;
			ctx.moveTo(_this.x, _this.y);
			ctx.lineTo(_this.x2, _this.y2);
			ctx.stroke();
			ctx.closePath();
		};

		this.modWidth = function(num) {
			_this.width = num;
		};

		this.contains = function(mx, my, ctx) {
			if ((mx > this.x + 5 && mx > this.x2 + 5) || (my > this.y + 5 && my > this.y2 + 5)) {
				return false;
			}
			if ((mx + 5 < this.x && mx + 5 < this.x2) || (my + 5 < this.y && my + 5 < this.y2)) {
				return false;
			}
			var slopem1 = (my - this.y) / (mx - this.x);
			var slopem2 = (this.y2 - my) / (this.x2 - mx);

			var dif = slopem1 - slopem2;

			if (dif <= 1 && dif >= -1) {
				return true;
			} else {
				return false;
			}
		};

		this.move = function(x, y, x2, y2) {
			var difOfX = x - x2;
			var difOfY = y - y2;

			this.x = this.x - difOfX;
			this.x2 = this.x2 - difOfX;
			this.y = this.y - difOfY;
			this.y2 = this.y2 - difOfY;
		};
	}
	return {
		Line: function(x,y,color) {
			return new Line(x,y,color);
		}
	};
});
