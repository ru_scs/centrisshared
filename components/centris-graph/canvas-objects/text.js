"use strict";
angular.module("sharedServices").factory("text",
function() {
	function Text(x, y, text) {
		var _this = this;
		(function() {
			_this.x = x >= 0 ? x : 0;
			_this.y = y >= 0 ? y : 0;
			_this.text = text || "Add some text here";
		})();

		this.draw = function(ctx) {
			if (_this.x < 0 || _this.y < 0|| !_this.text ) {
				console.error("text requires string and coordinates");
				return;
			}
			ctx.font = "10px Arial";
			ctx.fillStyle = "#000";
			ctx.fillText(this.text, this.x, this.y);
		};
	}

	return {
		Text: function(x,y,text) {
			return new Text(x,y,text);
		}
	};
});
