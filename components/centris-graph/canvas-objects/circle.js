"use strict";
angular.module("sharedServices").factory("Circle",
function CircleFactory() {

	function Circle(x,y,rad,color) {
		var _this = this;

		// constructor
		(function() {
			_this.x = x || null;
			_this.y = y || null;
			_this.radius = rad || null;
			_this.color = color || null;
		})();

		this.draw = function(ctx) {
			if (!_this.x || !_this.y || _this.radius || _this.color) {
				console.error("Circle requires an x, y, radius and color");
				return;
			}
			ctx.beginPath();
			ctx.arc(_this.x, _this.y, _this.radius, 0, 2 * Math.PI, false);
			ctx.fillStyle = _this.color;
			ctx.fill();
		};
	}
	return {
		Circle: function(x,y,rad,color) {
			return new Circle(x,y,rad,color);
		}
	};
});