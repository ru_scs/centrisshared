"use strict";

angular.module("sharedServices").factory("grid",
function(line, text) {

	var numHorizontalSplits = 10;

	function HorizontalGrid(unitWidth, unitHeight, range) {
		var _this = this;
		(function() {
			_this.unitWidth = unitWidth || null;
			_this.unitHeight = unitHeight || null;
			_this.range = range || null;
		})();

		this.draw = function(ctx) {
			if (!_this.unitWidth || !_this.unitHeight || !_this.range) {
				console.error("Grid requires an unitWidth, unitHeight and range");
				return;
			}

			_this.splitHeight = (ctx.canvas.height - (unitWidth * 2)) / numHorizontalSplits;
			_this.drawHorizontalLines(ctx);
			_this.drawCornerWrap(ctx);
			_this.drawLabels(ctx);
		};

		this.drawHorizontalLines = function(ctx) {
			for (var i = ctx.canvas.height - unitWidth; i > 0; i = i - _this.splitHeight) {
				var x = {x: unitWidth - 5, x2: ctx.canvas.width};
				var y = {y: i, y2: i};
				var lineToDraw = line.Line(x, y, "#AAA");
				lineToDraw.draw(ctx);
			}
		};

		this.drawCornerWrap = function(ctx) {
			// Set up vertical axis
			var x = {x: unitWidth, x2: unitWidth};
			var y = {y: unitWidth, y2: ctx.canvas.height - unitWidth};
			var lineToDraw = line.Line(x, y, "#000");
			lineToDraw.draw(ctx);
			// Set up horizontal axis
			x = {x: unitWidth, x2: ctx.canvas.width};
			y = {y: ctx.canvas.height - unitWidth, y2: ctx.canvas.height - unitWidth};
			lineToDraw = line.Line(x, y, "#000");
			lineToDraw.draw(ctx);
		};

		this.drawLabels = function(ctx) {
			var x = null;
			var y = null;
			var lineToDraw = null;
			var yLabel = range.Max;
			var sub = yLabel / numHorizontalSplits;
			var tmp = 0;
			var textToDraw = null;
			for (var i = ctx.canvas.height - (_this.splitHeight / 4 * 3); i >= unitWidth; i = i - _this.splitHeight) {
				textToDraw = text.Text(0, i, tmp.toString());
				textToDraw.draw(ctx);
				tmp = tmp + sub;
			}

			for (i = 0; i < range.xAxis.length; i++) {
				var lengthOfText = ctx.measureText(range.xAxis[i].toString());
				var offSet = (unitWidth - lengthOfText.width) / 2;
				textToDraw = text.Text(unitWidth * (i + 1) + offSet, ctx.canvas.height, range.xAxis[i].toString());
				textToDraw.draw(ctx);
				x = {x: unitWidth * (i + 2), x2: unitWidth * (i + 2)};
				y = {y: ctx.canvas.height - unitWidth, y2: ctx.canvas.height - unitWidth + 5};
				lineToDraw = line.Line(x, y, "#AAA");
				lineToDraw.draw(ctx);
			}
		};
	}

	return {
		HorizontalGrid: function(graphWidth, graphHeight, array) {
			return new HorizontalGrid(graphWidth, graphHeight, array);
		}
	};
});
