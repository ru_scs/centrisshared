"use strict";

angular.module("sharedServices").factory("image",
function() {
	function Image(x , y, prefs) {
		var _this = this;
		(function() {
			_this.x      = x >= 0 ? x : null;
			_this.y      = y >= 0 ? y : null;
			_this.img    = prefs.img    || null;
			_this.width  = prefs.width  || null;
			_this.height = prefs.height || null;
		})();

		this.draw = function(ctx) {
			console.log(_this);
			if ( !_this.img || _this.x < 0 || _this.y < 0 || !_this.width || !_this.height) {
				console.error("!_this.img || !_this.x || !_this.y || !_this.width || !_this.height. These are missing");
			}
			ctx.drawImage(_this.img, _this.x, _this.y, _this.width, _this.height);
		};
	}

	return {
		Image: function(x, y, prefs) {
			return new Image(x, y, prefs);
		}
	};
});
