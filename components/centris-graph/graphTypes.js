"use strict";

angular.module("sharedServices").factory("graphTypes",
function(line, grid, columns) {
	function rangeOfArray(array) {
		var range = {Max: array[Object.keys(array)[0]], Min: array[Object.keys(array)[0]], xAxis: []};
		for (var i = 0; i < array.length; i++) {
			range.Max = array[i] > range.Max ? array[i] : range.Max;
			range.Min = array[i] < range.Min ? array[i] : range.Min;
		}

		while (range.Max % 10 !== 0) {
			range.Max++;
		}
		return range;
	}

	function BarGraph(ctx, array, prefrences) {
		var unitWidth = ctx.canvas.width / (array.length + 1);
		prefrences.array = array;
		var range = rangeOfArray(array);
		range.xAxis = prefrences.xAxisLabels;

		var unitHeight = (ctx.canvas.height - (unitWidth * 2)) / range.Max;

		ctx.fillStyle = "#fff";
		ctx.fillRect(0,0, ctx.canvas.width, ctx.canvas.height);

		var gridToDraw = grid.HorizontalGrid(unitWidth, unitHeight, range);
		gridToDraw.draw(ctx);
		var columnsToDraw = columns.Columns(unitWidth, unitHeight, prefrences);
		columnsToDraw.draw(ctx);
	}

	return {
		BarGraph: function(ctx, graphWidth, graphHeight, array) {
			return new BarGraph(ctx, graphWidth, graphHeight, array);
		}
	};
});