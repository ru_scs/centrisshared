"use strict";

angular.module("sharedServices").directive("centrisTeacherTypeEdit",
function centrisTeacherTypeEdit($translate) {
	return {
		restrict: "E",
		scope: {
			type: "=ngModel",
			onChange: "&"
		},
		templateUrl: "components/centris-teacher-type/edit/edit.tpl.html",
		link: function (scope, element, attributes) {
			var types = [
				"teachertypes.TeacherTypes.Main",
				"teachertypes.TeacherTypes.Regular",
				"teachertypes.TeacherTypes.TA"
			];

			$translate(types).then(function(translated) {
				scope.registrationData = {
					// Note: IDs must match the TeacherType enum specified in the API.
					teacherTypes:[{
						ID: 1,
						Name: translated[types[0]]
					}, {
						ID: 2,
						Name: translated[types[1]]
					}, {
						ID: 3,
						Name: translated[types[2]]
					}]
				};
			});

		}
	};
});