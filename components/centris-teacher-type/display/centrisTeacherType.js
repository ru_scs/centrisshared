"use strict";

angular.module("sharedServices").directive("centrisTeacherType",
function centrisTeacherType() {
	return {
		restrict: "E",
		scope: {
			type: "="
		},
		templateUrl: "components/centris-teacher-type/display/teacher-type.tpl.html",
		link: function(scope, element, attributes) {

		}
	};
});