"use strict";

angular.module("aboutApp", ["sharedServices", "ui.router"])
.config(function ($stateProvider) {
	$stateProvider
	.state("about", {
		url: "/about",
		templateUrl: "components/about/about.tpl.html",
		controller: "AboutController",
		permission: "public"
	});
});