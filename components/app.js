"use strict";

/*
 * The module object for our shared services.
 * Note that it currently requires that the application which
 * uses this module has the angular-toastr component installed as well.
 */
var sharedServices = angular.module("sharedServices", [
	"toastr",                // For notifications
	"LocalStorageModule",    // For storage
	"ui.bootstrap"           // For modal window
]);

sharedServices.config(
function ($httpProvider) {
	$httpProvider.interceptors.push("centrisSpinnerInterceptor");
	$httpProvider.interceptors.push("centrisAuthInterceptor");
});
