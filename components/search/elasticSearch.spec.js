"use strict";

describe("centris elastic search directive", function() {

	var tran, scope, compile, language, element, ejsResource, ENV;

	// Load the directive's module
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	beforeEach(function() {
		tran = {
			preferredLanguage: function() {
				return language;
			},
			setPreferredLanguage: function(lang) {
				language = lang;
			}
		};

		ENV = {
			searchEndpoint: "mockServer"
		};

		ejsResource = function(endPoint) {
			return {
				Request: function(foo) {
					return "";
				}
			};
		};

		module(function($provide) {
			$provide.value("$translate", tran);
			$provide.value("ejsResource", ejsResource);
			$provide.value("ENV", ENV);
		});

		inject(function ($rootScope, $compile, $httpBackend) {
			$httpBackend.whenGET("temp.tpl.html").respond("<a>someData</a>");
			compile = $compile;
			scope = $rootScope.$new();
		});
	});
});
