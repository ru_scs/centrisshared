"use strict";

/**
 * @ngdoc object
 * @name general.elasticSearch
 * @description
 * Creates a search input field that use the Elastic Search server to search in.
 *
 * Also you have to make two templates, one for the input box and another
 * for the drop down list of the results.
 *
 * It has the following options:
 *
 * * persons = 'IsTeacher|IsActiveStudent|IsFaculty|IsStudent' - To set the priority order for persons
 * * rooms = 'true/false' - To search in rooms true or false
 * * researchers = 'true/false'
 * * coursetemplates = 'true/false'
 * * courseinstances = 'true/false'
 * * size = 'XX' - To set how many results to return
 * * templateurl = 'some-template.tpl.html' - To set path to the template for the input box.
 *   It should be in the template folder in your app.
 *
 * @example
 * ```html
 *	<elastic-search
 *		persons="IsTeacher|IsActiveStudent|IsFaculty|IsStudent"
 *		rooms="true"
 *		size="5"
 *		templateurl="home/partials/topmenu-search.tpl.html">
 *	</elastic-search>
 * ```
 * ```js
 * angular.module("**App", ['sharedServices'])
 * ```
 *
 * Example of the template for the search box:
 * ```html
 * <div class="header-search">
 *       <input id="search"
 *       class="form-control"
 *       type="text"
 *       autofocus
 *       placeholder="{{'Search_Placeholder'|translate}}"
 *       ng-model="asyncSelected"
 *       typeahead-min-length="3"
 *       typeahead="item for item in search($viewValue) | filter:$viewValue"
 *       typeahead-loading="loadingResults"
 *       typeahead-template-url="home/partials/topmenu-search-typeahead.tpl.html"
 *       typeahead-on-select="onSelectRedirect(asyncSelected); asyncSelected=''"/>
 * </div>
 * ```
 *
 * typeahead-on-select can also be "asyncSelected = asyncSelected.FullName" or what ever you want to do....
 *
 * Example of the template for the drop down list of results:
 *
 * ```html
 * <a class="col-md-12 header-search-inner" tabindex="-1" select="{{match.label.ssn}}">
 *     <div ng-show="match.label.type == 'person'" class="row">
 *         <centris-person-image class="col-md-2" ssn="match.label.ssn"></centris-person-image>
 *         <div class= "col-md-10">
 *             <div class="row">{{match.label.name}}</div>
 *             <div class="row">Kt: {{match.label.ssn | limitTo:6}}-{{match.label.ssn | limitTo:-4}}</div>
 *         </div>
 *     </div>
 * </a>
 * ```
 *
 * This will return list of names and ssn.
 *
 */
angular.module("sharedServices").directive("elasticSearch", [
	"$compile", "$location", "$translate", "ejsResource", "ENV",
	function ($compile, location, $translate, ejsResource, ENV) {

		var currentLanguage = $translate.preferredLanguage();

		return {
			restrict:   "EA",
			transclude: true,
			replace:    true,
			scope:      false,
			link: function($scope, element, attrib) {
				var personAttrs = [];
				var type        = []; // List of types (tables) to search in
				var fieldsHigh  = []; // Fields to search in
				var fieldsLow   = [];
				var size        = attrib.size; // How many results to return

				if (attrib.persons !== undefined) {
					personAttrs = attrib.persons.split("|");
				}

				// Check if it should search for persons and add the fields to array
				if (personAttrs.length > 0) {
					type.push("persons");
					fieldsHigh.push("FullName", "_id");
					fieldsLow.push("FullName", "_id", "Email");
				}

				// Check if search for rooms:
				if (attrib.rooms === "true") {
					type.push("rooms");
					// Check the language to search for and then push the fields to array
					if (currentLanguage === "is") {
						fieldsHigh.push("Name");
						fieldsLow.push("Name", "Location");
					} else {
						fieldsHigh.push("NameEN");
						fieldsLow.push("NameEN", "LocationEN");
					}
				}

				if (attrib.coursetemplates === "true") {
					type.push("coursetemplates");
					fieldsHigh.push("Name", "NameEN", "_id");
					fieldsLow.push("Name", "NameEN", "_id", "DepartmentName", "DepartmentNameEN");
				}

				if (attrib.courseinstances === "true") {
					type.push("courseinstances");
					fieldsHigh.push("Name", "Semester");
					fieldsLow.push("Name", "NameEN", "Semester");
				}

				if (attrib.researchers === "true") {
					type.push("researchers");
					fieldsHigh.push("Name");
					fieldsLow.push("Name", "SSN");
				}

				// Point to your ElasticSearch server
				var ejs = ejsResource(ENV.searchEndpoint);

				// Setup the indices and types to search across
				var request = ejs.Request()
				.indices(ENV.elasticSearchIndex) // index (database)
				.types(type); // type (table/view)

				// Define our search function that will be called when a user
				// submits a search
				$scope.search = function(input) {

					var query = ejs.BoolQuery();
					var positive;
					var negative;

					if (personAttrs.length > 0) {
						// Boost queries based on priority
						var positivePriority = personAttrs.length * 20;
						for (var i = 0; i < personAttrs.length; i++) {
							query.should(ejs.TermQuery(personAttrs[i], "1").boost(positivePriority));
							positivePriority /= 2;
						}

						positive = ejs.BoolQuery();
						negative = ejs.BoolQuery();

						positive.should(ejs.TermQuery("IsStudent", "1"));
						positive.should(ejs.TermQuery("IsTeacher", "1"));
						positive.should(ejs.TermQuery("IsFaculty", "1"));
						positive.should(ejs.TermQuery("IsActiveStudent", "1"));
						negative.should(ejs.TermQuery("IsStudent", "0"));
						negative.should(ejs.TermQuery("IsTeacher", "0"));
						negative.should(ejs.TermQuery("IsFaculty", "0"));
						negative.should(ejs.TermQuery("IsActiveStudent", "0"));

						query.should(ejs.BoostingQuery(positive, negative, 0.3));
					}

					// Note: this is PROBABLY not working. We need to investigate
					// ElasticSearch boosting further!
					if (attrib.courseinstances === "true") {
						positive = ejs.BoolQuery();
						negative = ejs.BoolQuery();

						positive.should(ejs.TermQuery("CurrentSemester", "1"));
						positive.should(ejs.TermQuery("NextSemester", "1"));
						positive.should(ejs.TermQuery("LastSemester", "1"));
						negative.should(ejs.TermQuery("CurrentSemester", "0"));
						negative.should(ejs.TermQuery("NextSemester", "0"));
						negative.should(ejs.TermQuery("LastSemester", "0"));

						query.should(ejs.BoostingQuery(positive, negative, 0.3));
					}

					query.should(ejs.QueryStringQuery(input)
						.boost(200)
						.allowLeadingWildcard(true)
						.defaultOperator("AND")
						.fields(fieldsHigh));
					query.must(ejs.QueryStringQuery(input + "*")
						.allowLeadingWildcard(false)
						.defaultOperator("AND")
						.fields(fieldsLow));

					// Send request:
					return request.query(query).size(size).doSearch().then(function(data) {

						var results  = []; // Array of obj. to return
						var flag     = 0;  // Counter of items in each group
						var tempType = ""; // Last type

						// Sort the result, such that we get all the types grouped together.
						// Also, we would like to ensure persons are always at the top...
						data.hits.hits.sort(function(a,b) {
							if (a._type < b._type) {
								return -1;
							} else if (a._type > b._type) {
								return 1;
							}

							// If the type is equal we could return 0 right away.
							// However, we can do better. For certain types, such
							// as course instances, we want to include the latest
							// instances first, based on the semester.
							if (a._type === "courseinstances") {
								if (a._source.Semester < b._source.Semester) {
									// Note: reverse ordering...
									return 1;
								} else if (a._source.Semester > b._source.Semester) {
									return -1;
								}
							}
							return 0;
						});

						// Loop through the data and make new object to return
						for (var i = 0; i < data.hits.hits.length; i++) {

							var item = {};
							var src  = data.hits.hits[i];

							// Reset flag for new type
							if (src._type !== tempType) {
								flag = 0;
							}

							if (src._type === "persons") {
								item = {
									type:            "person",
									flag:            flag,
									ssn:             src._id,
									name:            src._source.FullName,
									email:           src._source.Email,
									isTeacher:       src._source.IsTeacher,
									isFaculty:       src._source.IsFaculty,
									isStudent:       src._source.IsStudent,
									isActiveStudent: src._source.IsActiveStudent
								};
							} else if (src._type === "rooms") {
								item = {
									type:     "room",
									flag:     flag,
									id:       src._id,
									name:     src._source.Name,
									location: src._source.Location,
									desc:     src._source.Description
								};
							} else if (src._type === "coursetemplates") {
								item = {
									type:           "coursetemplate",
									flag:           flag,
									id:             src._id,
									courseid:       src._id,
									name:           src._source.Name,
									departmentid:   src._source.DepartmentID,
									departmentname: src._source.DepartmentName
								};
							} else if (src._type === "courseinstances") {
								item = {
									type:       "courseinstance",
									flag:       flag,
									id:         src._id,
									name:       src._source.Name,
									courseid:   src._source.CourseID,
									semester:   src._source.Semester,
									status:     src._source.StatusID,
									statusName: src._source.StatusName
								};
							}

							tempType = data.hits.hits[i]._type;
							flag++;
							results.push(item);
						}

						return results;
					});
				};

				// Redirect to right page when item is selected
				$scope.onSelectRedirect = function(item) {
					if (item.type === "person") {
						location.path("/students/" + item.ssn);
					} else if (item.type === "room") {
						location.path("/rooms/" + item.id);
					} else if (item.type === "coursetemplate") {
						location.path("/coursetemplates/edit/" + item.id);
					} else if (item.type === "courseinstance") {
						location.path("/courses/" + item.id);
					}
				};

				$scope.onSelectRedirectLMS = function(item) {
					if (item.type === "person") {
						location.path("/persons/" + item.ssn);
					} else if (item.type === "room") {
						location.path("/rooms/" + item.id);
					}  else if (item.type === "courseinstance") {
						location.path("/courses/" + item.id);
					}
				};
			},
			templateUrl: function(element, attrs) {
				return attrs.templateurl;
			}
		};
	}
]);
