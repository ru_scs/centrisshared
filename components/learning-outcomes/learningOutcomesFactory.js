"use strict";

/*
 * Note: this factory returns data that we should in theory fetch from the
 * backend.
 **/
angular.module("sharedServices").factory("LearningOutcomesFactory", function() {
	return {
		getCourseTypeNames: function getCourseTypeNames($translate) {
			var typeNames;
			typeNames = [
				{ID: 1, Name: "Þekking" },
				{ID: 2, Name: "Leikni"  },
				{ID: 3, Name: "Hæfni"   }
			];
			if ($translate.use() === "en_EN") {
				typeNames = [
					{ID: 1, Name: "Knowledge" },
					{ID: 2, Name: "Skills"    },
					{ID: 3, Name: "Abilities" }
				];
			}
			return typeNames;
		}
	};
});
