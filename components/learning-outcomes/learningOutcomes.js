"use strict";

/*
 * This directive allows for displaying and (optionally) editing of learning
 * outcomes.
 *
 * Options:
 * - allowEdit: when set to false, this will only display learning outcomes, but
 *              won't allow to edit them. Editing is enabled by default.
 */
angular.module("sharedServices").directive("learningOutcomes", function() {
	return {
		restrict: "E",
		scope: {
			learningOutcomes: "=ngModel",
			allowEdit: "="
		},
		templateUrl: "components/learning-outcomes/learningoutcomes.tpl.html",
		link: function(scope, element, attrs, controller) {
			scope.type = 1;
			// It would be nice to get these names from translation tables...
			scope.groups = {
				"1": {
					name: "learningoutcomes.Knowledge",
					items: []
				},
				"2": {
					name: "learningoutcomes.Skills",
					items: []
				},
				"3": {
					name: "learningoutcomes.Competence",
					items: []
				}
			};

			// Should we allow editing? By default, we do.
			if (scope.allowEdit === undefined) {
				scope.allowEdit = true;
			}

			scope.$watch("learningOutcomes", function(newVal, oldVal) {
				var item;
				var i;
				if (scope.learningOutcomes) {

					// Reset shadow arrays:
					for (i = 1; i <= 3; i++) {
						scope.groups[i].items = [];
					}

					// Regenerate array items:
					for (i = 0; i < scope.learningOutcomes.length; ++i) {
						item = scope.learningOutcomes[i];
						scope.groups[item.Type].items.push(item);
					}
				}
			}, true);

			scope.$watch("mainText", function(newValue, oldValue) {
				scope.textMissing = false;
			});

			scope.$watch("mainTextEN", function(newValue, oldValue) {
				scope.textMissing = false;
			});

			// Called when the user adds a new learning outcome:
			scope.onAdd = function() {
				if (!scope.learningOutcomes) {
					scope.learningOutcomes = [];
				}

				// The texts of the learning outcome can't both be empty:
				if ((!scope.mainText || scope.mainText.length === 0) &&
					(!scope.mainTextEN || scope.mainTextEN.length === 0)) {
					scope.textMissing = true;
					return;
				}

				// Create the new learning outcome object. Note that
				// the names of the properties must match what the API
				// expects.
				var lo = {
					Type:   parseInt(scope.type, 10),
					Text:   scope.mainText,
					TextEN: scope.mainTextEN
				};

				// Add to our main collection of learning outcomes:
				scope.learningOutcomes.push(lo);

				// Reset inputs. We don't reset the type, as it is likely
				// that the user wants to use the same type for the next
				// learning outcome.
				scope.mainText   = "";
				scope.mainTextEN = "";

				// Group array will be automatically updated in the watch (see above)
			};

			// Called when the user removes a learning outcome.
			scope.onRemove = function onRemove(index, group) {
				// Access the item being removed:
				var item = group.items[index];

				// Remove from the grouped collection:
				// group.items.splice(index, 1);

				// Remove from our main array:
				var mainIndex = scope.learningOutcomes.indexOf(item);
				if (mainIndex !== -1) {
					scope.learningOutcomes.splice(mainIndex, 1);
				}

				// Group array will be automatically updated in the watch (see above)
			};
		}
	};
});