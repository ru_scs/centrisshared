"use strict";

describe("learningOutcome directive", function() {

	var template = "<learning-outcomes ng-model='smu' allow-edit='allow' ></learning-outcomes>";
	var scope;
	var compile;
	var element;
	var backend;

	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	beforeEach(inject(function($rootScope, $compile, $httpBackend) {
		scope   = $rootScope.$new();
		compile = $compile;
		backend = $httpBackend;

		// We must declare the scope properties, they can be changed in each
		// describe block before we compile the directive. They don't need
		// to have a value however.
		scope.smu   = undefined;
		scope.allow = false;

		// Doesn't really matter what the HTML looks like which is returned
		// when querying for the template...
		$httpBackend.expectGET("components/learning-outcomes/learningoutcomes.tpl.html").respond("<div></div>");
	}));

	describe("when editing", function() {

		it("should initialize properly", function() {
			// Compile the HTML fragment into our directive object:
			element = compile(template)(scope);
			// Since the directive uses a templateUrl, we must flush
			// the HTTP pipeline to ensure the template is properly loaded:
			backend.flush();

			// Now we can start examining the scope of our directive.
			// Since it uses isolated scope, any changes it makes
			// to the scope object only affect its own scope,
			// which we must access explicitly:
			var isolatedScope = element.isolateScope();

			expect(scope.groups).not.toBeDefined();
			expect(isolatedScope.groups).toBeDefined();
		});

		it("should default to editing if not specified", function() {
			scope.allow = undefined;
			element = compile(template)(scope);
			backend.flush();

			var isolatedScope = element.isolateScope();

			expect(scope.groups).not.toBeDefined();
			expect(isolatedScope.groups).toBeDefined();
			expect(isolatedScope.allowEdit).toEqual(true);
		});

		it ("should add a new learning outcome", function() {
			element = compile(template)(scope);
			backend.flush();

			var isolatedScope = element.isolateScope();

			isolatedScope.type = 1;
			isolatedScope.mainText = "Ætti að læra";
			isolatedScope.mainTextEN = "Should learn";
			isolatedScope.onAdd();

			// Text fields should be reset to empty:
			expect(isolatedScope.mainText).toEqual("");
			expect(isolatedScope.mainTextEN).toEqual("");
			expect(isolatedScope.learningOutcomes.length).toEqual(1);
			expect(isolatedScope.learningOutcomes[0].Text).toEqual("Ætti að læra");
			expect(isolatedScope.learningOutcomes[0].TextEN).toEqual("Should learn");
		});

		it ("should NOT add a learning outcome with both texts empty", function() {
			element = compile(template)(scope);
			backend.flush();

			var isolatedScope = element.isolateScope();

			isolatedScope.type       = 1;
			isolatedScope.mainText   = "";
			isolatedScope.mainTextEN = "";
			isolatedScope.onAdd();
			expect(isolatedScope.textMissing).toBe(true);
		});

		it ("should remove a learning outcome", function() {
			scope.smu = [{
				Type: 1,
				Text: "Gaman",
				TextEN: "Smu"
			}];
			element = compile(template)(scope);
			backend.flush();

			var isolatedScope = element.isolateScope();
			isolatedScope.onRemove(0, isolatedScope.groups["1"]);

			expect(scope.smu.length).toEqual(0);
		});
	});

	describe("when read-only", function() {
		it("should initialize properly", function() {
			scope.allow = false;
			element = compile(template)(scope);
			backend.flush();

			var isolatedScope = element.isolateScope();

			expect(scope.groups).not.toBeDefined();
			expect(isolatedScope.groups).toBeDefined();
			expect(isolatedScope.allowEdit).toEqual(false);
		});
	});

});