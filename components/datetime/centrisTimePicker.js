"use strict";

/**
 * @ngdoc directive
 * @name dateTime.centrisTimePicker
 * @description
 * Gives input a more user experience when choosing fromTime or toTime.
 * When input has changed the directive emits an 'timeUpdate' event for user to catch and act.
 * Requires:
 *     sharedServices to be included in the App.
 * Uses:
 *     JQuery Timepicker (https://github.com/jonthornton/jquery-timepicker#timepicker-plugin-for-jquery):
 *     - jquery.timepicker.js
 *     - jquery.timepicker.css
 * OPTIONS
 *     timeFormat : Example: 'H:i' for 24h format. More here: http://php.net/manual/en/function.date.php
 *
 * @example
 * ```html
 *     <input type="text" centris-time-picker="{OPTIONS}">
 * ```
 */
angular.module("sharedServices").directive("centrisTimePicker",
function ($parse) {
	var timeFormat = "HH:mm";
	return {
		require: "?ngModel",
		restrict: "A",
		link: function(scope, element, attrs, ngModel) {
			var defaultOptions = {
				timeFormat: "H:i",
				minTime:    "8:30am",
				maxTime:    "9:25pm",
				step:       60
			};

			// 'customSteps': [45, 5, 45, 15, 45, 5, 45, 25, 45, 5, 45, 5, 45, 10, 45, 5, 45, 5, 45, 5, 45, 5, 45]

			// Note: we're currently using a custom fork of the jquery-timepicker library.
			// We should probably switch to the official version, once the custom step functionality
			// has been released there. However, it is implemented slightly differently than here,
			// so this code would have to be revisited.
			if (attrs.period === "start") {
				defaultOptions.customSteps = [50, 60, 50, 70, 50, 50, 55, 50, 50, 50, 50, 50, 45, 50];
				defaultOptions.minTime     = "8:30am";
			} else if (attrs.period === "stop") {
				defaultOptions.customSteps = [50, 60, 50, 70, 50, 50, 55, 50, 50, 50, 50, 50, 45, 50];
				defaultOptions.minTime     = "9:15am";
			}
			var userOptions = $parse(attrs.centrisTimePicker)() || {};
			var options = $.extend(defaultOptions, userOptions);
			var getter, setter;

			/*
			// Doesn't work at the moment... :-(
			if (attrs.minTime !== undefined) {
				scope.$watch(attrs.minTime, function(newValue) {
					if (newValue.indexOf("am") === -1) {
						newValue = newValue + "am";
					}
					element.timepicker("option", "disableTimeRanges", ['8:30am', newValue]);
				});
			}
			*/

			// Initialize the timepicker with given options
			$(element).timepicker(options);
			if (ngModel) {
				// Installing the getter, setter
				getter = $parse(attrs.ngModel);
				setter = getter.assign;
				// Set the time
				element.timepicker("setTime", getter(scope));
				// Installing jQuery change event
				element.on("change", function() {
					var newValue = element.timepicker("getTime");
					scope.$apply(function () {
						setter(scope, moment(newValue).format(timeFormat));
						scope.$emit("timeUpdate");
					});
				});
			}
		}
	};
});
