"use strict";

/**
 * @ngdoc directive
 * @name dateTime.centrisCalendar
 * @description
 *
 * The centrisCalendar directive represents an HTML element which displays a calendar.
 *
 * Calendars are used in at least 3 places in the application:
 *
 * * in room details, displaying all events/bookings belonging to a given room
 * * in student details, displaying all events/bookings belonging to a person/student
 * * in courseinstance details, displaying all events/bookings belonging to a
 *   given course
 *
 * In each case, the given view which uses the calendar must be able to supply an initial
 * list of events/bookings to display on the calendar, and then respond to a change in
 * the display, i.e. when the user switches to a new week/month/day etc.
 *
 *  Dependencies:
 *
 *  * FullCalendar.js (modified version, found in src/assets/fullcalendar in the SMS repo - AT THE MOMENT)
 */
angular.module("sharedServices").directive("centrisCalendar",
function ($compile, centrisBookingDlg, $timeout, $translate) {
	return {
		require:  "ngModel",
		restrict: "E",
		template: "<div class='calendar'></div>",
		scope: {
			// The array of events displayed in the calendar:
			ngModel:        "=",
			// Events emitted by the direcive.
			viewChanged:    "&viewChanged",
			// Input parameters, and usually there will only be one valid at a given time,
			// although there is nothing that prevents the users of this directive to
			// supply more than one.
			room:           "=room",
			courseInstance: "=courseInstance",
			ssn:            "=ssn",
			allowEdit:      "="
		},
		link: function (scope, element, attrs) {
			var sources = scope.$eval(attrs.ngModel);
			if (!sources) {
				sources = [];
			}
			var tracker = 0;

			var allowEdit;
			if (typeof scope.allowEdit === "undefined") {
				allowEdit = false;
			} else {
				allowEdit = scope.allowEdit;
			}

			// Returns the length of all source arrays plus the length of eventSource itself
			var getSources = function () {
				var equalsTracker = scope.$eval(attrs.equalsTracker);
				tracker = 0;
				angular.forEach(sources,function(value,key) {
					if (angular.isArray(value)) {
						tracker += value.length;
					}
				});

				if (angular.isNumber(equalsTracker)) {
					return tracker + sources.length + equalsTracker;
				} else {
					return tracker + sources.length;
				}
			};

			// Called when a new set of events is fetched and should
			// be displayed in the calendar
			scope.$on("eventsChanged", function(event, bookings) {
				// Since we've got a new set of events, we remove previous events:
				centrisGetFullCalendar().fullCalendar("removeEvents");
				angular.forEach(bookings, function (b) {
					centrisRenderEvent(b);
				});
			});

			// This event is raised whenever a new event is added to the calendar.
			scope.$on("eventsAdded", function(event, bookings) {
				angular.forEach(bookings, function (b) {
					centrisRenderEvent(b);
				});
			});

			// Helper function which renders a single event.
			function centrisRenderEvent(event) {
				centrisGetFullCalendar().fullCalendar("renderEvent", event, true);
			}

			function centrisGetFullCalendar() {
				// We need access to the first (and only) child
				// which is the actual fullCalendar object:
				var fc = element[0].querySelector(".calendar");
				return angular.element(fc);
			}

			// Specify the function which will get called each time the user:
			// - changes view (day/week/month)
			// - presses next/prev (to go to the next/prev day/week/month)
			function internalViewChange (start, end, callback) {
				scope.viewChanged({start: start, end: end});
				if (typeof callback === "function") {
					callback(scope.ngModel);
				}
			}

			// Update the calendar with the correct options
			function update() {
				// Calendar object exposed on scope
				scope.calendar = centrisGetFullCalendar();

				// Ensure we don't reset the view, i.e. if the user
				// had switched view, that we don't go back to agendaView:
				var view = scope.calendar.fullCalendar("getView");
				if (view && view.name) {
					view = view.name;
				} else {
					view = "agendaWeek";
				}

				var lang = $translate.use();
				var btnText;

				// Note this is a temporary fix because there seems to be
				// a bug in the language support in fullCalendar where the
				// buttonText is not translated
				if (lang === "is") {
					btnText = {
						today:    "Í dag",
						month:    "Mánuður",
						week:     "Vika",
						day:      "Dagur"
					};
				} else {
					btnText = {
						today:    "Today",
						month:    "Month",
						week:     "Week",
						day:      "Day"
					};
				}

				// Note: this config object assumes we're displaying Icelandic
				// data. TODO: allow english (and other languages?)
				scope.calendarConfig = {
					height: 851,
					firstDay: 1,
					header: {
						left:   "prev,next today",
						center: "title",
						right:  "month,agendaWeek,agendaDay"
					},
					defaultView: view,
					weekends: false,
					disableResizing: true,
					allDayDefault: false,
					eventSources: [ sources, internalViewChange ],
					selectConstraint: {
						start: "00:00",
						end: "24:00"
					},
					editable: allowEdit,
					selectable: allowEdit,
					selectHelper: allowEdit,
					eventDurationEditable: false, // resize events
					eventStartEditable: false,    // move events
					eventOverlap: false,
					snapOnSlots: true,
					unselectAuto: false,
					lang: lang,

					select: function(start, end) {
						var time = moment(start).format("HH:mm:ss");

						if (time === "00:00:00") {
							start = moment(start).hour(8).minute(30);
							end =  moment(end).hour(10).minute(5);
						} else {
							start = moment(start);
							end   = moment(end);
						}

						centrisBookingDlg.show(scope.room, scope.courseInstance, scope.ssn, start, end).then(function (newEvents) {
							angular.forEach(newEvents, function(newEvent) {
								scope.ngModel.push(newEvent);
								centrisRenderEvent(newEvent);
							});
						});
						// TODO It would be better if this was called after pressing cancel on the CBDlg
						centrisGetFullCalendar().fullCalendar("unselect");
					},
					eventRender: function (event, element) {
						// The popover needs this information.
						// When it's undefined, it shows empty popovers and the calendar will not render new calendar objects.
						if (!event.Type) { // TODO Find a better way to check?
							return;
						}

						// Format the popover based on language and the information available
						// Table for alignment
						var contentString = "<table>";
						var titleString = "";
						var courseName = "";
						var type = "";
						var roomName = "";
						var teacher = "";
						var group = "";
						var descriptio = "";
						if (lang === "is") {
							courseName = event.CourseName ? event.CourseName : "";
							type       = eventString("Tegund", event.Type.Name);
							roomName   = eventString("Stofa", event.RoomName);
							teacher    = eventString("Kennari", event.Teacher.Name);
							if (event.Description !== " ") {
								descriptio = eventString("Lýsing" , event.Description);
							}
							if (event.Group !== null) {
								group = eventString("Hópur" , event.Group.Title);
							}

						} else {
							courseName = event.CourseName ? event.CourseName : "";
							type       = eventString("Type", event.Type.Name);
							roomName   = eventString("Classroom", event.RoomName);
							teacher    = eventString("Teacher", event.Teacher.Name);
							if (event.Description !== " ") {
								descriptio = eventString("Description" , event.Description);
							}
							if (event.Group !== null) {
								group = eventString("Group" , event.Group.Title);
							}
						}
						if (courseName !== "") {
							contentString += event.Description !== " " ? "<tr><td colspan='2'>" + event.Description + "</td></tr>" : "";
						}
						titleString = event.Type.Name;
						contentString = contentString + group + teacher + roomName + descriptio + "</table>";
						element.popover({
							title:     titleString,
							placement: "top",
							html:      true,
							content:   contentString,
							container: "body",
							trigger:   "hover"
						});
					},
					eventClick: function eventClick(calEvent, jsEvent, view) {
						// TODO: display an edit dialog!!!
						console.log("eventClick");
					},
					eventResizeStop: function eventResizeStop( event, jsEvent, ui, view ) {
						// TODO: display an edit dialog
						console.log("eventResizeStop");
						// resize
					},
					eventDragStop: function eventDragStop( event, jsEvent, ui, view ) {
						// TODO: display an edit dialog
						console.log("eventDragStop");
					},
					allDaySlot: false,
					eventBorderColor: "#3d3d3d",
					minTime: "8:30am",
					maxTime: "21:25:00pm",
					// TODO: get correct localized format
					columnFormat: {
						month: "ddd",
						week:  "ddd D/M",
						day:   "ddd D/M"
					},
					titleFormat: {
						month: "MMMM YYYY",
						week:  "D. MMMM YYYY",
						day:   "dddd, D. MMMM, YYYY"
					},
					axisFormat: "H:mm",
					timeFormat: { agenda: "H:mm"},
					buttonText:  btnText,
					slots: [
						{start: "08:30", end: "09:15"},
						{start: "09:20", end: "10:05"},
						{start: "10:20", end: "11:05"},
						{start: "11:10", end: "11:55"},
						{start: "12:20", end: "13:05"},
						{start: "13:10", end: "13:55"},
						{start: "14:00", end: "14:45"},
						{start: "14:55", end: "15:40"},
						{start: "15:45", end: "16:30"},
						{start: "16:35", end: "17:20"},
						{start: "17:25", end: "18:10"},
						{start: "18:15", end: "19:00"},
						{start: "19:05", end: "19:50"},
						{start: "19:50", end: "20:35"},
						{start: "20:40", end: "21:25"}
					]
				};

				scope.calendar.fullCalendar("render");				// Forces the calendar to render, neccesary because of tabs
				scope.calendar.fullCalendar(scope.calendarConfig);
			}

			// Initial setup
			update();

			// Watches all eventSources
			scope.$watch(getSources, function( newVal, oldVal ) {
				update();
			});

			var eventString = function eventString(key, value) {
				if (value) {
					return "<tr><td>" + key + ": </td><td>" + value + " </td></tr>";
				} else {
					return "";
				}
			};
		}
	};
});
