"use strict";

/**
 * @ngdoc       filter
 * @name        dateTime.centrisDateTime
 * @description Similar as centrisDate, but includes the time as well (hours:minutes)
 *
 * @example
  * ```html
 * <div>{{ someDateObject | centrisDateTime }}</div>
 * ```
 */
angular.module("sharedServices").filter("centrisDateTime",
function (dateTimeService) {
	return function(dateString, format) {
		if (dateString !== undefined && dateString !== null && dateString !== "") {
			if (format === undefined) {
				format = dateTimeService.dateFormat() + " HH:mm";
			}

			return moment(dateString).format(format);
		}
	};
});