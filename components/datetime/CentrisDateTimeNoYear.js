"use strict";

/**
 * @ngdoc       filter
 * @name        dateTime.centrisDateTimeNoYear
 * @description Similar as centrisDate, but includes the time as well (hours:minutes) and does not include the year.
 *
 * @example
  * ```html
 * <div>{{ someDateObject | centrisDateTimeNoYear }}</div>
 * ```
 */
angular.module("sharedServices").filter("centrisDateTimeNoYear",
function (dateTimeService) {
	return function(dateString, format) {
		if (dateString !== undefined && dateString !== null && dateString !== "") {
			if (format === undefined) {
				format = dateTimeService.dateFormatNoYear() + " HH:mm";
			}

			return moment(dateString).format(format);
		}
	};
});