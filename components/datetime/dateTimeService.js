"use strict";

/**
 * @ngdoc service
 * @name dateTime.dateTimeService
 * @description
 * Contains various date/time functions. Includes the following:
 *
 * * mergeDateAndTime: Merge a date string and a time string.  The date
 * part contains the values of the passed-in date, and the time part
 * is overridden with the values of the passed in time.
 * * parseTime: Parses a time string, which should be in the format "HH:mm"
 * * toISO - plus a number of other functions (check the source Luke)
 *
 * @param {date} date - A date to alter to get the resulting string.
 * @param {time} time - An object with 'hours' and 'minutes' attributes
 *
 * @example:
 * ```js
 * // TODO
 * });
 * ```
 */
angular.module("sharedServices").service("dateTimeService",
function dateTimeService() {

	var locales = {
		is: {
			format:       "D. MMMM YYYY",
			formatNoYear: "D. MMMM",
			code:         "is"
		},
		en: {
			// TODO: look this up!!!
			format:       "DD/MM/YYYY",
			formatNoYear: "DD/MM",
			code:         "en"
		}
	};

	var currentLocale = "is";

	return {
		// Creates a moment date based on two strings:
		// - dateString - a string containing a date, formatted according to
		//   the default display format of the current locale
		// - timeString - a string containing the time.
		mergeDateAndTime: function (dateString, timeString) {

			var dateObj = moment(dateString, locales[currentLocale].format);
			var timeObj = this.parseTime(timeString);

			return dateObj.clone()
				.startOf("day")// ensure that all values are set to zero (including milliseconds)
				.hours(timeObj.hours)
				.minutes(timeObj.minutes)
				.seconds(timeObj.seconds)
				.milliseconds(timeObj.milliseconds);
		},

		// Parses a time string, which should be in the format "HH:mm"
		parseTime: function (timeString) {
			var TIME_REGEXP = /^(\d{1,2}):(\d{2})$/;
			var match = TIME_REGEXP.exec(timeString);
			return {
				hours:        match ? match[1] : 0,
				minutes:      match ? match[2] : 0,
				seconds:      0,
				milliseconds: 0,

				isValid: function isValid() {
					return !!match;
				}
			};
		},

		// Returns the default format which should be used to display dates to the user.
		dateFormat: function() {
			return locales[currentLocale].format;
		},

		// Returns a format with the year not included.
		dateFormatNoYear: function() {
			return locales[currentLocale].formatNoYear;
		},

		timeFormat: function () {
			return "HH:mm";
		},

		locale: function () {
			return currentLocale;
		},

		setLocale: function (locale) {
			currentLocale = locale;
		},

		// Accepts one variable:
		// - a date with centris format
		// Converts the date to a comparable number
		// Done because of converting icelandic months
		// was not consistent, might need further additions
		// for other sections of Centris
		getComparableDate: function(date) {
			function addZeroToNumber(number) {
				return (number > 9) ? "".concat(number.toString()) : "0".concat(number.toString());
			}

			var dateArray = [];
			var comparableDate;
			var isMonths = ["janúar", "febrúar", "mars", "apríl", "maí",
					"júní", "júlí", "ágúst", "september", "október", "nóvember", "desember"];
			var enMonths = ["January", "February", "March", "April", "May",
					"June", "July", "August", "September", "October", "November", "December"];

			if (date.indexOf(".") > -1) {
				date = date.replace(".", "");
				dateArray = date.split(" ");
				if (isMonths.indexOf(dateArray[1]) > -1) {
					dateArray[1] = (isMonths.indexOf(dateArray[1]) + 1).toString();
				} else {
					dateArray[1] = (enMonths.indexOf(dateArray[1]) + 1).toString();
				}
				for (var i = 0, len = dateArray.length; i < len; ++i) {
					dateArray[i] = addZeroToNumber(parseInt(dateArray[i], 10));
				}
				dateArray.reverse();
				comparableDate = dateArray.join("");
			} else if (date.indexOf("/") > -1) {
				dateArray = date.split("/");
				dateArray.reverse();
				comparableDate = dateArray.join("");
			}
			return comparableDate;
		},

		// Accepts two variables:
		// - date - a moment object
		// - time - a string in the format "HH:mm"
		toISO: function(date, time) {
			// If the time is pre-10:00, we append a leading zero
			// to ensure the date is legal:
			if (time.length === 4) {
				time = "0" + time;
			}
			var isoDateTime = moment(date).format("YYYY-MM-DD") + "T" + time + ":00Z";
			return isoDateTime;
		},

		momentToISO: function (momentDate) {
			return momentDate.format("YYYY-MM-DD") + "T" + momentDate.format("HH:mm:ss") + "Z";
		}
	};
});
