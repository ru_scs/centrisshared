"use strict";

/**
 * @ngdoc directive
 * @name dateTime.centrisDatePicker
 * @description
 *     Allows us to add one or more datepicker to our page with ease.
 * Requires:
 *     sharedServices to be included in the App.
 * Uses:
 *
 * OPTIONS
 *
 * @example
 * ```html
 *     <centris-date-picker ng-model="someVariable"></centris-date-picker>
 * ```
 */
angular.module("sharedServices").directive("centrisDatePicker",
function ($compile, dateTimeService) {
	return {
		restrict: "E",
		scope: {
			ngModel:  "=",
			onEnter:  "&",
			ngChange: "&",
			index:    "=",
			minDate:  "=",
			maxDate:  "="
		},
		link: function(scope, element, attrs, ngModel) {
			scope.dateFormat = dateTimeService.dateFormat();
			scope.locale     = dateTimeService.locale();

			var template = "<ng-bs3-datepicker ng-model='ngModel' ";
			// TODO: once the control has been created, it won't
			// respond to language changes. Not top priority, but should be fixed nonetheless!
			template += "language='" + scope.locale + "' date-format='" + scope.dateFormat + "' ";
			template += "min-date='minDate' max-date='maxDate' ";

			if (scope.onEnter) {
				template += "ng-keypress='onKeyPress()'";
			}

			if (scope.ngChange) {
				template += "ng-change='onChange()'";
			}
			template += "/>";

			element.append(template);
			$compile(element.contents())(scope);

			scope.onKeyPress = function ($event) {
				if ($event.keyCode === 13) {
					if (scope.onEnter) {
						scope.onEnter();
					}
				}
			};

			scope.onChange = function () {
				if (scope.ngChange) {
					scope.ngChange({index: scope.index, newDate: scope.ngModel });
				}
			};
		}
	};
});
