"use strict";

/**
 * @ngdoc overview
 * @name dateTime
 * @description
 * This module contains directives, filters and services which help working with dates and times.
 */