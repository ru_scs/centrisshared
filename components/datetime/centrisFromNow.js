"use strict";

/**
 * @ngdoc directive
 * @name dateTime.centrisFromNow
 * @description
 * Allows us to display to users how long ago (or how long until) a given
 * date is from the current date/time. Internally it uses a similar function
 * found in momentjs, and the [documentation for that function should therefore
 * be consulted](http://momentjs.com/docs/#/displaying/fromnow/)
 *
 * @example
 * ```html
 *     <span>{{ model.dueDate | centrisFromNow }} </span>
 * ```
 */
angular.module("sharedServices").filter("centrisFromNow", function() {
	return function(date) {
		return moment(date).fromNow();
	};
});
