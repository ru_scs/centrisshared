"use strict";

/**
 * @ngdoc       filter
 * @name        dateTime.centrisDate
 * @description Filter that displays a date using momentJS.
 *              In general, the default formatting should be used.
 *              There should be a very good reason why a custom format is supplied!
 * More info on formats:
 * http://momentjs.com/docs/#/parsing/string-format/
 *
 * @example
  * ```html
 * <div>{{ someDateObject | centrisDate }}</div>
 * <!-- ... or like this if it is reeeeeaaaaaaally necessary: -->
 * <div>{{ someDateObject | centrisDate:'DD. MMMM YYYY - HH:mm:ss'}}</div>
 * ```
 */
angular.module("sharedServices").filter("centrisDate",
function (dateTimeService) {
	return function(dateString, format) {
		if (dateString !== undefined && dateString !== null && dateString !== "") {
			if (format === undefined) {
				format = dateTimeService.dateFormat();
			}
			if (moment(dateString).isValid()) {
				return moment(dateString).format(format);
			} else {
				return "";
			}
		}
	};
});
