"use strict";

var typeOfClasses = function(classtype) {
	if (classtype === "Booking room") {
		return "eventBookingRoom";
	} else if (classtype === "Rep/Sick") {
		return "eventRepSick";
	} else if (classtype === "Final exam") {
		return "eventFinalExam";
	} else if (classtype === "Lecture") {
		return "eventLecture";
	} else if (classtype === "Workshop") {
		return "eventWorkshop";
	} else if (classtype === "Message") {
		return "eventMessage";
	} else if (classtype === "Appointment") {
		return "eventAppointment";
	} else {
		return "eventNoType";
	}
};

/**
 * @ngdoc service
 * @name dateTime.formatEventsForCalendar
 * @description
 * Format room bookings from Centris API server for centrisCalendar directive
 *
 * @example:
 * ```js
 *  someFunctionWhichLoadsEventsFromServer().success(function(data){
 *     $scope.data = formatEventsForCalendar(data);
 * });
 * ```
 */
angular.module("sharedServices").factory("formatEventsForCalendar",
function formatEventsForCalendar($injector) {
	return {
		formatEvents: function (events) {
			var formattedEvents = [];

			// Loop through events from server and format them for calendar directive
			angular.forEach(events, function(event) {
				if (event.Description === null || event.Description === undefined) {
					event.Description = " ";
				}

				var tempEvent = {
					title:        event.CourseName ? event.CourseName : event.Description,
					start:        moment(event.StartTime).toDate(),
					end:          moment(event.EndTime).toDate(),
					description:  event.Description.toString(),
					allDay:       false,
					newest:       true,
					eventId:      event.ID,
					deleted:      false,
					className:    typeOfClasses(event.TypeOfClass),

					// Event data for tooltips
					ID:           event.ID,
					Summary:      event.Summary,
					RoomID:       event.RoomID,
					RoomName:     event.RoomName,
					CourseID:     event.CourseID,
					CourseName:   event.CourseName,
					CourseNameEN: event.courseNameEN,
					Description:  event.Description,
					Organizer:    event.Organizer,
					GroupID:      event.GroupID,
					Group:        event.Group,
					TypeOfClass:  event.TypeOfClass,
					RecurrenceID: event.RecurrenceID,
					Teacher:      event.Teacher,
					Type:         event.Type
				};

				// Then add event to scope
				formattedEvents.push(tempEvent);
			});
			return formattedEvents;
		}
	};
});
