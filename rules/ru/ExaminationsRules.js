"use strict";

angular.module("sharedServices").factory("ExamRule",
[ function ExamRule() {
	// Only students showing up for final examinations or giving notice of illness (cf. Article 5.1)
	// are eligible for repeat examinations or supplementary examinations due to illness.
	// --
	//  @param {object}  contain status code. ResultEN and Result.
	//                  Example: "Failed", "Registered", "FailedDidNotShowUp" and some more

	return {
		RetakeExamRule1: function RetakeExamRule1(info) {
			if (info.ResultEN === "FailedDidNotShowUp") {
				return false;
			}

			return true;
		}
	};
}]);

